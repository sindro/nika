var Encore = require('@symfony/webpack-encore');

Encore

    .setOutputPath('public/build/')

    .setPublicPath('/build')

    .autoProvidejQuery()

    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    .enableBuildNotifications()
    .enableReactPreset()

    // uncomment to create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning(Encore.isProduction())

    .addEntry('js/app', './public/assets/js/app.js')
    .addEntry('js/product_show_configurable.react', './src/ade/shop-bundle/Resources/public/js/product_show_configurable.react.js')
    .addEntry('js/product_show_simple.react', './src/ade/shop-bundle/Resources/public/js/product_show_simple.react.js')
;

let config = Encore.getWebpackConfig();
module.exports = config;
