var $ = require('jquery');
require('tether');
require('bootstrap-sass');
require('lightbox2');
require('owl.carousel');

$( document ).ready( function() {

    $(".electro-store-directory .product-categories > li").arrangeObjects('ul', 4);
});

(function($, window){
    "use strict";
    var arrowWidth = 16;

    $.fn.resizeselect = function(settings) {

        return this.each( function() {

            $(this).change( function(){

                var $this = $(this);

                // create test element
                var text = $this.find("option:selected").text();
                var $test = $("<span>").html(text);

                // add to body, get width, and get out
                $test.appendTo('body');
                var width = $test.width();
                $test.remove();

                // set select width
                $this.width(width + arrowWidth);

                // run on start
            }).change();

        });
    };

    $.fn.arrangeObjects = function(wrapWith, maxCols) {

        this.each(function() {
            if ($(this).parent(wrapWith).length) $(this).unwrap();
        });

        this.parent().each(function () {
            var $subnodes       = $(this).children();

            // true will cause counter increment
            // false will cause counter decrement
            var inc     = true;
            var cols    = [];

            for (var i = 0; i < maxCols; i++) {
                cols.push($('<ul></ul>'));
                cols[i].appendTo($(this));
            }

            function sortByHeight(a, b) {
                return $(a).height() > $(b).height() ? 0 : 1;
            }

            $subnodes = $subnodes.sort(sortByHeight);

            var i = 0;
            $subnodes.each(function () {
                // logic for left and right boundry
                if (i < 0 || i === maxCols) {
                    inc = !inc;
                    // this will cause node to be added once again to the same column
                    inc ? i++ : i--;
                }

                cols[i].append($(this));

                inc ? i++ : i--;
            });
        });
    };

    if($('#order_createUser').is(':checked')){
        $('#order_plainPassword').slideDown();
    }
    $('#order_createUser').click(function() {
        if($(this).is(':checked'))
            $('#order_plainPassword').slideDown();
        else
            $('#order_plainPassword').slideUp();
    });

})(jQuery, window);

