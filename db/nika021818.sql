-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: nika
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ade_gallery`
--

DROP TABLE IF EXISTS `ade_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_gallery`
--

LOCK TABLES `ade_gallery` WRITE;
/*!40000 ALTER TABLE `ade_gallery` DISABLE KEYS */;
INSERT INTO `ade_gallery` VALUES (1,'Galleria',1,'2018-02-16 17:46:17','2018-02-16 17:46:17');
/*!40000 ALTER TABLE `ade_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_gallery_image`
--

DROP TABLE IF EXISTS `ade_gallery_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_gallery_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FE2EBA0D4E7AF8F` (`gallery_id`),
  CONSTRAINT `FK_FE2EBA0D4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `ade_gallery` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_gallery_image`
--

LOCK TABLES `ade_gallery_image` WRITE;
/*!40000 ALTER TABLE `ade_gallery_image` DISABLE KEYS */;
INSERT INTO `ade_gallery_image` VALUES (1,'alt immagine','cielo.jpeg','2018-02-16 17:48:36','2018-02-16 17:48:36',1,0),(2,'stelle','Hubble_heic0206j.jpg','2018-02-17 11:29:11','2018-02-17 11:29:11',1,1);
/*!40000 ALTER TABLE `ade_gallery_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_attribute`
--

DROP TABLE IF EXISTS `ade_shop_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_attribute`
--

LOCK TABLES `ade_shop_attribute` WRITE;
/*!40000 ALTER TABLE `ade_shop_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `ade_shop_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_attribute_option`
--

DROP TABLE IF EXISTS `ade_shop_attribute_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_attribute_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_attribute_option`
--

LOCK TABLES `ade_shop_attribute_option` WRITE;
/*!40000 ALTER TABLE `ade_shop_attribute_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `ade_shop_attribute_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_attributes_options`
--

DROP TABLE IF EXISTS `ade_shop_attributes_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_attributes_options` (
  `attribute_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  PRIMARY KEY (`attribute_id`,`option_id`),
  KEY `IDX_A9908B6EB6E62EFA` (`attribute_id`),
  KEY `IDX_A9908B6EA7C41D6F` (`option_id`),
  CONSTRAINT `FK_A9908B6EA7C41D6F` FOREIGN KEY (`option_id`) REFERENCES `ade_shop_attribute_option` (`id`),
  CONSTRAINT `FK_A9908B6EB6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `ade_shop_attribute` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_attributes_options`
--

LOCK TABLES `ade_shop_attributes_options` WRITE;
/*!40000 ALTER TABLE `ade_shop_attributes_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `ade_shop_attributes_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_author`
--

DROP TABLE IF EXISTS `ade_shop_author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `is_public` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_author`
--

LOCK TABLES `ade_shop_author` WRITE;
/*!40000 ALTER TABLE `ade_shop_author` DISABLE KEYS */;
/*!40000 ALTER TABLE `ade_shop_author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_catalog_rule`
--

DROP TABLE IF EXISTS `ade_shop_catalog_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_catalog_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `discount` decimal(10,0) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CAE207B4584665A` (`product_id`),
  KEY `IDX_CAE207B12469DE2` (`category_id`),
  KEY `IDX_CAE207BA23B42D` (`manufacturer_id`),
  KEY `IDX_CAE207BF675F31B` (`author_id`),
  KEY `IDX_CAE207B2ADD6D8C` (`supplier_id`),
  CONSTRAINT `FK_CAE207B12469DE2` FOREIGN KEY (`category_id`) REFERENCES `ade_shop_category` (`id`),
  CONSTRAINT `FK_CAE207B2ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `ade_shop_supplier` (`id`),
  CONSTRAINT `FK_CAE207B4584665A` FOREIGN KEY (`product_id`) REFERENCES `ade_shop_product` (`id`),
  CONSTRAINT `FK_CAE207BA23B42D` FOREIGN KEY (`manufacturer_id`) REFERENCES `ade_shop_manufacturer` (`id`),
  CONSTRAINT `FK_CAE207BF675F31B` FOREIGN KEY (`author_id`) REFERENCES `ade_shop_author` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_catalog_rule`
--

LOCK TABLES `ade_shop_catalog_rule` WRITE;
/*!40000 ALTER TABLE `ade_shop_catalog_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ade_shop_catalog_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_category`
--

DROP TABLE IF EXISTS `ade_shop_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_description` longtext COLLATE utf8_unicode_ci,
  `is_public` tinyint(1) NOT NULL,
  `lft` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `root` int(11) DEFAULT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_27A8D26727ACA70` (`parent_id`),
  CONSTRAINT `FK_27A8D26727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `ade_shop_category` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_category`
--

LOCK TABLES `ade_shop_category` WRITE;
/*!40000 ALTER TABLE `ade_shop_category` DISABLE KEYS */;
INSERT INTO `ade_shop_category` VALUES (1,NULL,'2018-02-12 16:07:31','2018-02-15 10:28:25','Smartphone e Tablets',NULL,1,2,0,3,1,NULL,'Smartphone e Tablets'),(2,NULL,'2018-02-15 10:24:57','2018-02-15 10:28:25','barman',NULL,1,4,0,5,2,NULL,'barman');
/*!40000 ALTER TABLE `ade_shop_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_coupon`
--

DROP TABLE IF EXISTS `ade_shop_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `end_date` date DEFAULT NULL,
  `discount` decimal(10,0) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_coupon`
--

LOCK TABLES `ade_shop_coupon` WRITE;
/*!40000 ALTER TABLE `ade_shop_coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `ade_shop_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_manufacturer`
--

DROP TABLE IF EXISTS `ade_shop_manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_manufacturer`
--

LOCK TABLES `ade_shop_manufacturer` WRITE;
/*!40000 ALTER TABLE `ade_shop_manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `ade_shop_manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_product`
--

DROP TABLE IF EXISTS `ade_shop_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` longtext COLLATE utf8_unicode_ci,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `isbn` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `in_stock` int(11) NOT NULL,
  `height` decimal(10,2) DEFAULT NULL,
  `width` decimal(10,2) DEFAULT NULL,
  `depth` decimal(10,2) DEFAULT NULL,
  `weight` decimal(10,2) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `is_public` tinyint(1) NOT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3EF8F62BB2A824D8` (`tax_id`),
  KEY `IDX_3EF8F62BA23B42D` (`manufacturer_id`),
  KEY `IDX_3EF8F62B2ADD6D8C` (`supplier_id`),
  FULLTEXT KEY `IDX_3EF8F62B2B36786B6DE440269BE5A5B1` (`title`,`description`,`short_description`),
  CONSTRAINT `FK_3EF8F62B2ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `ade_shop_supplier` (`id`),
  CONSTRAINT `FK_3EF8F62BA23B42D` FOREIGN KEY (`manufacturer_id`) REFERENCES `ade_shop_manufacturer` (`id`),
  CONSTRAINT `FK_3EF8F62BB2A824D8` FOREIGN KEY (`tax_id`) REFERENCES `ade_shop_tax` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_product`
--

LOCK TABLES `ade_shop_product` WRITE;
/*!40000 ALTER TABLE `ade_shop_product` DISABLE KEYS */;
INSERT INTO `ade_shop_product` VALUES (1,1,NULL,NULL,'2018-02-12 16:36:28','2018-02-12 16:36:28','Iphone','Iphone',NULL,NULL,'simple','IPHONE',800.00,NULL,10,NULL,NULL,NULL,NULL,NULL,1,NULL),(2,NULL,NULL,NULL,'2018-02-15 10:25:14','2018-02-17 12:07:01','Shaker','Shaker',NULL,'deuhdue hdedhuehduehduehuehuehduehduehduehd','configurable',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `ade_shop_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_product_attribute`
--

DROP TABLE IF EXISTS `ade_shop_product_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`attribute_id`),
  KEY `IDX_3165DD214584665A` (`product_id`),
  KEY `IDX_3165DD21B6E62EFA` (`attribute_id`),
  CONSTRAINT `FK_3165DD214584665A` FOREIGN KEY (`product_id`) REFERENCES `ade_shop_product` (`id`),
  CONSTRAINT `FK_3165DD21B6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `ade_shop_attribute` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_product_attribute`
--

LOCK TABLES `ade_shop_product_attribute` WRITE;
/*!40000 ALTER TABLE `ade_shop_product_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `ade_shop_product_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_product_products`
--

DROP TABLE IF EXISTS `ade_shop_product_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_product_products` (
  `product_id` int(11) NOT NULL,
  `product_product_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`product_product_id`),
  KEY `IDX_CD3B0CEA4584665A` (`product_id`),
  KEY `IDX_CD3B0CEAD6E33971` (`product_product_id`),
  CONSTRAINT `FK_CD3B0CEA4584665A` FOREIGN KEY (`product_id`) REFERENCES `ade_shop_product` (`id`),
  CONSTRAINT `FK_CD3B0CEAD6E33971` FOREIGN KEY (`product_product_id`) REFERENCES `ade_shop_product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_product_products`
--

LOCK TABLES `ade_shop_product_products` WRITE;
/*!40000 ALTER TABLE `ade_shop_product_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `ade_shop_product_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_products_authors`
--

DROP TABLE IF EXISTS `ade_shop_products_authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_products_authors` (
  `product_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`author_id`),
  KEY `IDX_E000EC2B4584665A` (`product_id`),
  KEY `IDX_E000EC2BF675F31B` (`author_id`),
  CONSTRAINT `FK_E000EC2B4584665A` FOREIGN KEY (`product_id`) REFERENCES `ade_shop_product` (`id`),
  CONSTRAINT `FK_E000EC2BF675F31B` FOREIGN KEY (`author_id`) REFERENCES `ade_shop_author` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_products_authors`
--

LOCK TABLES `ade_shop_products_authors` WRITE;
/*!40000 ALTER TABLE `ade_shop_products_authors` DISABLE KEYS */;
/*!40000 ALTER TABLE `ade_shop_products_authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_products_categories`
--

DROP TABLE IF EXISTS `ade_shop_products_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_products_categories` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`),
  KEY `IDX_F78397634584665A` (`product_id`),
  KEY `IDX_F783976312469DE2` (`category_id`),
  CONSTRAINT `FK_F783976312469DE2` FOREIGN KEY (`category_id`) REFERENCES `ade_shop_category` (`id`),
  CONSTRAINT `FK_F78397634584665A` FOREIGN KEY (`product_id`) REFERENCES `ade_shop_product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_products_categories`
--

LOCK TABLES `ade_shop_products_categories` WRITE;
/*!40000 ALTER TABLE `ade_shop_products_categories` DISABLE KEYS */;
INSERT INTO `ade_shop_products_categories` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `ade_shop_products_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_supplier`
--

DROP TABLE IF EXISTS `ade_shop_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_supplier`
--

LOCK TABLES `ade_shop_supplier` WRITE;
/*!40000 ALTER TABLE `ade_shop_supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `ade_shop_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_tax`
--

DROP TABLE IF EXISTS `ade_shop_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percentage` int(11) NOT NULL,
  `is_public` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_tax`
--

LOCK TABLES `ade_shop_tax` WRITE;
/*!40000 ALTER TABLE `ade_shop_tax` DISABLE KEYS */;
INSERT INTO `ade_shop_tax` VALUES (1,'iva',22,1);
/*!40000 ALTER TABLE `ade_shop_tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ade_shop_variant`
--

DROP TABLE IF EXISTS `ade_shop_variant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ade_shop_variant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isbn` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `in_stock` int(11) NOT NULL,
  `height` decimal(10,2) DEFAULT NULL,
  `width` decimal(10,2) DEFAULT NULL,
  `depth` decimal(10,2) DEFAULT NULL,
  `weight` decimal(10,2) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `is_public` tinyint(1) NOT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1CF14D2B4584665A` (`product_id`),
  KEY `IDX_1CF14D2BB2A824D8` (`tax_id`),
  CONSTRAINT `FK_1CF14D2B4584665A` FOREIGN KEY (`product_id`) REFERENCES `ade_shop_product` (`id`),
  CONSTRAINT `FK_1CF14D2BB2A824D8` FOREIGN KEY (`tax_id`) REFERENCES `ade_shop_tax` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ade_shop_variant`
--

LOCK TABLES `ade_shop_variant` WRITE;
/*!40000 ALTER TABLE `ade_shop_variant` DISABLE KEYS */;
INSERT INTO `ade_shop_variant` VALUES (1,2,1,'100ml','SHAKER100ML',NULL,21.00,5,NULL,NULL,NULL,NULL,NULL,1,NULL),(2,2,1,'200ml','SHAKER200ML',NULL,30.00,2,NULL,NULL,NULL,NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `ade_shop_variant` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-18  0:00:01
