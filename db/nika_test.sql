-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Feb 13, 2018 alle 23:34
-- Versione del server: 5.7.21-0ubuntu0.17.10.1
-- Versione PHP: 7.1.14-1+ubuntu17.10.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nika_test`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_attribute`
--

CREATE TABLE `ade_shop_attribute` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_attributes_options`
--

CREATE TABLE `ade_shop_attributes_options` (
  `attribute_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_attribute_option`
--

CREATE TABLE `ade_shop_attribute_option` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_author`
--

CREATE TABLE `ade_shop_author` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `is_public` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_catalog_rule`
--

CREATE TABLE `ade_shop_catalog_rule` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `discount` decimal(10,0) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ade_shop_catalog_rule`
--

INSERT INTO `ade_shop_catalog_rule` (`id`, `product_id`, `category_id`, `manufacturer_id`, `author_id`, `supplier_id`, `title`, `context`, `discount`, `start_date`, `end_date`) VALUES
(1, 1, NULL, NULL, NULL, NULL, 'sconto', '', '10', NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_category`
--

CREATE TABLE `ade_shop_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_description` longtext COLLATE utf8_unicode_ci,
  `is_public` tinyint(1) NOT NULL,
  `lft` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `root` int(11) DEFAULT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ade_shop_category`
--

INSERT INTO `ade_shop_category` (`id`, `parent_id`, `created_at`, `updated_at`, `title`, `short_description`, `is_public`, `lft`, `lvl`, `rgt`, `root`, `image_name`, `slug`) VALUES
(1, NULL, NULL, NULL, 'categoria prodotto', NULL, 1, 1, 1, 1, NULL, NULL, 'categoria-prodotto');

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_coupon`
--

CREATE TABLE `ade_shop_coupon` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `end_date` date DEFAULT NULL,
  `discount` decimal(10,0) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_manufacturer`
--

CREATE TABLE `ade_shop_manufacturer` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_product`
--

CREATE TABLE `ade_shop_product` (
  `id` int(11) NOT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` longtext COLLATE utf8_unicode_ci,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `isbn` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `in_stock` int(11) NOT NULL,
  `height` decimal(10,2) DEFAULT NULL,
  `width` decimal(10,2) DEFAULT NULL,
  `depth` decimal(10,2) DEFAULT NULL,
  `weight` decimal(10,2) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `is_public` tinyint(1) NOT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ade_shop_product`
--

INSERT INTO `ade_shop_product` (`id`, `tax_id`, `manufacturer_id`, `supplier_id`, `created_at`, `updated_at`, `slug`, `title`, `subtitle`, `short_description`, `context`, `sku`, `price`, `isbn`, `in_stock`, `height`, `width`, `depth`, `weight`, `description`, `is_public`, `image_name`) VALUES
(1, 1, NULL, NULL, '2018-02-13 00:00:00', '2018-02-13 00:00:00', 'prodotto', 'Prodotto', NULL, NULL, 'simple', 'PRODOTTO', '10.00', NULL, 10, NULL, NULL, NULL, NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_products_authors`
--

CREATE TABLE `ade_shop_products_authors` (
  `product_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_products_categories`
--

CREATE TABLE `ade_shop_products_categories` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ade_shop_products_categories`
--

INSERT INTO `ade_shop_products_categories` (`product_id`, `category_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_product_attribute`
--

CREATE TABLE `ade_shop_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_product_products`
--

CREATE TABLE `ade_shop_product_products` (
  `product_id` int(11) NOT NULL,
  `product_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_supplier`
--

CREATE TABLE `ade_shop_supplier` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_tax`
--

CREATE TABLE `ade_shop_tax` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percentage` int(11) NOT NULL,
  `is_public` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ade_shop_tax`
--

INSERT INTO `ade_shop_tax` (`id`, `title`, `percentage`, `is_public`) VALUES
(1, 'iva', 22, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `ade_shop_variant`
--

CREATE TABLE `ade_shop_variant` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isbn` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `in_stock` int(11) NOT NULL,
  `height` decimal(10,2) DEFAULT NULL,
  `width` decimal(10,2) DEFAULT NULL,
  `depth` decimal(10,2) DEFAULT NULL,
  `weight` decimal(10,2) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `is_public` tinyint(1) NOT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `ade_shop_attribute`
--
ALTER TABLE `ade_shop_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ade_shop_attributes_options`
--
ALTER TABLE `ade_shop_attributes_options`
  ADD PRIMARY KEY (`attribute_id`,`option_id`),
  ADD KEY `IDX_A9908B6EB6E62EFA` (`attribute_id`),
  ADD KEY `IDX_A9908B6EA7C41D6F` (`option_id`);

--
-- Indici per le tabelle `ade_shop_attribute_option`
--
ALTER TABLE `ade_shop_attribute_option`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ade_shop_author`
--
ALTER TABLE `ade_shop_author`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ade_shop_catalog_rule`
--
ALTER TABLE `ade_shop_catalog_rule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CAE207B4584665A` (`product_id`),
  ADD KEY `IDX_CAE207B12469DE2` (`category_id`),
  ADD KEY `IDX_CAE207BA23B42D` (`manufacturer_id`),
  ADD KEY `IDX_CAE207BF675F31B` (`author_id`),
  ADD KEY `IDX_CAE207B2ADD6D8C` (`supplier_id`);

--
-- Indici per le tabelle `ade_shop_category`
--
ALTER TABLE `ade_shop_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_27A8D26727ACA70` (`parent_id`);

--
-- Indici per le tabelle `ade_shop_coupon`
--
ALTER TABLE `ade_shop_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ade_shop_manufacturer`
--
ALTER TABLE `ade_shop_manufacturer`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ade_shop_product`
--
ALTER TABLE `ade_shop_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3EF8F62BB2A824D8` (`tax_id`),
  ADD KEY `IDX_3EF8F62BA23B42D` (`manufacturer_id`),
  ADD KEY `IDX_3EF8F62B2ADD6D8C` (`supplier_id`);
ALTER TABLE `ade_shop_product` ADD FULLTEXT KEY `IDX_3EF8F62B2B36786B6DE440269BE5A5B1` (`title`,`description`,`short_description`);

--
-- Indici per le tabelle `ade_shop_products_authors`
--
ALTER TABLE `ade_shop_products_authors`
  ADD PRIMARY KEY (`product_id`,`author_id`),
  ADD KEY `IDX_E000EC2B4584665A` (`product_id`),
  ADD KEY `IDX_E000EC2BF675F31B` (`author_id`);

--
-- Indici per le tabelle `ade_shop_products_categories`
--
ALTER TABLE `ade_shop_products_categories`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `IDX_F78397634584665A` (`product_id`),
  ADD KEY `IDX_F783976312469DE2` (`category_id`);

--
-- Indici per le tabelle `ade_shop_product_attribute`
--
ALTER TABLE `ade_shop_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`),
  ADD KEY `IDX_3165DD214584665A` (`product_id`),
  ADD KEY `IDX_3165DD21B6E62EFA` (`attribute_id`);

--
-- Indici per le tabelle `ade_shop_product_products`
--
ALTER TABLE `ade_shop_product_products`
  ADD PRIMARY KEY (`product_id`,`product_product_id`),
  ADD KEY `IDX_CD3B0CEA4584665A` (`product_id`),
  ADD KEY `IDX_CD3B0CEAD6E33971` (`product_product_id`);

--
-- Indici per le tabelle `ade_shop_supplier`
--
ALTER TABLE `ade_shop_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ade_shop_tax`
--
ALTER TABLE `ade_shop_tax`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ade_shop_variant`
--
ALTER TABLE `ade_shop_variant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1CF14D2B4584665A` (`product_id`),
  ADD KEY `IDX_1CF14D2BB2A824D8` (`tax_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `ade_shop_attribute`
--
ALTER TABLE `ade_shop_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `ade_shop_attribute_option`
--
ALTER TABLE `ade_shop_attribute_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `ade_shop_author`
--
ALTER TABLE `ade_shop_author`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `ade_shop_catalog_rule`
--
ALTER TABLE `ade_shop_catalog_rule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `ade_shop_category`
--
ALTER TABLE `ade_shop_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `ade_shop_coupon`
--
ALTER TABLE `ade_shop_coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `ade_shop_manufacturer`
--
ALTER TABLE `ade_shop_manufacturer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `ade_shop_product`
--
ALTER TABLE `ade_shop_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `ade_shop_supplier`
--
ALTER TABLE `ade_shop_supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `ade_shop_tax`
--
ALTER TABLE `ade_shop_tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `ade_shop_variant`
--
ALTER TABLE `ade_shop_variant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `ade_shop_attributes_options`
--
ALTER TABLE `ade_shop_attributes_options`
  ADD CONSTRAINT `FK_A9908B6EA7C41D6F` FOREIGN KEY (`option_id`) REFERENCES `ade_shop_attribute_option` (`id`),
  ADD CONSTRAINT `FK_A9908B6EB6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `ade_shop_attribute` (`id`);

--
-- Limiti per la tabella `ade_shop_catalog_rule`
--
ALTER TABLE `ade_shop_catalog_rule`
  ADD CONSTRAINT `FK_CAE207B12469DE2` FOREIGN KEY (`category_id`) REFERENCES `ade_shop_category` (`id`),
  ADD CONSTRAINT `FK_CAE207B2ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `ade_shop_supplier` (`id`),
  ADD CONSTRAINT `FK_CAE207B4584665A` FOREIGN KEY (`product_id`) REFERENCES `ade_shop_product` (`id`),
  ADD CONSTRAINT `FK_CAE207BA23B42D` FOREIGN KEY (`manufacturer_id`) REFERENCES `ade_shop_manufacturer` (`id`),
  ADD CONSTRAINT `FK_CAE207BF675F31B` FOREIGN KEY (`author_id`) REFERENCES `ade_shop_author` (`id`);

--
-- Limiti per la tabella `ade_shop_category`
--
ALTER TABLE `ade_shop_category`
  ADD CONSTRAINT `FK_27A8D26727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `ade_shop_category` (`id`) ON DELETE SET NULL;

--
-- Limiti per la tabella `ade_shop_product`
--
ALTER TABLE `ade_shop_product`
  ADD CONSTRAINT `FK_3EF8F62B2ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `ade_shop_supplier` (`id`),
  ADD CONSTRAINT `FK_3EF8F62BA23B42D` FOREIGN KEY (`manufacturer_id`) REFERENCES `ade_shop_manufacturer` (`id`),
  ADD CONSTRAINT `FK_3EF8F62BB2A824D8` FOREIGN KEY (`tax_id`) REFERENCES `ade_shop_tax` (`id`);

--
-- Limiti per la tabella `ade_shop_products_authors`
--
ALTER TABLE `ade_shop_products_authors`
  ADD CONSTRAINT `FK_E000EC2B4584665A` FOREIGN KEY (`product_id`) REFERENCES `ade_shop_product` (`id`),
  ADD CONSTRAINT `FK_E000EC2BF675F31B` FOREIGN KEY (`author_id`) REFERENCES `ade_shop_author` (`id`);

--
-- Limiti per la tabella `ade_shop_products_categories`
--
ALTER TABLE `ade_shop_products_categories`
  ADD CONSTRAINT `FK_F783976312469DE2` FOREIGN KEY (`category_id`) REFERENCES `ade_shop_category` (`id`),
  ADD CONSTRAINT `FK_F78397634584665A` FOREIGN KEY (`product_id`) REFERENCES `ade_shop_product` (`id`);

--
-- Limiti per la tabella `ade_shop_product_attribute`
--
ALTER TABLE `ade_shop_product_attribute`
  ADD CONSTRAINT `FK_3165DD214584665A` FOREIGN KEY (`product_id`) REFERENCES `ade_shop_product` (`id`),
  ADD CONSTRAINT `FK_3165DD21B6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `ade_shop_attribute` (`id`);

--
-- Limiti per la tabella `ade_shop_product_products`
--
ALTER TABLE `ade_shop_product_products`
  ADD CONSTRAINT `FK_CD3B0CEA4584665A` FOREIGN KEY (`product_id`) REFERENCES `ade_shop_product` (`id`),
  ADD CONSTRAINT `FK_CD3B0CEAD6E33971` FOREIGN KEY (`product_product_id`) REFERENCES `ade_shop_product` (`id`);

--
-- Limiti per la tabella `ade_shop_variant`
--
ALTER TABLE `ade_shop_variant`
  ADD CONSTRAINT `FK_1CF14D2B4584665A` FOREIGN KEY (`product_id`) REFERENCES `ade_shop_product` (`id`),
  ADD CONSTRAINT `FK_1CF14D2BB2A824D8` FOREIGN KEY (`tax_id`) REFERENCES `ade_shop_tax` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
