<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return array(
            new TwigFunction('filter_exist', array($this, 'checkIfFilterExist'), array('needs_environment' => true)),
            new TwigFunction('call_filter_if_it_exist', array($this, 'callFilterIfItExist'), array('needs_environment' => true))
        );
    }

    public function checkIfFilterExist(\Twig_Environment $env, $filter)
    {
        return $env->getFilter($filter) !== false;
    }

    public function callFilterIfItExist(\Twig_Environment $env, $input, $filter, $args = array())
    {
        $filter = $env->getFilter($filter);

        if ($filter === false) {
            return $input;
        }

        return $filter->getCallable()($input, $args);
    }

}