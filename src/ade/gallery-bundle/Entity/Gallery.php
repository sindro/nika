<?php
namespace AdeGalleryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="ade_gallery")
 * @ORM\Entity()
 */
class Gallery
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $title;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="gallery", cascade={"persist"}, orphanRemoval=true)
     */
    protected $images;

    /**
     * @ORM\Column(name="is_public", type="boolean", nullable=true)
     */
    protected $isPublic = true;

    use ORMBehaviors\Timestampable\Timestampable;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return arraycollection
     */
    public function getImages()
    {
        return $this->images;
    }

    public function setImages($images)
    {
        if (count($images) > 0) {
            foreach ($images as $image) {
                $this->addImage($image);
            }
        }
    }

    /**
     * @param Image $Image
     */
    public function addImage(Image $image)
    {
        $this->images[] = $image;

        $image->setGallery($this);
    }

    /**
     * Remove Image
     */
    public function removeImage(Image $image)
    {
        if (!$this->images->contains($image)) {
            return;
        }

        $this->images->removeElement($image);

        $image->setGallery(null);

    }

    /**
     * @return mixed
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * @param mixed $isPublic
     */
    public function setIsPublic($isPublic): void
    {
        $this->isPublic = $isPublic;
    }

    public function __toString()
    {
        return (string) $this->getTitle();
    }
}