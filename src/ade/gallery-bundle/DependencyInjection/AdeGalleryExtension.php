<?php

namespace AdeGalleryBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class AdeGalleryExtension extends Extension implements PrependExtensionInterface
{
    private $bundleConfigs;

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');

        $container->setParameter('ade_gallery.translation_domain', 'AdeGalleryBundle');

        $this->configureParameterEntity($container, $config);

        $bundles = $container->getParameter('kernel.bundles');

        if(isset($bundles['SonataAdminBundle'])) {
            $this->configureParameterAdmin($container, $config);
            $loader->load('admin.yaml');
        }
    }

    public function prepend(ContainerBuilder $container)
    {
        $bundles = $container->getParameter('kernel.bundles');

        if (isset($bundles['SonataAdminBundle'])) {
            $this->bundleConfigs['SonataAdminBundle'] = current($container->getExtensionConfig('sonata_admin'));

            $sonataAdminConfig['dashboard']['groups']['admin.gallery'] = [
                'label' => 'ade_gallery',
                'label_catalogue' => 'AdeGalleryBundle',
                'icon' => '<i class="fa fa-camera"></i>',
                'items' => ['admin.gallery']
            ];

            $container->prependExtensionConfig('sonata_admin', array_merge($this->bundleConfigs['SonataAdminBundle'], $sonataAdminConfig));
        }
    }
    

    public function configureParameterEntity(ContainerBuilder $container, array $config)
    {
        $container->setParameter('ade_gallery.entity.gallery', $config['entity']['gallery']);
        $container->setParameter('ade_gallery.entity.image', $config['entity']['image']);
    }

    public function configureParameterAdmin(ContainerBuilder $container, array $config)
    {
        $container->setParameter('ade_gallery.admin.gallery', $config['admin']['gallery']);
        $container->setParameter('ade_gallery.admin.image', $config['admin']['image']);
    }
}