<?php
namespace AdeGalleryBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ade_gallery');

        $this->addEntitySection($rootNode);
        $this->addAdminSection($rootNode);
        
        return $treeBuilder;
    }
    
    private function addEntitySection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->arrayNode('entity')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('gallery')->defaultValue('AdeGalleryBundle\\Entity\\Gallery')->end()
                        ->scalarNode('image')->defaultValue('AdeGalleryBundle\\Entity\\Image')->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    private function addAdminSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->arrayNode('admin')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('gallery')->defaultValue('AdeGalleryBundle\\Admin\\GalleryAdmin')->end()
                        ->scalarNode('image')->defaultValue('AdeGalleryBundle\\Admin\\ImageAdmin')->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
