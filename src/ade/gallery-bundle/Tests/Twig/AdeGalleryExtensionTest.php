<?php
namespace AdeGalleryBundle\Tests\Twig;

use AdeGalleryBundle\Entity\Gallery;
use AdeGalleryBundle\Twig\AdeGalleryExtension;
use Doctrine\Common\Persistence\ObjectManager;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityRepository;

class AdeGalleryExtensionTest extends TestCase
{
    private $em;
    private $templating;
    private $extension;

    public function setUp()
    {
        $this->templating = $this->getMockForAbstractClass('Symfony\Component\Templating\EngineInterface');

        $gallery = new Gallery();
        $gallery->setTitle('galleria');

        $repository = $this->getMockBuilder(EntityRepository::class)
            ->setMethods(['findOneBy'])
            ->disableOriginalConstructor()
            ->getMock();

        $repository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($gallery);

        $objectManager = $this->createMock(ObjectManager::class);

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($repository);

        $this->extension = new AdeGalleryExtension($objectManager, $this->templating);
    }

    public function testAdeGalleryIsAsset()
    {
        $filters = $this->extension->getFilters();

        $this->assertCount(1, $filters);
        $this->assertSame('ade_gallery', $filters[0]->getName());
    }

    public function testAdeGalleryFilter()
    {
        $loader = new \Twig_Loader_Filesystem(__DIR__ . '/../../Resources/views/');
        $twig = new \Twig_Environment($loader, array(
            'cache' => '/var/tmp/gallery-twig-tests-cache',
        ));

        $gallery = $this->getMockBuilder(Gallery::class)
            ->setMethods(['getId'])
            ->getMock();

        $gallery->expects($this->once())
                ->method('getId')
                ->will($this->returnValue(1));

        $this->assertSame('<div class="lightbox-gallery row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                <a href="/uploads/gallery/cielo.jpeg" data-lightbox="roadtrip">
                    <img src="/uploads/gallery/cielo.jpeg" alt="alt immagine">
                </a>
            </div>
            </div>', $twig->render('gallery.html.twig', ['gallery' => $gallery]));
    }
}