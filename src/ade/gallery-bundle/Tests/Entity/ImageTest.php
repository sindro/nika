<?php
namespace AdeGalleryBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;

class ImageTest extends TestCase
{
    public function testAlt()
    {
        $image = $this->getImage();
        $image->setAlt('alt immagine');

        $this->assertSame('alt immagine', $image->getAlt());
    }

    public function testIsPublic()
    {
        $image = $this->getImage();
        $image->setIsPublic(true);

        $this->assertTrue($image->getIsPublic());
    }

    public function testGallery()
    {
        $gallery = $this->getGallery();
        $image = $this->getImage();
        $image->setGallery($gallery);

        $this->assertSame($gallery, $image->getGallery());
    }

    public function getImage()
    {
        return $this->getMockForAbstractClass('AdeGalleryBundle\Entity\Image');
    }

    public function getGallery()
    {
        $gallery = $this->getMockForAbstractClass('AdeGalleryBundle\Entity\Gallery');
        $gallery->setTitle('galleria');

        return $gallery;
    }
}