<?php
namespace AdeGalleryBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;

class GalleryTest extends TestCase
{
    public function testTitle()
    {
        $gallery = $this->getGallery();
        $gallery->setTitle('titolo galleria');

        $this->assertSame('titolo galleria', $gallery->getTitle());
    }

    public function testAddImage()
    {
        $image = $this->getImage();

        $gallery = $this->getGallery();
        $gallery->setTitle('titolo galleria');
        $gallery->addImage($image);

        $this->assertSame(1, count($gallery->getImages()));
    }

    public function testSetImages()
    {
        $image = $this->getImage();
        $image2 = $this->getImage();

        $gallery = $this->getGallery();
        $gallery->setTitle('titolo galleria');
        $gallery->setImages([$image, $image2]);

        $this->assertSame(2, count($gallery->getImages()));
    }

    public function testRemoveImage()
    {
        $image = $this->getImage();
        $image2 = $this->getImage();

        $gallery = $this->getGallery();
        $gallery->setTitle('titolo galleria');
        $gallery->setImages([$image, $image2]);

        $gallery->removeImage($image);

        $this->assertSame(1, count($gallery->getImages()));
    }

    public function testIsPublic()
    {
        $gallery = $this->getGallery();
        $gallery->setIsPublic(true);

        $this->assertTrue($gallery->getIsPublic());
    }

    public function getGallery()
    {
        return $this->getMockForAbstractClass('AdeGalleryBundle\Entity\Gallery');
    }

    public function getImage()
    {
        $image = $this->getMockForAbstractClass('AdeGalleryBundle\Entity\Image');
        $image->setAlt('alt-image');
        $image->setImageName('name-image');

        return $image;
    }
}