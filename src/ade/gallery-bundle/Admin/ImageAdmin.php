<?php
namespace AdeGalleryBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ImageAdmin extends AbstractAdmin
{
    protected $parentAssociationMapping = 'gallery';

    public function getParentAssociationMapping()
    {
        return 'gallery';
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        if ($this->isChild()) {
            return;
        }

        $collection->clear();
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('imageName', null,array(
                'label' => 'form.image',
                'template' => '@AdeGallery/Admin/_image.html.twig'
            ))
            ->add('alt', null, array(
                'label' => 'form.alt'
            ))
            ->add('_action', 'actions', array(
                'label' => 'form.actions',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('alt', null, array(
                'label' => 'form.alt'
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('alt', null, array(
                'label' => 'form.alt'
            ))
            ->add('imageFile', VichImageType::class, [
                'label' => 'form.image',
                'required' => false,
                'allow_delete' => true,
                'download_uri' => true,
                'image_uri' => true
            ])
            ->add('isPublic', null, array(
                'label' => 'form.isPublic'
            ));
    }
}