<?php
namespace AdeGalleryBundle\Twig;

use AdeGalleryBundle\Entity\Gallery;
use Doctrine\Common\Persistence\ObjectManager;
use Twig\Extension\AbstractExtension;
use Thunder\Shortcode\HandlerContainer\HandlerContainer;
use Thunder\Shortcode\Parser\RegularParser;
use Thunder\Shortcode\Processor\Processor;
use Thunder\Shortcode\Shortcode\ShortcodeInterface;
use Symfony\Component\Templating\EngineInterface;
use Twig\TwigFilter;

class AdeGalleryExtension extends AbstractExtension
{
    private $em;
    private $templating;

    public function __construct(ObjectManager $em, EngineInterface $templating)
    {
        $this->em = $em;
        $this->templating = $templating;
    }

    public function getFilters()
    {
        return array(
            new TwigFilter('ade_gallery', array($this, 'adeGalleryFilter'))
        );
    }

    public function adeGalleryFilter($text)
    {
        $repository = $this->em->getRepository(Gallery::class);

        $handlers = new HandlerContainer();

        $handlers->add('gallery', function(ShortcodeInterface $s) use ($repository) {
            $gallery = $repository->findOneBy(['id' => $s->getParameter('id')]);
            if(!$gallery)
            {
                return 'Gallery not exist';
            }

            return $this->templating->render('@AdeGallery/gallery.html.twig', ['gallery' => $gallery, 'type' => $s->getParameter('type')]);
        });

        $processor = new Processor(new RegularParser(), $handlers);

        return $processor->process($text);
    }

}