<?php

namespace AdeTreeSonataBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use AdeTreeSonataBundle\DependencyInjection\Compiler\ChangeAdminControllerCompilerPass;

class AdeTreeSonataBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ChangeAdminControllerCompilerPass());
    }
}
