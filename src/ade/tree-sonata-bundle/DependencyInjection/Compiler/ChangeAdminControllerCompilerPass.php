<?php

namespace AdeTreeSonataBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ChangeAdminControllerCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $mappings = $container->getParameter('ade_tree_sonata.mappings');

        foreach($mappings as $mapping) {
            $definition = $container->getDefinition($mapping);
            $definition->replaceArgument(2, 'AdeTreeSonataBundle:Admin/TreeAdmin');
        }
    }
}