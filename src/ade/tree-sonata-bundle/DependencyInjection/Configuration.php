<?php
namespace AdeTreeSonataBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ade_tree_sonata');

        $rootNode->children()
            ->arrayNode('mappings')
            ->prototype('scalar')
            ->end()
            ->end()
            ->end();
        return $treeBuilder;
    }
}
