<?php
namespace AdeTreeSonataBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormRenderer;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TreeAdminController extends Controller
{
    public function listAction(Request $request = null)
    {
        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }

        $datagrid = $this->admin->getDatagrid();

        if ($this->admin->getPersistentParameter('context')) {
            $datagrid->setValue('context', null, $this->admin->getPersistentParameter('context'));
        }

        $formView = $datagrid->getForm()->createView();

        $this->get('twig')->getRuntime(FormRenderer::class)->setTheme($formView, $this->admin->getFilterTheme());

        return $this->render('@AdeTreeSonata/Admin/list.html.twig', array(
            'action'     => 'list',
            'form'       => $formView,
            'datagrid'   => $datagrid,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ));
    }

    public function treeAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository($this->admin->getClass());

        if(! method_exists($repository, 'findRootNodes')) {
            throw new NotFoundHttpException('Function findRootNodes not exist, please create it in ' . $this->admin->getClass() . ' repository');
        }

        $objects = $this->getDoctrine()->getRepository($this->admin->getClass())->findRootNodes(false);

        $datagrid = $this->admin->getDatagrid();

        $formView = $datagrid->getForm()->createView();

        $this->get('twig')->getRuntime(FormRenderer::class)->setTheme($formView, $this->admin->getFilterTheme());

        return $this->render('@AdeTreeSonata/Admin/tree.html.twig', array(
            'action'           => 'tree',
            'objects'       => $objects,
            'form'             => $formView,
            'csrf_token'       => $this->getCsrfToken('sonata.batch'),
        ));
    }

    public function jsonTreeAction(Request $request)
    {
        $json = $this->cleanJson($request->get('json'));

        if (is_array($json)) {
            for ($i = 1; $i < count($json['data']); $i++) {
                $repository = $this->getDoctrine()->getRepository($this->admin->getClass());

                $em = $this->container->get('doctrine')->getManager();

                $object = $repository->findOneById($json['data'][$i]['id']);

                $object->setLft($json['data'][$i]['left']);
                $object->setRgt($json['data'][$i]['right']);
                $object->setLvl($json['data'][$i]['depth']);

                if (isset($json['data'][$i]['parent_id'])) {
                    $parent = $repository->findOneById($json['data'][$i]['parent_id']);
                    $object->setParent($parent);
                } else {
                    $object->setParent(null);
                }

                $em->persist($object);
                $em->flush();
            }

            echo $this->getJsonResponse('success');
            exit();
        }

        echo $this->getJsonResponse('error');
        exit();
    }

    private function getJsonResponse($type)
    {
        return json_encode(array(
            'type' => $type,
            'message' => $this->get('translator')->trans(
                'ade_tree_sonata.admin.flash.' . $type . '_tree',
                array(),
                'AdeTreeSonataBundle')
        ));
    }

    private function cleanJson($data)
    {
        $create_json_1 = str_replace(',}', '}', $data);
        $create_json_2 = str_replace(',]', ']', $create_json_1);
        $create_json_3 = str_replace('{},', "", $create_json_2);
        $create_json_4 = str_replace('none', '"none"', $create_json_3);

        return json_decode($create_json_4, true);
    }
}