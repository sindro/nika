 $(document).ready(function(){
    var init_dumped_text = '{"data":[';
    var close_dumped_text = ']}';
    var url = $('.nested-url').data('url');

    $('ol.sortable').nestedSortable({
        handle: 'div',
        items: 'li',
        toleranceElement: '> div',
        maxLevels: 2,
        stop: function(event, ui)
        {
            arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
            arraied = init_dumped_text + dump(arraied) + close_dumped_text;
            $.ajax({
                type: 'POST',
                url: url,
                data: { 
                    'json': arraied,
                },
                success: function(response) {
                    var object = $.parseJSON(response);
                    $('.alert').remove();
                    var $before = $('.box-primary');
                    $('<div class="alert alert-'+object.type+' alert-dismissable">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+object.message+'' +
                    '</div>').insertBefore($before);
                }
            });
        }
    });

    function dump(arr,level) {
        var dumped_text = '';
        if(!level) level = 0;

        if(typeof(arr) == 'object'){
            for(var item in arr){
                var value = arr[item];

                if(typeof(value) == 'object'){
                    dumped_text += '{';
                    dumped_text += dump(value,level+1);
                    dumped_text += '},';
                } else {
                    dumped_text += '"' + item + '": ' + value +',';
                }
            }
        }else{
            dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
        }

        return dumped_text;
    }
});
