<?php
namespace AdeShopBundle\EventListener;

use AdeShopBundle\Entity\CatalogRule;
use AdeShopBundle\Entity\Variant;
use Doctrine\ORM\Event\LifecycleEventArgs;
use AdeShopBundle\Price\EngineInterface;

class ProductListener
{
    private $engine;

    public function __construct(EngineInterface $engine)
    {
        $this->engine = $engine;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        if (!$args->getEntity() instanceof Variant) {
            return;
        }

        $variant = $args->getEntity();

        $ruleWithMaxDiscount = $args->getEntityManager()
                             ->getRepository(CatalogRule::class)
                             ->findBetterDiscountRule($variant);

        $rules = count($ruleWithMaxDiscount) > 0 ?
            $ruleWithMaxDiscount[0] :
            ['discount' => null, 'floatDiscount' => null, 'startDate' => null, 'endDate' => null];

        $this->engine->addDiscountRules("rangeDate(date('now'), catalogRule) ? catalogRule['floatDiscount'] : (dateAreNull(catalogRule) ? catalogRule['floatDiscount'] : 0)");

        $price = $this->engine->calculatePrice($variant, $rules);

        $variant->setCurrentDiscount(isset($rules['discount']) ? $rules['discount']: 0);
        $variant->setCurrentPrice($price);
    }

}