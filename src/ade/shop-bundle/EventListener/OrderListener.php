<?php

namespace AdeShopBundle\EventListener;

use AdeShopBundle\Entity\PaymentDetails;
use AdeShopBundle\Event\GetResponseOrderEvent;
use AdeShopBundle\Util\ManageStockInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Payum\Core\Payum;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;

class OrderListener implements EventSubscriberInterface
{
    private $em;
    private $payum;
    private $router;

    public function __construct(ObjectManager $em, Payum $payum, RouterInterface $router)
    {
        $this->em = $em;
        $this->payum = $payum;
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return array(
            'ade_shop.order.token' => 'token',
        );
    }

    public function token(GetResponseOrderEvent $event)
    {
        $order = $event->getOrder();

        $captureToken = $order->getPaymentMethod() == 'Paypal' ?
                            $this->getGatewayPaypal($order) :
                            $this->getGatewayOffline($order);

        $event->setResponse(new RedirectResponse($captureToken->getTargetUrl()));
    }

    private function getGatewayOffline($order)
    {
        $order->setStatus('pending_payment');
        $order->setDetails([]);
        $this->em->persist($order);
        $this->em->flush();

        return $this->getCaptureToken('offline', $order,'ade_payment_done');
    }

    private function getGatewayPaypal($order)
    {
        $storage = $this->payum->getStorage(PaymentDetails::class);

        $payment = $storage->create();
        $payment['PAYMENTREQUEST_0_CURRENCYCODE'] = 'EUR';
        $payment['PAYMENTREQUEST_0_AMT'] = $order->getAmount();

        $storage->update($payment);

        $order->setStatus('on_hold');
        $order->setDetails([]);
        $this->em->persist($order);
        $this->em->flush();

        $captureToken = $this->getCaptureToken('paypal_express_checkout',$payment,'ade_payment_paypal_done');

        $payment['INVNUM'] = $order->getId();
        $storage->update($payment);

        return $captureToken;
    }

    private function getCaptureToken($gatewayName, $model, $path)
    {
        return $this->payum->getTokenFactory()->createCaptureToken($gatewayName, $model, $path);
    }
}
