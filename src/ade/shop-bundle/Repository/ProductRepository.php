<?php
namespace AdeShopBundle\Repository;

use AdeShopBundle\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findProducts($category, $manufacturer = null, $author = null, $query = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.categories', 'product_categories')
            ->leftJoin('p.manufacturer', 'product_manufacturer')
            ->leftJoin('p.authors', 'product_authors')
            ->addSelect('product_manufacturer')
            ->addSelect('product_authors');

        if($manufacturer) {
            $qb->andWhere($qb->expr()->like('product_manufacturer.slug', ':manufacturer_slug'))->setParameter('manufacturer_slug', $manufacturer->getSlug());
        }

        if($author) {
            $qb->andWhere($qb->expr()->like('product_authors.slug', ':author_slug'))->setParameter('author_slug', $author->getSlug());
        }

        $qb->andWhere($qb->expr()->like('product_categories.slug', ':category_slug'))->setParameter('category_slug', $category->getSlug());

        if($query) {
            $qb->andWhere($qb->expr()->like('p.title', ':product_title'))->setParameter('product_title', '%' . $query . '%');
        }

        $qb->addOrderBy('p.createdAt');

        return $qb->getQuery()->getResult();
    }

    public function findProductWithMinPrice()
    {
        $sql = "SELECT LEAST(IFNULL(MIN(p.price), 9999999), IFNULL(MIN(v.price), 9999999))
                FROM AdeShopBundle:Product p 
                LEFT JOIN p.variants AS v";
        $qb = $this->getEntityManager()->createQuery($sql);

        return $qb->getSingleScalarResult();
    }

    public function findProductWithMaxPrice()
    {
        $sql = "SELECT GREATEST(IFNULL(MAX(p.price), 0), IFNULL(MAX(v.price),0))
                FROM AdeShopBundle:Product p 
                LEFT JOIN p.variants AS v";
        $qb = $this->getEntityManager()->createQuery($sql);

        return $qb->getSingleScalarResult();
    }

    public function findByIds(array $ids)
    {
        if (empty($ids)) {
            return array();
        }

        $qb = $this->createQueryBuilder('p')
            ->add('from', 'AdeShopBundle:Product p INDEX BY p.id');

        $qb->where($qb->expr()->in('p.id', $ids));

        return $qb->getQuery()->getResult();
    }
}