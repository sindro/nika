<?php
namespace AdeShopBundle\Repository;

use AdeShopBundle\Entity\Variant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class VariantRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Variant::class);
    }

    public function findByIds(array $ids)
    {
        if (empty($ids)) {
            return array();
        }

        $qb = $this->createQueryBuilder('v')
                ->add('from', 'AdeShopBundle:Variant v INDEX BY v.id');

        $qb->where($qb->expr()->in('v.id', $ids));

        return $qb->getQuery()->getResult();
    }

    public function findVariantByOptions($options, $slug, $variant_id)
    {
        $qb = $this->createQueryBuilder('v')
            ->leftJoin('v.options', 'vo')
            ->leftJoin('v.product', 'vp')
            ->addSelect('vo')
            ->addSelect('vp');
        $qb->andWhere($qb->expr()->like('vp.slug', ':p_slug'))->setParameter('p_slug', $slug);
        $qb->andWhere($qb->expr()->like('v.id', ':id'))->setParameter('id', $variant_id);
        $variants = explode('-', $options);

        $qb->andWhere($qb->expr()->in('vo.title', ':opt'))->setParameter('opt', $variants);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function findAllByStock()
    {
        $qb = $this->createQueryBuilder('v');

        return $qb->where($qb->expr()->gt('v.inStock', ':inStock'))->setParameter('inStock', 0)
                    ->getQuery()
                    ->getResult();
    }

    public function orderBySort()
    {
        return $this->createQueryBuilder('v')
            ->orderBy('v.sort', 'ASC');
    }
}