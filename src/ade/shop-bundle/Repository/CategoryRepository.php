<?php
namespace AdeShopBundle\Repository;

use AdeShopBundle\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function findRootNodes($isPublic = true)
    {
        $qb = $this->createQueryBuilder('pc');

        $qb->where($qb->expr()->isNull('pc.parent'));

        if ($isPublic) {
            $qb->andWhere($qb->expr()->like('pc.isPublic', ':ip'))->setParameter('ip', 1);
        }

        $qb->orderBy('pc.lft', 'ASC');

        return $qb->getQuery()->getResult();
    }
}
