<?php
namespace AdeShopBundle\Repository;

use AdeShopBundle\Entity\CatalogRule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CatalogRuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CatalogRule::class);
    }

    public function findBetterDiscountRule($variant)
    {
        $qb = $this->createQueryBuilder('cr')
                   ->select('cr.discount', 'cr.startDate', 'cr.endDate', '(cr.discount/100) as floatDiscount');

            $qb->leftJoin('cr.product', 'p');
            $qb->orWhere($qb->expr()->like('p.id', ':product'))->setParameter('product', $variant->getProduct()->getId());

            $qb->leftJoin('cr.category', 'c');
            $qb->orWhere($qb->expr()->in('c.id', ':categories'))->setParameter('categories', $variant->getProduct()->getCategories());

            $qb->leftJoin('cr.author', 'a');
            $qb->orWhere($qb->expr()->in('a.id', ':authors'))->setParameter('authors', $variant->getProduct()->getAuthors());

            $qb->leftJoin('cr.manufacturer', 'm');
            $qb->orWhere($qb->expr()->like('m.id', ':manufacturer'))->setParameter('manufacturer', $variant->getProduct()->getManufacturer());

            $qb->leftJoin('cr.supplier', 's');
            $qb->orWhere($qb->expr()->like('s.id', ':supplier'))->setParameter('supplier', $variant->getProduct()->getSupplier());

            $qb->andWhere(':today BETWEEN COALESCE(cr.startDate, 1900-01-01) AND COALESCE(cr.endDate, 3000-01-01)')
                ->setParameter('today', date('Y-m-d'));

            $qb->orderBy('cr.discount', 'DESC');

        return $qb->setMaxResults(1)->getQuery()->getResult();
    }
}