<?php
namespace AdeShopBundle\Repository;

use AdeShopBundle\Entity\ComboProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ComboProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ComboProduct::class);
    }

    public function findByIds(array $ids)
    {
        if (empty($ids)) {
            return array();
        }

        $qb = $this->createQueryBuilder('cp')
            ->add('from', 'AdeShopBundle:ComboProduct cp INDEX BY cp.id');

        $qb->where($qb->expr()->in('cp.id', $ids));

        return $qb->getQuery()->getResult();
    }
}