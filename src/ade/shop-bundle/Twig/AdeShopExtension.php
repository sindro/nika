<?php
namespace AdeShopBundle\Twig;

use AdeShopBundle\Entity\ComboProduct;
use AdeShopBundle\Entity\Product;
use AdeShopBundle\Entity\Variant;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Twig\TwigFilter;
use Symfony\Component\Templating\EngineInterface;
use Thunder\Shortcode\HandlerContainer\HandlerContainer;
use Thunder\Shortcode\Parser\RegularParser;
use Thunder\Shortcode\Processor\Processor;
use Thunder\Shortcode\Shortcode\ShortcodeInterface;

class AdeShopExtension extends AbstractExtension
{
    private $uploadHelper;
    private $translator;
    private $em;
    private $templating;

    public function __construct(UploaderHelper $uploaderHelper, TranslatorInterface $translator, EntityManagerInterface $em, EngineInterface $templating)
    {
        $this->uploadHelper = $uploaderHelper;
        $this->translator = $translator;
        $this->em = $em;
        $this->templating = $templating;
    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('finalPrice', array($this, 'finalPriceFunction')),
            new TwigFunction('discount', array($this, 'discountFunction')),
            new TwigFunction('provideVariantsPrices', array($this, 'provideVariantsPricesFunction'))
        );
    }

    public function getFilters()
    {
        return array(
            new TwigFilter('ade_combo_product', array($this, 'adeComboProductFilter'))
        );
    }


    public function finalPriceFunction($object)
    {
        $currentPrice = $object->getCurrentPriceWithTax();
        $price = $object->getPriceWithTax();
        if($currentPrice == $price)
        {
            return '<ins><span class="amount">' . twig_localized_currency_filter($price, 'EUR') . '</span></del>';
        }

        return '<ins><span class="amount">' . twig_localized_currency_filter($currentPrice, 'EUR') . '</span></ins> 
            <del><span class="amount">' . twig_localized_currency_filter($price, 'EUR') . '</span></del>';
    }

    public function discountFunction($object)
    {
        if($object->getCurrentPriceWithTax() != $object->getPriceWithTax())
        {
            return '(-' . $object->getCurrentDiscount() . '%)';
        }
    }

    public function provideVariantsPricesFunction(Product $product): array
    {
        $variantsPrices = [];

        foreach ($product->getVariants() as $variant) {
            $variantsPrices[] = $this->constructOptionsMap($variant);
        }

        return $variantsPrices;
    }

    private function constructOptionsMap(Variant $variant): array
    {
        $optionMap = [];

        foreach ($variant->getOptionValues() as $option) {
            $optionMap[$option->getOptionCode()] = $option->getCode();
        }

        $optionMap['currentPrice'] = $variant->getCurrentPriceWithTax();
        $optionMap['price'] = $variant->getPriceWithTax();
        $optionMap['discount'] = $variant->getCurrentDiscount();
        $optionMap['sku'] = $variant->getSku();
        $optionMap['shortDescription'] = $variant->getShortDescription();
        $optionMap['inStock'] = $variant->getInStock();
        $optionMap['saving'] = $this->translator->trans('saving', ['%saving%' => twig_localized_currency_filter($variant->getSaving(), 'EUR')], 'AdeShopBundle');
        $optionMap['tax'] = $variant->getTax() ? $this->translator->trans('price_tax',['%title%' => $variant->getTax()], 'AdeShopBundle') : '';
        $optionMap['specifications'] = json_encode($variant->getSpecifications(), true);
        $optionMap['variantId'] = $variant->getId();

        $image = $variant->getImageFile();
        $objImage = $variant;

        $gallery = $variant->getGallery();

        $images = [];

        if(null !== $gallery) {
            foreach ($gallery->getImages() as $image) {
                $images[] = ['image' => $this->uploadHelper->asset($image, 'imageFile'), 'alt' => $image->getAlt()];
            }
        } else if($image) {
            $images[] = ['image' => $this->uploadHelper->asset($objImage, 'imageFile'), 'alt' => $variant->getProduct()->getTitle()];
        }

        $optionMap['images'] = json_encode($images, true);

        return $optionMap;
    }

    public function adeComboProductFilter($text)
    {
        $repository = $this->em->getRepository(ComboProduct::class);

        $handlers = new HandlerContainer();

        $handlers->add('combo_product', function(ShortcodeInterface $s) use ($repository) {
            $comboProduct = $repository->findOneBy(['id' => $s->getParameter('id')]);
            if(!$comboProduct)
            {
                return 'Combo Product not exist';
            }

            return $this->templating->render('@AdeShop/Shortcode/combo_product.html.twig', ['comboProduct' => $comboProduct]);
        });

        $processor = new Processor(new RegularParser(), $handlers);

        return $processor->process($text);
    }
}