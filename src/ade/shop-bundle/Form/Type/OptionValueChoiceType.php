<?php

namespace AdeShopBundle\Form\Type;

use AdeShopBundle\Entity\Option;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class OptionValueChoiceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'choices' => function (Options $options) {
                    return $options['option']->getValues();
                },
                'choice_value' => 'code',
                'choice_label' => 'title',
                'choice_translation_domain' => false,
            ])
            ->setRequired([
                'option',
            ])
            ->addAllowedTypes('option', [Option::class])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
