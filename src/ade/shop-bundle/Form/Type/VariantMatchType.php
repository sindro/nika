<?php
namespace AdeShopBundle\Form\Type;

use AdeShopBundle\Form\DataTransformer\VariantToProductOptionsTransformer;
use AdeShopBundle\Entity\Product;
use AdeShopBundle\Entity\Option;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class VariantMatchType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer(new VariantToProductOptionsTransformer($options['product']));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'entries' => function (Options $options) {
                    $product = $options['product'];

                    return $product->getOptions();
                },
                'entry_type' => OptionValueChoiceType::class,
                'entry_name' => function (Option $productOption) {
                    return $productOption->getCode();
                },
                'entry_options' => function (Option $productOption) {
                    return [
                        'label' => $productOption->getTitle(),
                        'option' => $productOption,
                        'translation_domain' => false,
                    ];
                },
            ])

            ->setRequired('product')
            ->setAllowedTypes('product', Product::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return FixedCollectionType::class;
    }
}
