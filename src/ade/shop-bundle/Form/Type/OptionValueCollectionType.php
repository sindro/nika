<?php

namespace AdeShopBundle\Form\Type;

use AdeShopBundle\Entity\Option;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webmozart\Assert\Assert;


final class OptionValueCollectionType extends AbstractType
{
    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException
     * @throws InvalidConfigurationException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->assertOptionsAreValid($options);

        foreach ($options['options'] as $i => $option) {
            if (!$option instanceof Option) {
                throw new InvalidConfigurationException(
                    sprintf('Each object passed as option list must implement "%s"', Option::class)
                );
            }

            $builder->add((string) $option->getCode(), OptionValueChoiceType::class, [
                'label' => $option->getTitle(),
                'option' => $option,
                'choice_translation_domain' => false,
                'property_path' => '[' . $i . ']',
                'block_name' => 'entry',
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'options' => null,
            ])
        ;
    }


    /**
     * @param mixed $options
     *
     * @throws \InvalidArgumentException
     */
    private function assertOptionsAreValid($options): void
    {
        Assert::true(
            isset($options['options']) && is_iterable($options['options'])
        );
    }
}
