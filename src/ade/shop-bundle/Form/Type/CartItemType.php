<?php
namespace AdeShopBundle\Form\Type;

use AdeShopBundle\Entity\Product;
use AdeShopBundle\Entity\Variant;
use AdeShopBundle\Form\DataTransformer\VariantToIdTransformer;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotEqualTo;

class CartItemType extends AbstractType
{
    private $transformer;

    public function __construct(VariantToIdTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if($options['product']->getContext() == 'simple')
        {
            if($options['product']->getVariants()->count() > 1)
            {
                $builder->add('variant', EntityType::class,[
                    'label' => 'Option',
                    'choice_translation_domain' => false,
                    'translation_domain' => 'AdeShopBundle',
                    'class' => Variant::class,
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder('v')
                            ->where('v.product = ?1')
                            ->setParameter('1', $options['product'])
                            ->orderBy('v.price', 'ASC');
                    },
                ]);
            } else {
                $builder->add('variant', HiddenType::class,[
                    'data' => $options['product']->getVariants()->first(),
                    'data_class' => null
                ]);

                $builder->get('variant')->addModelTransformer($this->transformer);
            }
        } else {
            $builder->add('variant', VariantMatchType::class, ['product' => $options['product']]);
        }

        $builder->add('quantity', QuantityType::class, array(
                'label' => '',
                'required' => false,
                'data' => 1,
                'min' => 0,
                'constraints' => array(
                    new NotBlank(),
                    new NotEqualTo(array('value' => 0))
                )
            ))
            ->add('add', SubmitType::class, array(
                'label' => 'add_to_cart',
                'translation_domain' => 'AdeShopBundle',
                'disabled' => true
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver
            ->setDefined([
                'product',
            ])
            ->setAllowedTypes('product', Product::class)
        ;
    }
}
