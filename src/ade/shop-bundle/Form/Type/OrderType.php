<?php
namespace AdeShopBundle\Form\Type;

use AdeShopBundle\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', null, [
                'label' => 'form.email',
                'translation_domain' => 'AdeShopBundle'
            ])
            ->add('firstname', null, [
                'label' => 'form.firstname',
                'translation_domain' => 'AdeShopBundle'
            ])
            ->add('lastname', null, [
                'label' => 'form.lastname',
                'translation_domain' => 'AdeShopBundle'
            ])
            ->add('company', null, [
                'label' => 'form.company',
                'translation_domain' => 'AdeShopBundle'
            ])
            ->add('vat', null, [
                'label' => 'form.vat',
                'translation_domain' => 'AdeShopBundle'
            ])
            ->add('address', null, [
                'label' => 'form.address',
                'translation_domain' => 'AdeShopBundle'
            ])
            ->add('postalCode', null, [
                'label' => 'form.postalCode',
                'translation_domain' => 'AdeShopBundle'
            ])
            ->add('city', null, [
                'label' => 'form.city',
                'translation_domain' => 'AdeShopBundle'
            ])
            ->add('country', null, [
                'label' => 'form.country',
                'translation_domain' => 'AdeShopBundle'
            ])
            ->add('fiscalCode', null, [
                'label' => 'form.fiscalCode',
                'translation_domain' => 'AdeShopBundle'
            ])
            ->add('phone', null, [
                'label' => 'form.phone',
                'translation_domain' => 'AdeShopBundle'
            ])
            ->add('createUser', CheckboxType::class, [
                'label' => 'form.createUser',
                'translation_domain' => 'AdeShopBundle',
                'data' => false,
                'mapped' => false
            ])
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'options' => ['translation_domain' => 'FOSUserBundle'],
                'first_options' => ['label' => 'form.password', 'attr' => ['class' => 'input-text']],
                'second_options' => ['label' => 'form.password_confirmation', 'attr' => ['class' => 'input-text']],
                'invalid_message' => 'fos_user.password.mismatch',
                'mapped' => false,
                'translation_domain' => 'FOSUserBundle',
                'constraints' => [
                    new Assert\NotBlank(['groups' => ['password_required']])
                ],
                'attr' => ['style' => 'display:none']
            ))
            ->add('note', null, [
                'label' => 'form.note',
                'translation_domain' => 'AdeShopBundle'
            ])
            ->add('paymentMethod', null, array(
                'label' => 'form.paymentMethod',
                'translation_domain' => 'AdeShopBundle',
                'expanded' => true,
                'multiple' => false,
                'placeholder' => false,
                'choice_translation_domain' => false,
            ))
            ->add('terms', CheckboxType::class, [
                'label' => 'form.terms',
                'data' => true,
                'translation_domain' => 'AdeShopBundle',
                'mapped' => false,
                'constraints' => [new Assert\NotBlank()],
            ])
            ->add('add', SubmitType::class, array(
                'label' => 'proceed_checkout',
                'translation_domain' => 'AdeShopBundle',
            ));
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
            'validation_groups' => function(FormInterface $form)
            {
                if($form->get('createUser')->getData() == 1)
                {
                    return array('Default', 'unique_email', 'password_required');
                }

                return array('Default');
            }
        ]);
    }
}