<?php

namespace AdeShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class QuantityType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['step'] = $options['step'];
        $view->vars['min'] = $options['min'];
        $view->vars['size'] = $options['size'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'step' => 1,
            'min' => 1,
            'size' => 4,
        ));
    }

    public function getParent()
    {
        return IntegerType::class;
    }
}
