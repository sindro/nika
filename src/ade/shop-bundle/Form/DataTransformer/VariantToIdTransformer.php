<?php
namespace AdeShopBundle\Form\DataTransformer;

use AdeShopBundle\Entity\Variant;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class VariantToIdTransformer implements DataTransformerInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function transform($variant)
    {
        if (null === $variant) {
            return '';
        }

        return $variant->getId();
    }

    public function reverseTransform($variantId)
    {
        if (!$variantId) {
            return;
        }

        $variant = $this->em->getRepository(Variant::class)->findOneBy([ 'id' => $variantId]);

        if (null === $variantId) {
            throw new TransformationFailedException(sprintf(
                'An variant with id "%s" does not exist!',
                $variantId
            ));
        }

        return $variant;
    }
}
