<?php
namespace AdeShopBundle\Form\DataTransformer;

use AdeShopBundle\Entity\Product;
use AdeShopBundle\Entity\OptionValue;
use AdeShopBundle\Entity\Variant;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

final class VariantToProductOptionsTransformer implements DataTransformerInterface
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * {@inheritdoc}
     *
     * @throws UnexpectedTypeException
     */
    public function transform($value): array
    {
        if (null === $value) {
            return [];
        }

        if (!$value instanceof Variant) {
            throw new UnexpectedTypeException($value, Variant::class);
        }

        return array_combine(
            array_map(function (OptionValue $productOptionValue) {
                return $productOptionValue->getOptionCode();
            }, $value->getOptionValues()->toArray()),
            $value->getOptionValues()->toArray()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value): ?Variant
    {
        if (null === $value || '' === $value) {
            return null;
        }

        if (!is_array($value) && !$value instanceof \Traversable && !$value instanceof \ArrayAccess) {
            throw new UnexpectedTypeException($value, '\Traversable or \ArrayAccess');
        }

        return $this->matches($value);
    }

    /**
     * @param OptionValue[] $optionValues
     *
     * @return Variant|null
     *
     * @throws TransformationFailedException
     */
    private function matches(array $optionValues): ?Variant
    {
        foreach ($this->product->getVariants() as $variant) {
            foreach ($optionValues as $optionValue) {
                if (null === $optionValue || !$variant->hasOptionValue($optionValue)) {
                    continue 2;
                }
            }

            return $variant;
        }

        throw new TransformationFailedException(sprintf(
            'Variant "%s" not found for product %s',
            !empty($optionValues[0]) ? $optionValues[0]->getCode() : '',
            $this->product->getSku()
        ));
    }
}
