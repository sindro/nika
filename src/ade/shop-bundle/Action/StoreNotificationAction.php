<?php
namespace AdeShopBundle\Action;

use AdeShopBundle\Entity\Author;
use Payum\Core\Action\ActionInterface;
use Payum\Core\Request\Notify;
use Symfony\Bridge\Doctrine\RegistryInterface;

class StoreNotificationAction implements ActionInterface
{
    /**
     * @var RegistryInterface
     */
    protected $doctrine;
    /**
     * @param RegistryInterface $doctrine
     */
    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }
    /**
     * {@inheritDoc}
     *
     * @param Notify $request
     */
    public function execute($request)
    {
        $author = new Author();
        $author->setTitle('ciao');
        $this->doctrine->getManager()->persist($author);
        $this->doctrine->getManager()->flush();
    }
    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return $request instanceof Notify;
    }
}