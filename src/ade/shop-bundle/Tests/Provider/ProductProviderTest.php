<?php
namespace AdeShopBundle\Tests\Provider;

use AdeShopBundle\Util\Cart;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;
use AdeShopBundle\Provider\ProductProvider;

class ProductProviderTest extends TestCase
{
    private $provider;
    private $session;
    private $cart;

    public function setUp()
    {
        $this->session = $this->getMockBuilder('Symfony\Component\HttpFoundation\Session\SessionInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->cart = new Cart($this->session);
    }

    public function testFindInCart()
    {
        $variant = $this->getMockBuilder('AdeShopBundle\Entity\Variant')
            ->disableOriginalConstructor()
            ->getMock();

        $variant->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $this->session->expects($this->at(1))
            ->method('set')
            ->will;

        $variantRepository = $this
            ->getMockBuilder(EntityRepository::class)
            ->setMethods(['findByIds'])
            ->disableOriginalConstructor()
            ->getMock();

        $variantRepository->expects($this->any())
            ->method('findByIds')
            ->willReturn($variant);

        $objectManager = $this->createMock(ObjectManager::class);
        $objectManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($variantRepository);

        $this->provider = new ProductProvider($objectManager, $this->cart);

        $this->assertSame(count($this->provider->findInCart()), count($this->cart->get()));
    }
}