<?php
namespace AdeShopBundle\Tests\Form\Type;


use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\Extension\Validator\Type\FormTypeValidatorExtension;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Bridge\Doctrine\Test\DoctrineTestHelper;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension;

/**
 * Test case used to test form types with entity field
 *
 * @author Nicolas MACHEREY <nicolas.macherey@gmail.com>
 */
abstract class FormTypeTestCase extends TypeTestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @bar ManagerRegistry
     */
    private $emRegistry;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->em = DoctrineTestHelper::createTestEntityManager();
        $this->emRegistry = $this->createRegistryMock('default', $this->em);

        parent::setUp();

        $this->createSchema();

        $validator = $this->getMock('\Symfony\Component\Validator\ValidatorInterface');
        $validator->expects($this->any())->method('validate')->will($this->returnValue(new ConstraintViolationList()));

        $this->factory = Forms::createFormFactoryBuilder()
            ->addExtensions($this->getExtensions())
            ->addTypeExtension(
                new FormTypeValidatorExtension(
                    $validator
                )
            )
            ->addTypeGuesser(
                $this->getMockBuilder(
                    'Symfony\Component\Form\Extension\Validator\ValidatorTypeGuesser'
                )
                    ->disableOriginalConstructor()
                    ->getMock()
            )
            ->getFormFactory();

        $this->dispatcher = $this->getMock('Symfony\Component\EventDispatcher\EventDispatcherInterface');
        $this->builder = new FormBuilder(null, null, $this->dispatcher, $this->factory);

    }

    /**
     * Create the schema that will be used for testing
     */
    protected function createSchema()
    {
        $schemaTool = new SchemaTool($this->em);
        $classes = [];

        foreach ($this->getEntities() as $entityClass) {
            $classes[]= $this->em->getClassMetadata($entityClass);
        }

        try {
            $schemaTool->dropSchema($classes);
        } catch (\Exception $e) {
        }

        try {
            $schemaTool->createSchema($classes);
        } catch (\Exception $e) {
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function getExtensions()
    {
        return array_merge(parent::getExtensions(), array(
            new DoctrineOrmExtension($this->emRegistry),
        ));
    }

    /**
     * Return the entities to map with the entity manager
     *
     * @return array
     */
    protected function getEntities()
    {
        return array();
    }

    /**
     * Create a mock of entity manager registry
     *
     * @param string        $name
     * @param EntityManager $em
     *
     * @return \Doctrine\Common\Persistence\ManagerRegistry
     */
    protected function createRegistryMock($name, $em)
    {
        $registry = $this->getMock('Doctrine\Common\Persistence\ManagerRegistry');
        $registry->expects($this->any())
            ->method('getManager')
            ->with($this->equalTo($name))
            ->will($this->returnValue($em));

        $registry->expects($this->any())
            ->method('getManagerForClass')
            ->will($this->returnValue($em));

        return $registry;
    }
}