<?php

namespace AdeShopBundle\Tests\Form\Type;

use AdeShopBundle\Entity\Order;
use AdeShopBundle\Entity\PaymentMethod;
use AdeShopBundle\Form\Type\OrderType;

class OrderTypeTest extends FormTypeTestCase
{
    public function testSubmitValidData()
    {
        $paymentMethod = new FakePaymentMethod();
        $paymentMethod->setId(1);
        $paymentMethod->setTitle('paypal');

        $formData = array(
            'firstname' => 'Alessandro',
            'lastname' => 'Gregoletto',
            'company' => 'Mabon',
            'email' => 'a.gregoletto.mabon@gmail.com',
            'address' => 'via dei mughetti',
            'city' => 'Roma',
            'country' => 'Italia',
            'fiscalCode' => 'GRGLSN88H10H501T',
            'phone' => '3454312039',
            'vat' => '123456789',
            'paymentMethod' => $paymentMethod->getId()
        );

        $objectToCompare = new Order();
        $form = $this->factory->create(OrderType::class, $objectToCompare);

        $object = new Order();
        $object->setFirstname('Alessandro');
        $object->setLastname('Gregoletto');
        $object->setCompany('Mabon');
        $object->setEmail('a.gregoletto.mabon@gmail.com');
        $object->setAddress('via dei mughetti');
        $object->setCity('Roma');
        $object->setCountry('Italia');
        $object->setFiscalCode('GRGLSN88H10H501T');
        $object->setPhone('3454312039');
        $object->setVat('123456789');
        $object->setPaymentMethod($paymentMethod);

        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());

        $this->assertEquals($paymentMethod, $form->getData()->getPaymentMethod());

        $this->assertEquals($object, $objectToCompare);

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

    protected function getEntities()
    {
        return array_merge(parent::getEntities(), array(
            'AdeShopBundle\Entity\PaymentMethod',
        ));
    }
}

class FakePaymentMethod extends PaymentMethod
{
    public function setId($id)
    {
        $this->id = $id;
    }
}