<?php

namespace AdeShopBundle\Tests\Form\DataTransformer;

use AdeShopBundle\Form\DataTransformer\VariantToIdTransformer;
use PHPUnit\Framework\TestCase;

class VariantToIdTransformerTest extends TestCase
{
    protected $manager;
    protected $transformer;
    protected $variant;

    public function setUp()
    {
        $this->manager = $this->getMockBuilder('Doctrine\ORM\EntityManagerInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->variant = $this->getMockBuilder('AdeShopBundle\Entity\Variant')
            ->disableOriginalConstructor()
            ->getMock();

        $this->transformer = new VariantToIdTransformer($this->manager);
    }

    public function testTransform()
    {
        $this->variant->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(1));

        $this->transformer->transform($this->variant);
    }

}
