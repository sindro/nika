<?php
namespace AdeShopBundle\Tests\Repository;

use AdeShopBundle\Entity\Product;
use AdeShopBundle\Model\Category;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use AdeShopBundle\Entity\CatalogRule;

class CatalogRuleRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testFindBetterDiscountRuleHasProduct()
    {
        $product = $this->getMockBuilder('AdeShopBundle\Entity\Product')
            ->disableOriginalConstructor()
            ->getMock();

        $product->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $catalogRule = $this->em
            ->getRepository(CatalogRule::class)
            ->findBetterDiscountRule($product);

        $keys = [
            'discount' => "10",
            'startDate' => NULL,
            'endDate' => NULL,
            'floatDiscount' => "0.1000"
          ];

        foreach (array_keys($catalogRule[0]) as $key) {
            $this->assertArrayHasKey($key, $keys);
        }

    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}