<?php
namespace AdeShopBundle\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use AdeShopBundle\Entity\Product;

class ProductRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testFindProductsPerPage()
    {
        $category = $this->getCategory();
        $category->setTitle('categoria prodotto');
        $category->setSlug('categoria-prodotto');

        $products = $this->em
            ->getRepository(Product::class)
            ->findProducts($category, null, null, null);

        $this->assertCount(1, $products);
    }

    public function testFindProductWithMinPrice()
    {
        $minPrice = $this->em
            ->getRepository(Product::class)
            ->findProductWithMinPrice();

        $this->assertSame(10.00, (float) $minPrice);
    }

    public function testFindProductWithMaxPrice()
    {
        $maxPrice = $this->em
            ->getRepository(Product::class)
            ->findProductWithMaxPrice();

        $this->assertSame(10.00, (float) $maxPrice);
    }

    public function testFindByIds()
    {
        $products = $this->em
            ->getRepository(Product::class)
            ->findByIds([1]);

        $this->assertCount(1, $products);
    }

    private function getCategory()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\Category');
    }

    private function getManufacturer()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\Manufacturer');
    }

    private function getAuthor()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\Author');
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}