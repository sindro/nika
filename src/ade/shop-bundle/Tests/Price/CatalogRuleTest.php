<?php
namespace AdeShopBundle\Tests\Price;
use AdeShopBundle\Price\Engine;
use AdeShopBundle\Price\Language;
use PHPUnit\Framework\TestCase;

class CatalogRuleTest extends TestCase
{
    public function testDiscountRule()
    {
        $engine = new Engine(new Language());

        $catalogRule = ['discount' => 10, 'floatDiscount' => 0.1, 'startDate' => null, 'endDate' => null];

        $variant = $this->getVariant();
        $variant->setInStock(1);
        $variant->setPrice(10.00);

        $engine->addDiscountRules("rangeDate(date('now'), catalogRule) ? catalogRule['floatDiscount'] : (dateAreNull(catalogRule) ? catalogRule['floatDiscount'] : 0)");

        $this->assertSame(9.0, $engine->calculatePrice($variant, $catalogRule));
    }

    public function testDiscountRuleInRangeDate()
    {
        $engine = new Engine(new Language());

        $catalogRule = ['discount' => 10, 'floatDiscount' => 0.1, 'startDate' => new \DateTime(date('Y-m-d')), 'endDate' => new \DateTime('2200-02-01')];

        $variant = $this->getVariant();
        $variant->setInStock(1);
        $variant->setPrice(10.00);

        $engine->addDiscountRules("rangeDate(date('now'), catalogRule) ? catalogRule['floatDiscount'] : (dateAreNull(catalogRule) ? catalogRule['floatDiscount'] : 0)");

        $this->assertSame(9.0, $engine->calculatePrice($variant, $catalogRule));
    }

    public function testDiscountRuleIsNotInRangeDate()
    {
        $engine = new Engine(new Language());

        $catalogRule = ['discount' => 10, 'floatDiscount' => 0.1, 'startDate' => new \DateTime('2018-01-24'), 'endDate' => new \DateTime('2018-01-30')];

        $variant = $this->getVariant();
        $variant->setInStock(1);
        $variant->setPrice(10.00);

        $engine->addDiscountRules("rangeDate(date('now'), catalogRule) ? catalogRule['floatDiscount'] : (dateAreNull(catalogRule) ? catalogRule['floatDiscount'] : 0)");

        $this->assertSame(10.00, $engine->calculatePrice($variant, $catalogRule));
    }

    private function getVariant()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\Variant');
    }

    private function getTax()
    {
        $tax = $this->getMockForAbstractClass('AdeShopBundle\Model\Tax');
        $tax->setTitle('Iva');
        $tax->setPercentage(22);

        return $tax;
    }
}