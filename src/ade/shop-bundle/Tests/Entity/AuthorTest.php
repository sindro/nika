<?php
namespace AdeShopBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{
    public function testTitle()
    {
        $author = $this->getAuthor();
        $author->setTitle('Autore');

        $this->assertSame($author->getTitle(), 'Autore');
    }

    public function testDescription()
    {
        $author = $this->getAuthor();
        $author->setDescription('Descrizione Autore');

        $this->assertSame($author->getDescription(), 'Descrizione Autore');
    }

    public function testIsPublic()
    {
        $author = $this->getAuthor();
        $author->setIsPublic(true);

        $this->assertTrue($author->getIsPublic());
    }

    public function testSlug()
    {
        $author = $this->getAuthor();
        $author->setSlug('autore');

        $this->assertSame($author->getSlug(), 'autore');
    }

    /**
     * @return Author
     */
    protected function getAuthor()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\Author');
    }
}