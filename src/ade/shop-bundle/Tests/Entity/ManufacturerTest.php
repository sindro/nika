<?php
namespace AdeShopBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;

class ManufacturerTest extends TestCase
{
    public function testTitle()
    {
        $manufacturer = $this->getManufacturer();
        $manufacturer->setTitle('Produttore');

        $this->assertSame($manufacturer->getTitle(), 'Produttore');
    }

    public function testIsPublic()
    {
        $manufacturer = $this->getManufacturer();
        $manufacturer->setIsPublic(true);

        $this->assertTrue($manufacturer->getIsPublic());
    }

    public function testSlug()
    {
        $manufacturer = $this->getManufacturer();
        $manufacturer->setSlug('Produttore');

        $this->assertSame($manufacturer->getSlug(), 'Produttore');
    }

    public function testProduct()
    {
        $product = $this->getMockForAbstractClass('AdeShopBundle\Entity\Product');
        $product->setTitle('Product');

        $manufacturer = $this->getManufacturer();
        $manufacturer->addProduct($product);

        $this->assertSame(count($manufacturer->getProducts()), 1);
    }

    /**
     * @return Manufacturer
     */
    protected function getManufacturer()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\Manufacturer');
    }
}