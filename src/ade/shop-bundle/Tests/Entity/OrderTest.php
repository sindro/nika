<?php
namespace AdeShopBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
    public function testNumberBill()
    {
        $order = $this->getOrder();
        $order->setNumberBill(10);

        $this->assertSame($order->getNumberBill(), 10);
    }

    public function testDateBill()
    {
        $dateBill = new \DateTime(date('d-m-Y'));
        $order = $this->getOrder();
        $order->setDateBill($dateBill);

        $this->assertSame($order->getDateBill(), $dateBill);
    }

    public function testTracking()
    {
        $order = $this->getOrder();
        $order->setTracking(101112);

        $this->assertSame($order->getTracking(), 101112);
    }

    public function testStatus()
    {
        $order = $this->getOrder();
        $order->setStatus('paypal');

        $this->assertSame($order->getStatus(), 'paypal');
    }

    public function testFirstname()
    {
        $order = $this->getOrder();
        $order->setFirstname('Alessandro');

        $this->assertSame($order->getFirstname(), 'Alessandro');
    }

    public function testLastname()
    {
        $order = $this->getOrder();
        $order->setLastname('Gregoletto');

        $this->assertSame($order->getLastname(), 'Gregoletto');
    }

    public function testEmail()
    {
        $order = $this->getOrder();
        $order->setEmail('a.gregoletto@maboncreative.com');

        $this->assertSame($order->getEmail(), 'a.gregoletto@maboncreative.com');
    }

    public function testFiscalCode()
    {
        $order = $this->getOrder();
        $order->setFiscalCode('GRGLSN88H10H501T');

        $this->assertSame($order->getFiscalCode(), 'GRGLSN88H10H501T');
    }

    public function testPhone()
    {
        $order = $this->getOrder();
        $order->setPhone('3454312039');

        $this->assertSame($order->getPhone(), '3454312039');
    }

    public function testAddress()
    {
        $order = $this->getOrder();
        $order->setAddress('via dei mughetti');

        $this->assertSame($order->getAddress(), 'via dei mughetti');
    }

    public function testPostalCode()
    {
        $order = $this->getOrder();
        $order->setPostalCode('00172');

        $this->assertSame($order->getPostalCode(), '00172');
    }

    public function testCity()
    {
        $order = $this->getOrder();
        $order->setCity('Roma');

        $this->assertSame($order->getCity(), 'Roma');
    }

    public function testCountry()
    {
        $order = $this->getOrder();
        $order->setCountry('italia');

        $this->assertSame($order->getCountry(), 'italia');
    }

    public function testCreateUser()
    {
        $order = $this->getOrder();
        $order->setCreateUser(true);

        $this->assertTrue($order->getCreateUser());
    }

    public function testNote()
    {
        $order = $this->getOrder();
        $order->setNote('nota');

        $this->assertSame($order->getNote(), 'nota');
    }

    public function testCompany()
    {
        $order = $this->getOrder();
        $order->setCompany('Mabon');

        $this->assertSame($order->getCompany(), 'Mabon');
    }

    public function testVat()
    {
        $order = $this->getOrder();
        $order->setVat('123456789');

        $this->assertSame($order->getVat(), '123456789');
    }

    public function testPaymentMethod()
    {
        $paymentMethod = $this->getMockForAbstractClass('AdeShopBundle\Entity\PaymentMethod');
        $paymentMethod->setTitle('Paypal');

        $order = $this->getOrder();
        $order->setPaymentMethod($paymentMethod);

        $this->assertSame($order->getPaymentMethod(), $paymentMethod);
    }

    public function testUser()
    {
        $user = $this->getUser();

        $order = $this->getOrder();
        $order->setUser($user);

        $this->assertSame($order->getUser(), $user);
    }


    private function getOrder()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\Order');
    }

    private function getUser()
    {
        $user = $this->getMockForAbstractClass('App\Entity\User');
        $user->setEmail('a.gregoletto@maboncreaative.com');

        return $user;
    }
}