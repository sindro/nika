<?php
namespace AdeShopBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    public function testTitle()
    {
        $category = $this->getCategory();
        $category->setTitle('Categoria');

        $this->assertSame($category->getTitle(), 'Categoria');
    }

    public function testShortDescription()
    {
        $category = $this->getCategory();
        $category->setShortDescription('Descrizione Categoria');

        $this->assertSame($category->getShortDescription(), 'Descrizione Categoria');
    }

    public function testIsPublic()
    {
        $category = $this->getCategory();
        $category->setIsPublic(true);

        $this->assertTrue($category->getIsPublic());
    }

    public function testSlug()
    {
        $category = $this->getCategory();
        $category->setSlug('categoria');

        $this->assertSame($category->getSlug(), 'categoria');
    }

    public function testLft()
    {
        $category = $this->getCategory();
        $category->setLft(1);

        $this->assertSame($category->getLft(), 1);
    }

    public function testRgt()
    {
        $category = $this->getCategory();
        $category->setRgt(1);

        $this->assertSame($category->getRgt(), 1);
    }

    public function testLvl()
    {
        $category = $this->getCategory();
        $category->setLvl(1);

        $this->assertSame($category->getLvl(), 1);
    }

    public function testRoot()
    {
        $category = $this->getCategory();
        $category->setRoot(1);

        $this->assertSame($category->getRoot(), 1);
    }

    public function testParent()
    {
        $parent = $this->getCategory();
        $parent->setTitle('Parente');

        $category = $this->getCategory();
        $category->setParent($parent);

        $this->assertSame($category->getParent(), $parent);
    }

    public function testChild()
    {
        $child = $this->getCategory();
        $child->setTitle('Figlio');

        $category = $this->getCategory();
        $category->addChild($child);

        $this->assertSame(count($category->getChildren()), 1);
    }

    public function testProduct()
    {
        $product = $this->getMockForAbstractClass('AdeShopBundle\Entity\Product');
        $product->setTitle('Product');

        $category = $this->getCategory();
        $category->addProduct($product);

        $this->assertSame(count($category->getProducts()), 1);
    }
    
    /**
     * @return Category
     */
    protected function getCategory()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\Category');
    }
}