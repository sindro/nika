<?php
namespace AdeShopBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;

class VariantTest extends TestCase
{
    public function testSku()
    {
        $variant = $this->getVariant();
        $variant->setSku('Variant Sku');

        $this->assertSame($variant->getSku(), 'Variant Sku');
    }

    public function testIsbn()
    {
        $variant = $this->getVariant();
        $variant->setIsbn('Variant Isbn');

        $this->assertSame($variant->getIsbn(), 'Variant Isbn');
    }

    public function testPrice()
    {
        $variant = $this->getVariant();
        $variant->setPrice(10.00);

        $this->assertSame($variant->getPrice(), 10.00);
    }

    public function testInStock()
    {
        $variant = $this->getVariant();
        $variant->setInStock(1);

        $this->assertSame($variant->getInStock(), 1);
    }

    public function testWeight()
    {
        $variant = $this->getVariant();
        $variant->setWeight(10);

        $this->assertSame($variant->getWeight(), 10);
    }

    public function testHeight()
    {
        $variant = $this->getVariant();
        $variant->setHeight(10);

        $this->assertSame($variant->getHeight(), 10);
    }

    public function testWidth()
    {
        $variant = $this->getVariant();
        $variant->setWidth(10);

        $this->assertSame($variant->getWidth(), 10);
    }

    public function testDepth()
    {
        $variant = $this->getVariant();
        $variant->setDepth(10);

        $this->assertSame($variant->getDepth(), 10);
    }

    public function testShortDescription()
    {
        $variant = $this->getVariant();
        $variant->setShortDescription('Descrizione Variante');

        $this->assertSame($variant->getShortDescription(), 'Descrizione Variante');
    }

    public function testProduct()
    {
        $product = $this->getMockForAbstractClass('AdeShopBundle\Entity\Product');
        $product->setTitle('Prodotto');

        $variant = $this->getVariant();
        $variant->setProduct($product);

        $this->assertSame($variant->getProduct(), $product);
    }

    public function testGallery()
    {
        $gallery = $this->getMockForAbstractClass('AdeGalleryBundle\Entity\Gallery');
        $gallery->setTitle('Galleria');

        $variant = $this->getVariant();
        $variant->setGallery($gallery);

        $this->assertSame($variant->getGallery(), $gallery);
    }

    public function testTax()
    {
        $tax = $this->getMockForAbstractClass('AdeShopBundle\Entity\Tax');
        $tax->setTitle('Imposta');

        $variant = $this->getVariant();
        $variant->setTax($tax);

        $this->assertSame($variant->getTax(), $tax);
    }

    public function testOptionValues()
    {
        $optionValue = $this->getMockForAbstractClass('AdeShopBundle\Entity\OptionValue');
        $optionValue->setTitle('opzione');

        $variant = $this->getVariant();
        $variant->addOptionValue($optionValue);

        $this->assertSame(count($variant->getOptionValues()), 1);
    }

    public function testSetOptionValues()
    {
        $optionValue = $this->getMockForAbstractClass('AdeShopBundle\Entity\OptionValue');
        $optionValue->setTitle('opzione');

        $optionValue2 = $this->getMockForAbstractClass('AdeShopBundle\Entity\OptionValue');
        $optionValue2->setTitle('opzione 2');

        $variant = $this->getVariant();
        $variant->setOptionValues([$optionValue, $optionValue2]);

        $this->assertSame(count($variant->getOptionValues()), 2);
    }

    public function testRemoveOptionValue()
    {
        $optionValue = $this->getMockForAbstractClass('AdeShopBundle\Entity\OptionValue');
        $optionValue->setTitle('opzione');

        $optionValue2 = $this->getMockForAbstractClass('AdeShopBundle\Entity\OptionValue');
        $optionValue2->setTitle('opzione 2');

        $variant = $this->getVariant();
        $variant->setOptionValues([$optionValue, $optionValue2]);

        $variant->removeOptionValue($optionValue);

        $this->assertSame(count($variant->getOptionValues()), 1);
    }

    public function testHasOptionValue()
    {
        $optionValue = $this->getMockForAbstractClass('AdeShopBundle\Entity\OptionValue');
        $optionValue->setTitle('opzione');

        $optionValue2 = $this->getMockForAbstractClass('AdeShopBundle\Entity\OptionValue');
        $optionValue2->setTitle('opzione 2');

        $variant = $this->getVariant();
        $variant->setOptionValues([$optionValue, $optionValue2]);

        $this->assertTrue($variant->hasOptionValue($optionValue));
    }

    public function testSpecifications()
    {
        $optionValue = $this->getMockForAbstractClass('AdeShopBundle\Entity\OptionValue');
        $optionValue->setTitle('opzione');

        $optionValue2 = $this->getMockForAbstractClass('AdeShopBundle\Entity\OptionValue');
        $optionValue2->setTitle('opzione 2');

        $option = $this->getMockForAbstractClass('AdeShopBundle\Entity\Option');
        $option->setTitle('opzione');
        $option->setValues([$optionValue2, $optionValue]);

        $product = $this->getMockForAbstractClass('AdeShopBundle\Entity\Product');
        $product->setTitle('prodotto');
        $product->setOptions([$option]);

        $variant = $this->getVariant();
        $variant->setProduct($product);
        $variant->setHeight(10);
        $variant->setWidth(10);
        $variant->setWeight(10);
        $variant->setDepth(10);


        $keys = ['opzione' => ['opzione 2', 'opzione'], 'width' => 10, 'height' => 10, 'weight' => 10, 'depth' => 10];

        foreach (array_keys($keys) as $key) {
            $this->assertArrayHasKey($key, $variant->getSpecifications());
        }

    }

    /**
     * @return Variant
     */
    protected function getVariant()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\Variant');
    }
}