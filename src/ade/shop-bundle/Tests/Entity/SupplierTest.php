<?php
namespace AdeShopBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;

class SupplierTest extends TestCase
{
    public function testTitle()
    {
        $supplier = $this->getSupplier();
        $supplier->setTitle('Supplier');

        $this->assertSame($supplier->getTitle(), 'Supplier');
    }

    public function testIsPublic()
    {
        $supplier = $this->getSupplier();
        $supplier->setIsPublic(true);

        $this->assertTrue($supplier->getIsPublic());
    }

    public function testSlug()
    {
        $supplier = $this->getSupplier();
        $supplier->setSlug('Supplier');

        $this->assertSame($supplier->getSlug(), 'Supplier');
    }

    /**
     * @return Supplier
     */
    protected function getSupplier()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\Supplier');
    }
}