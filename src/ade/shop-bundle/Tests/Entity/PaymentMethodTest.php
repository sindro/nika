<?php
namespace AdeShopBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;

class PaymentMethodTest extends TestCase
{
    public function testTitle()
    {
        $paymentMethod = $this->getPaymentMethod();
        $paymentMethod->setTitle('Paypal');

        $this->assertSame($paymentMethod->getTitle(), 'Paypal');
    }

    public function testDescription()
    {
        $paymentMethod = $this->getPaymentMethod();
        $paymentMethod->setDescription('Descrizione');

        $this->assertSame($paymentMethod->getDescription(), 'Descrizione');
    }

    public function testAdditionalCost()
    {
        $paymentMethod = $this->getPaymentMethod();
        $paymentMethod->setAdditionalCost(5.00);

        $this->assertSame($paymentMethod->getAdditionalCost(), 5.00);
    }

    public function testIsPublic()
    {
        $paymentMethod = $this->getPaymentMethod();
        $paymentMethod->setIsPublic(true);

        $this->assertTrue($paymentMethod->getIsPublic());
    }

    /**
     * @return PaymentMethod
     */
    protected function getPaymentMethod()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\PaymentMethod');
    }
}