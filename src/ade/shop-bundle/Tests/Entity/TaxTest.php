<?php
namespace AdeShopBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;

class TaxTest extends TestCase
{
    public function testTitle()
    {
        $tax = $this->getTax();
        $tax->setTitle('iva');

        $this->assertSame($tax->getTitle(), 'iva');
    }

    public function testDescription()
    {
        $tax = $this->getTax();
        $tax->setPercentage(22);

        $this->assertSame($tax->getPercentage(), 22);
    }

    /**
     * @return Author
     */
    protected function getTax()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\Tax');
    }
}