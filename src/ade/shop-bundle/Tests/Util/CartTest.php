<?php
namespace AdeShopBundle\Tests\Util;

use AdeShopBundle\Util\Cart;
use PHPUnit\Framework\TestCase;

class CartTest extends TestCase
{
    protected $session;
    protected $cart;

    public function setUp()
    {
        $this->session = $this->getMockBuilder('Symfony\Component\HttpFoundation\Session\SessionInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->cart = new Cart($this->session);
    }
    public function testGet()
    {
        $this->session->expects($this->once())
            ->method('get')
            ->will($this->returnValue(array()));

        $this->cart->get();
    }

    public function testAdd()
    {
        $this->session->expects($this->at(0))
            ->method('get')
            ->will($this->returnValue(array()));

        $variant = $this->getMockBuilder('AdeShopBundle\Entity\Variant')
            ->disableOriginalConstructor()
            ->getMock();

        $variant->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $this->session->expects($this->at(1))
            ->method('get')
            ->will($this->returnValue(array($variant->getId() => 1)));

        $this->session->expects($this->once())
            ->method('set')
            ->will($this->returnValue(null));

        $this->cart->add($variant, 1);

        $cart = $this->cart->get();
    }

    public function testAddCombo()
    {
        $this->session->expects($this->at(0))
            ->method('get')
            ->will($this->returnValue(array()));

        $variant = $this->getMockBuilder('AdeShopBundle\Entity\Variant')
            ->disableOriginalConstructor()
            ->getMock();

        $variant->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $this->session->expects($this->at(1))
            ->method('get')
            ->will($this->returnValue(array($variant->getId() => 1)));

        $this->session->expects($this->once())
            ->method('set')
            ->will($this->returnValue(null));

        $this->cart->addCombo($variant);

        $cart = $this->cart->get();
    }

    public function testRemove()
    {
        $this->session->expects($this->at(0))
            ->method('get')
            ->will($this->returnValue(array()));

        $variant = $this->getMockBuilder('AdeShopBundle\Entity\Variant')
            ->disableOriginalConstructor()
            ->getMock();

        $variant->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $this->session->expects($this->at(1))
            ->method('get')
            ->will($this->returnValue(array($variant->getId() => 1)));

        $this->session->expects($this->once())
            ->method('set')
            ->will($this->returnValue(null));

        $this->cart->remove($variant);

        $cart = $this->cart->get();
    }

    public function testErase()
    {
        $this->session->expects($this->once())
            ->method('set')
            ->will($this->returnValue(array()));

        $this->cart->erase();
    }

}