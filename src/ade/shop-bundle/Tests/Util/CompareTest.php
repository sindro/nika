<?php
namespace AdeShopBundle\Tests\Util;

use AdeShopBundle\Util\Compare;
use PHPUnit\Framework\TestCase;

class CompareTest extends TestCase
{
    protected $session;
    protected $compare;

    public function setUp()
    {
        $this->session = $this->getMockBuilder('Symfony\Component\HttpFoundation\Session\SessionInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->compare = new Compare($this->session);
    }
    public function testGet()
    {
        $this->session->expects($this->once())
            ->method('get')
            ->will($this->returnValue(array()));

        $this->compare->get();
    }

    public function testAdd()
    {
        $this->session->expects($this->at(0))
            ->method('get')
            ->will($this->returnValue(array()));

        $variant = $this->getMockBuilder('AdeShopBundle\Entity\Variant')
            ->disableOriginalConstructor()
            ->getMock();

        $variant->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $this->session->expects($this->at(1))
            ->method('get')
            ->will($this->returnValue(array($variant->getId() => 1)));

        $this->session->expects($this->once())
            ->method('set')
            ->will($this->returnValue(null));

        $this->compare->add($variant, 1);

        $compare = $this->compare->get();
    }

    public function testRemove()
    {
        $this->session->expects($this->at(0))
            ->method('get')
            ->will($this->returnValue(array()));

        $variant = $this->getMockBuilder('AdeShopBundle\Entity\Variant')
            ->disableOriginalConstructor()
            ->getMock();

        $variant->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $this->session->expects($this->at(1))
            ->method('get')
            ->will($this->returnValue(array($variant->getId() => 1)));

        $this->session->expects($this->once())
            ->method('set')
            ->will($this->returnValue(null));

        $this->compare->remove($variant);

        $compare = $this->compare->get();
    }

    public function testErase()
    {
        $this->session->expects($this->once())
            ->method('set')
            ->will($this->returnValue(array()));

        $this->compare->erase();
    }
}