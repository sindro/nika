<?php
namespace AdeShopBundle\Tests\EventListener;

use AdeShopBundle\Price\Language;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;
use AdeShopBundle\Price\Engine;

class ProductListenerTest extends TestCase
{
    public function testPostLoad()
    {
        $engine = new Engine(new Language());

        $variant = $this->getVariant();
        $variant->setPrice(12.00);

        $catalogRepository = $this
            ->getMockBuilder(EntityRepository::class)
            ->setMethods(['findBetterDiscountRule'])
            ->disableOriginalConstructor()
            ->getMock();

        $catalogRepository->expects($this->any())
            ->method('findBetterDiscountRule')
            ->willReturn([0 => ['discount' => 10, 'floatDiscount' => 0.1, 'startDate' => null, 'endDate' => null ]]);

        $rules = count($catalogRepository->findBetterDiscountRule($variant)) > 0 ? $catalogRepository->findBetterDiscountRule($variant)[0] : ['discount' => null, 'floatDiscount' => null, 'startDate' => null, 'endDate' => null];

        $engine->addDiscountRules("rangeDate(date('now'), catalogRule) ? catalogRule['floatDiscount'] : (dateAreNull(catalogRule) ? catalogRule['floatDiscount'] : 0)");

        $price = $engine->calculatePrice($variant, $rules);

        $this->assertSame([0 => ['discount' => 10, 'floatDiscount' => 0.1, 'startDate' => null, 'endDate' => null ]], $catalogRepository->findBetterDiscountRule($variant));

        $this->assertSame($price, 10.8);
    }

    private function getVariant()
    {
        return $this->getMockForAbstractClass('AdeShopBundle\Entity\Variant');
    }
}