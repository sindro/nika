<?php
namespace AdeShopBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    public function testApiVariant()
    {
        $client = static::createClient();

        $client->request('GET', '/api/variant/1/1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertSame('application/json', $client->getResponse()->headers->get('Content-Type'));

        $response = json_decode($client->getResponse()->getContent(), true);

        $keys = ['sku' => 'SKU', 'currentPrice' => 0.00, 'price' => 0.00, 'discount' => 10, 'images' => [], 'inStock' => 0, 'specifications' => [], 'shortDescription' => 'lorem ipsum'];

        foreach (array_keys($keys) as $key) {
            $this->assertArrayHasKey($key, $response);
        }
    }
}