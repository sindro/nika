<?php
namespace AdeShopBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CartControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/shop/cart');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}