<?php
namespace AdeShopBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ComponentControllerTest extends WebTestCase
{
    public function testManufacturer()
    {
        $client = static::createClient();

        $client->request('GET', '/component/shop/manufacturer');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testPriceFilter()
    {
        $client = static::createClient();

        $client->request('GET', '/component/shop/price_filter');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}