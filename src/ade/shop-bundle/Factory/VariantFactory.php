<?php

namespace AdeShopBundle\Factory;

use AdeShopBundle\Entity\Product;
use AdeShopBundle\Entity\Variant;

class VariantFactory implements VariantFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createForProduct(Product $product): Variant
    {
        /** @var ProductVariantInterface $variant */
        $variant = new Variant();
        $variant->setSku(uniqid());
        $variant->setPrice(0.00);
        $variant->setProduct($product);

        return $variant;
    }
}
