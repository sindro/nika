<?php

namespace AdeShopBundle\Factory;

use AdeShopBundle\Entity\Product;
use AdeShopBundle\Entity\Variant;

interface VariantFactoryInterface
{
    /**
     * @param Product $product
     *
     * @return Variant
     */
    public function createForProduct(Product $product): Variant;
}
