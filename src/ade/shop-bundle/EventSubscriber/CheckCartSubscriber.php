<?php
namespace AdeShopBundle\EventSubscriber;

use AdeShopBundle\Controller\CheckCartController;
use AdeShopBundle\Util\CartInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class CheckCartSubscriber implements EventSubscriberInterface
{
    private $cart;
    private $router;
    private $isEmpty = false;

    public function __construct(CartInterface $cart, RouterInterface $router)
    {
        $this->cart = $cart;
        $this->router = $router;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof CheckCartController)
        {
            $this->isEmpty = $this->cart->isEmpty();
        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if($this->isEmpty) {
            $url = $this->router->generate('ade_shop_cart');
            $response = new RedirectResponse($url);

            $event->setResponse($response);
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => 'onKernelController',
            KernelEvents::RESPONSE => 'onKernelResponse'
        );
    }
}