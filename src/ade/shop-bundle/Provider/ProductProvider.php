<?php
namespace AdeShopBundle\Provider;

use AdeShopBundle\Entity\Variant;
use AdeShopBundle\Util\CartInterface;
use Doctrine\Common\Persistence\ObjectManager;

final class ProductProvider
{
    private $em;
    private $cart;

    public function __construct(ObjectManager $em, CartInterface $cart)
    {
        $this->em = $em;
        $this->cart = $cart;
    }

    public function findByCart()
    {
        $items = $this->em->getRepository(Variant::class)
                            ->findByIds(array_keys($this->cart->get()));

        return $items;
    }
}