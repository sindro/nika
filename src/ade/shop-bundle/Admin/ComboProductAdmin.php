<?php
namespace AdeShopBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;

class ComboProductAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('comboProductItems', null, array(
                'label' => 'form.comboProductItems',
            ))
            ->add('shortcode', null, array(
                'label' => 'form.shortcode',
                'template' => '@AdeShop/Admin/ComboProduct/_shortcode.html.twig'
            ))
            ->add('_action', 'actions', array(
                'label' => 'form.actions',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('comboProductItems', null, array(
                'label' => 'form.comboProductItems'
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('comboProductItems', CollectionType::class, array(
                'label' => 'form.comboProductItems',
                'required' => false,
                'by_reference' => false,
            ), array(
                'edit' => 'inline',
                'inline' => 'table'
            ));
    }
}