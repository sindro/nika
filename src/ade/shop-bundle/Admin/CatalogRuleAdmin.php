<?php
namespace AdeShopBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;

class CatalogRuleAdmin extends AbstractAdmin
{
    public function getFormBuilder()
    {
        $this->formOptions['data_class'] = $this->getClass();

        $options = $this->formOptions;

        $options['validation_groups'] = $this->getRequest()->get('rule_type');

        $formBuilder = $this->getFormContractor()->getFormBuilder($this->getUniqid(), $options);

        $this->defineFormBuilder($formBuilder);

        return $formBuilder;
    }

    public function getPersistentParameters()
    {
        if (!$this->getRequest()) {
            return [];
        }

        return [
            'rule_type' => $this->getRequest()->get('rule_type')
        ];
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, array(
                'label' => 'form.title'
            ))
            ->add('discount', null, array(
                'label' => 'form.discount'
            ))
            ->add('startDate', null, array(
                'label' => 'form.discount_start_date'
            ))
            ->add('endDate', null, array(
                'label' => 'form.discount_end_date'
            ))
            ->add('_action', 'actions', array(
                'label' => 'form.actions',
                'actions' => array(
                    'edit' => array('template' => '@AdeShop/Admin/CatalogRule/list__action_edit.html.twig'),
                    'delete' => array()
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array(
                'label' => 'form.title'
            ))
            ->add('discount', null, array(
                'label' => 'form.discount'
            ))
            ->add('category', ModelAutocompleteFilter::class, array(
                'label' => 'form.category'
            ), null, array(
                'property' => 'title'
            ))
            ->add('author', ModelAutocompleteFilter::class, array(
                'label' => 'form.author'
            ), null, array(
                'property' => 'title'
            ))
            ->add('manufacturer', ModelAutocompleteFilter::class, array(
                'label' => 'form.manufacturer'
            ), null, array(
                'property' => 'title'
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('discount', null, array(
                    'label' => 'form.discount'
                ))
                ->add('startDate', DatePickerType::class, array(
                    'label' => 'form.discount_start_date',
                    'required' => false,
                ))
                ->add('endDate', DatePickerType::class, array(
                    'label' => 'form.discount_end_date',
                    'required' => false,
                ))
                ->add($this->getRequest()->get('rule_type'), null, array(
                    'label' => 'form.' . $this->getRequest()->get('rule_type')
                ));
    }

    public function prePersist($object)
    {
        $object->setTitle($this->getTitle($object));
        $object->setContext($this->request->get('rule_type'));
    }

    public function preUpdate($object)
    {
        $object->setTitle($this->getTitle($object));
    }

    private function getTitle($object)
    {
        $title = '';

        switch($this->request->get('rule_type'))
        {
            case 'product':
                $title .= 'Sconto sul prodotto "' . $object->getProduct() . '"';
                break;
            case 'variant':
                $title .= 'Sconto sulla variante "' . $object->getVariant() . '" del Prodotto "' . $object->getVariant()->getProduct() . '"';
                break;
            case 'category':
                $title .= 'Sconto sulla categoria "' . $object->getCategory() . '"';
                break;
            case 'manufacturer':
                $title .= 'Sconto sulla marca "' . $object->getManufacturer() . '"';
                break;
            case 'author':
                $title .= 'Sconto sull\'autore "' . $object->getAuthor() . '"';
                break;
            case 'supplier':
                $title .= 'Sconto sul fornitore "' . $object->getSupplier() . '"';
                break;
        }

        return $title;
    }
}