<?php
namespace AdeShopBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;

class OptionAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, array(
                'label' => 'form.title'
            ))
            ->add('values', null, array(
                'label' => 'form.values'
            ))
            ->add('_action', 'actions', array(
                'label' => 'form.actions',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array(
                'label' => 'form.title'
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {        
        $formMapper
            ->add('title', null, array(
                'label' => 'form.title'
            ))
            ->add('code', null, array(
                'label' => 'form.code'
            ))
            ->add('values', CollectionType::class, [
                'label' => 'form.values',
                'by_reference' => false
            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ]);
    }

    public function prePersist($object)
    {
        parent::prePersist($object);
        $this->setCode($object);
    }

    public function preUpdate($object)
    {
        parent::preUpdate($object);
        $this->setCode($object);
    }

    private function setCode($object)
    {
        $object->setCode($this->clean($object->getTitle()));

        foreach($object->getValues() as $value)
        {
            $value->setCode($this->clean($object . ' ' . $value->getTitle()));
        }
    }

    private function clean($string)
    {
       $string = strtolower(str_replace(' ', '_', $string));

       return preg_replace('/[^A-Za-z0-9\_]/', '', $string); // Removes special chars.
    }
}