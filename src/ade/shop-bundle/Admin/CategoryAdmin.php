<?php
namespace AdeShopBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Route\RouteCollection;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CategoryAdmin extends AbstractAdmin
{
    public function configureRoutes(RouteCollection $routes)
    {
        $routes->add('tree', 'tree');
        $routes->add('jsonTree', 'json/tree');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, array(
                'label' => 'form.title'
            ))
            ->add('isPublic', null, array(
                'label' => 'form.isPublic',
                'editable' => true
            ))
            ->add('_action', 'actions', array(
                'label' => 'form.actions',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array(
                'label' => 'form.title'
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', null, array(
                'label' => 'form.title'
            ))
            ->add('imageFile', VichImageType::class, [
                'label' => 'form.image',
                'required' => false,
                'allow_delete' => true,
                'download_uri' => true,
                'image_uri' => true
            ])
            ->add('shortDescription', null, array(
                'label' => 'form.shortDescription',
                /*'format' => 'richhtml',
                'ckeditor_context' => 'default',*/
            ))
            ->add('parent', ModelListType::class, array(
                'label' => 'form.parent',
                'required' => false,
                'btn_add' => false
            ))
            ->add('isPublic', null, array(
                'label' => 'form.isPublic'
            ));
    }

    public function delete($object)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();

        if($object->hasChildren()) {
            foreach ($object->getChildren() as $child) {
                $child->setParent(NULL);
                $em->persist($child);
                $em->flush();
            }
        }

        $this->preRemove($object);
        $this->getSecurityHandler()->deleteObjectSecurity($this, $object);
        $this->getModelManager()->delete($object);
        $this->postRemove($object);
    }
}