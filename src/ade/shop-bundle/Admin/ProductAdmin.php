<?php
namespace AdeShopBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;

class ProductAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    );

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('generateVariants', $this->getRouterIdParameter().'/generate-variants');
    }

    public function getPersistentParameters()
    {
        if (!$this->getRequest()) {
            return [];
        }

        return [
            'product_type' => $this->getRequest()->get('product_type'),
        ];
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, array(
                'label' => 'form.title'
            ))
            ->add('context', null, array(
                'label' => 'form.context'
            ))
            ->add('inStock', null, array(
                'label' => 'form.inStock',
                'template' => '@AdeShop/Admin/Product/_update_field.html.twig'
            ))
            ->add('price', null, array(
                'label' => 'form.price',
                'template' => '@AdeShop/Admin/Product/_price.html.twig'
            ))
            ->add('categories', null, array(
                'label' => 'form.categories'
            ))
            ->add('isPublic', null, array(
                'label' => 'form.isPublic',
                'editable' => true
            ))
            ->add('_action', 'actions', array(
                'label' => 'form.actions',
                'actions' => array(
                    'edit' => array('template' => '@AdeShop/Admin/Product/list__action_edit.html.twig'),
                    'delete' => array(),
                    'manageVariants' => array('template' => '@AdeShop/Admin//Product/_manage_variants_action.html.twig')
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array(
                'label' => 'form.title'
            ))
            ->add('subtitle', null, array(
                'label' => 'form.subtitle'
            ))
            ->add('categories', ModelAutocompleteFilter::class, array(
                'label' => 'form.categories'
            ), null, array(
                'property' => 'title'
            ))
            ->add('authors', ModelAutocompleteFilter::class, array(
                'label' => 'form.authors'
            ), null, array(
                'property' => 'title'
            ))
            ->add('manufacturer', ModelAutocompleteFilter::class, array(
                'label' => 'form.manufacturer'
            ), null, array(
                'property' => 'title'
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('form.group.product_configurable', array(
                'class'       => 'col-md-8',
                'box_class'   => 'box box-primary'
            ))
                ->add('title', null, array(
                    'label' => 'form.title'
                ))
                ->add('subtitle', null, array(
                    'label' => 'form.subtitle'
                ))
                ->add('description', null, array(
                    'label' => 'form.description'
                ))
                ->add('options', ModelType::class, array(
                    'property' => 'title',
                    'multiple' => true,
                    'required' => false,
                    'btn_add' => false,
                    'choice_translation_domain' => false,
                    'label' => 'form.options',
                ))
                ->add('products', ModelAutocompleteType::class, array(
                    'property' => ['title', 'sku'],
                    'multiple' => true,
                    'required' => false,
                    'label' => 'form.product_related'
                ))
            ->end()
            ->with('form.group.general', array(
                'class'       => 'col-md-4',
                'box_class'   => 'box box-primary'
            ))
                ->add('isPublic', null, array(
                    'label' => 'form.isPublic'
                ))
                ->add('manufacturer', ModelAutocompleteType::class, array(
                    'property' => 'title',
                    'multiple' => false,
                    'required' => false,
                    'label' => 'form.manufacturer',
                    'template' => $this->getConfigurationPool()->getContainer()->getParameter('ade_shop.form.template.autocomplete_add_template')
                ))
                ->add('authors', ModelAutocompleteType::class, array(
                    'property' => 'title',
                    'multiple' => true,
                    'required' => false,
                    'label' => 'form.authors',
                    'template' => $this->getConfigurationPool()->getContainer()->getParameter('ade_shop.form.template.autocomplete_add_template')
                ))
                ->add('categories', ModelAutocompleteType::class, array(
                    'property' => 'title',
                    'multiple' => true,
                    'required' => false,
                    'label' => 'form.categories',
                    'template' => $this->getConfigurationPool()->getContainer()->getParameter('ade_shop.form.template.autocomplete_add_template')
                ))
                ->add('supplier', ModelAutocompleteType::class, array(
                    'property' => 'title',
                    'multiple' => false,
                    'required' => false,
                    'label' => 'form.supplier',
                    'template' => $this->getConfigurationPool()->getContainer()->getParameter('ade_shop.form.template.autocomplete_add_template')
                ))
            ->end();
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        if ($this->isGranted('EDIT') && $this->getRequest()->get('product_type') == 'configurable') {
            $menu->addChild('Edit Product', [
                'uri' => $admin->generateUrl('edit', ['id' => $id, 'product_type' => 'configurable'])
            ]);
        }

        if ($this->isGranted('LIST') && $this->getRequest()->get('product_type') == 'configurable') {
            $menu->addChild('Manage Variants', [
                'uri' => $admin->generateUrl('admin.variant.list', ['id' => $id, 'product_type' => 'configurable'])
            ]);
        }
    }

    public function prePersist($object)
    {
        $object->setContext($this->request->get('product_type'));
        $this->recursiveCategories($object);
    }

    public function preUpdate($object)
    {
        $this->recursiveCategories($object);
    }

    private function recursiveCategories($object) {

        foreach($object->getCategories() as $category)
        {
            if(null !== $category->getParent() && !$object->hasCategory($category->getParent()->getTitle())) {
                $object->addCategory($category->getParent());
                $this->recursiveCategories($object);
            }
        }
    }
}