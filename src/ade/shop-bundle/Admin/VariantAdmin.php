<?php
namespace AdeShopBundle\Admin;

use AdeShopBundle\Form\EventSubscriber\BuildProductVariantFormSubscriber;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Vich\UploaderBundle\Form\Type\VichImageType;

class VariantAdmin extends AbstractAdmin
{
    protected $parentAssociationMapping = 'product';

    public function getParentAssociationMapping()
    {
        return 'product';
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        if ($this->isChild()) {
            return;
        }

        $collection->clear();
    }

    public function getPersistentParameters()
    {
        if (!$this->getRequest()) {
            return [];
        }

        return [
            'product_type' => $this->getRequest()->get('product_type'),
        ];
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('imageFile', null, array(
                'label' => 'form.image',
                'template' => '@AdeShop/Admin/Product/_image.html.twig'
            ))
            ->add('optionValues', null, array(
                'label' => 'form.optionValues'
            ))
            ->add('sku', null, array(
                'label' => 'form.sku',
                'template' => '@AdeFastEditSonata/Admin/_update_field.html.twig'
            ))
            ->add('inStock', null, array(
                'label' => 'form.inStock',
                'template' => '@AdeFastEditSonata/Admin/_update_field.html.twig'
            ))
            ->add('price', null, array(
                'label' => 'form.price',
                'template' => '@AdeFastEditSonata/Admin/_update_field.html.twig'
            ))
            ->add('_action', 'actions', array(
                'label' => 'form.actions',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('optionValues', null, array(
                'label' => 'form.optionValues'
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('form.group.inventory', array(
                'box_class'   => 'box box-primary',
                'class'       => 'col-md-8'
            ));

        $formMapper
                ->add('sku', null, array(
                    'label' => 'form.sku'
                ))
                ->add('isbn', null, array(
                    'label' => 'form.isbn'
                ))
                ->add('inStock', null, array(
                    'label' => 'form.inStock'
                ))
                ->add('shortDescription', null, array(
                    'label' => 'form.shortDescription'
                ))
                ->add('price', MoneyType::class, array(
                    'label' => 'form.price',
                    'scale' => 4
                ))
                ->add('tax', null, array(
                    'label' => 'form.tax'
                ))
            ->end()
            ->with('form.group.general', array(
                'box_class'   => 'box box-primary',
                'class'       => 'col-md-4'
            ))
                ->add('imageFile', VichImageType::class, [
                    'label' => 'form.image',
                    'required' => false,
                    'allow_delete' => true,
                    'download_uri' => false
                ])
                ->add('gallery',null, array(
                    'label' => 'form.gallery'
                ))
            ->end()
            ->with('form.group.shipping', array(
                'box_class'   => 'box box-primary'
            ))
                ->add('width', null, array(
                    'label' => 'form.width'
                ))
                ->add('height', null, array(
                    'label' => 'form.height'
                ))
                ->add('depth', null, array(
                    'label' => 'form.depth'
                ))
                ->add('weight', null, array(
                    'label' => 'form.weight'
                ))
            ->end();

        if($this->getSubject()->getProduct()->getContext() == 'configurable') {
            $formMapper->getFormBuilder()->addEventSubscriber(new BuildProductVariantFormSubscriber($formMapper->getFormBuilder()->getFormFactory()));
        }
    }
}