<?php
namespace AdeShopBundle\Admin;

use AdeShopBundle\Entity\Variant;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ComboProductItemAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('variant', null, array(
                'label' => 'form.variant'
            ))
            ->add('_action', 'actions', array(
                'label' => 'form.actions',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()

                )
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('variant', EntityType::class, array(
                'class' => Variant::class,
                'label' => 'form.productVariants',
                'placeholder' => '',
                'choice_translation_domain' => false,
                'translation_domain' => 'AdeShopBundle',
                'choice_label' => function ($variant) {
                    return $variant->getProductName() . ' ' . twig_localized_currency_filter($variant->getCurrentPriceWithTax(), 'EUR');
                }
            ));
    }
    
}