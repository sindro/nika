<?php
namespace AdeShopBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class OptionValueAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, array(
                'label' => 'form.title'
            ))
            ->add('_action', 'actions', array(
                'label' => 'form.actions',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array(
                'label' => 'form.title'
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {        
        $formMapper
            ->add('title', null, array(
                'label' => 'form.title'
            ))
            ->add('code', null, array(
                'label' => 'form.code'
            ));
    }

    public function prePersist($object)
    {
        parent::prePersist($object);
        $this->setCode($object);
    }

    public function preUpdate($object)
    {
        parent::preUpdate($object);
        $this->setCode($object);
    }

    private function setCode($object)
    {
        $object->setCode($this->clean($object->getOption() . ' ' . $object->getTitle()));
    }

    private function clean($string)
    {
        $string = strtolower(str_replace(' ', '_', $string));

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
}