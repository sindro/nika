<?php
namespace AdeShopBundle\Admin;

use AdeShopBundle\Util\ManageStockFactory;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class OrderAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('firstname', null, array(
                'label' => 'form.firstname'
            ))
            ->add('lastname', null, array(
                'label' => 'form.lastname'
            ))
            ->add('status', null, array(
                'label' => 'form.status'
            ))
            ->add('paymentMethod', null, array(
                'label' => 'form.paymentMethod'
            ))
            ->add('amount', null, array(
                'label' => 'form.amount'
            ))
            ->add('createdAt', null, array(
                'label' => 'form.created'
            ))
            ->add('_action', 'actions', array(
                'label' => 'form.actions',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('firstname', null, array(
                'label' => 'form.firstname'
            ))
            ->add('lastname', null, array(
                'label' => 'form.lastname'
            ))
            ->add('user', ModelAutocompleteFilter::class, array(
                'label' => 'form.categories'
            ), null, array(
                'property' => 'email'
            ))
            ->add('status', null, array(
                'label' => 'form.status'
            ))
            ->add('paymentMethod', null, array(
                'label' => 'form.paymentMethod'
            ))
            ->add('createdAt', null, array(
                'label' => 'form.created'
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {        
        $formMapper
            ->with('form.group.order_data', array(
                'class'       => 'col-md-8',
                'box_class'   => 'box box-primary'
            ))
                ->add('firstname', null, array(
                    'label' => 'form.firstname'
                ))
                ->add('lastname', null, array(
                    'label' => 'form.lastname'
                ))
                ->add('user', null, array(
                    'label' => 'form.user'
                ))
                ->add('email', null, array(
                    'label' => 'form.email'
                ))
                ->add('address', null, array(
                    'label' => 'form.address'
                ))
                ->add('city', null, array(
                    'label' => 'form.city'
                ))
                ->add('country', null, array(
                    'label' => 'form.country'
                ))
                ->add('postalCode', null, array(
                    'label' => 'form.postalCode'
                ))
                ->add('phone', null, array(
                    'label' => 'form.phone'
                ))
            ->end()
            ->with('form.group.order_info', array(
                'class'       => 'col-md-4',
                'box_class'   => 'box box-primary'
            ))
                ->add('status', ChoiceType::class, array(
                    'label' => 'form.status',
                    'choice_translation_domain' => false,
                    'choices' => $this->getStatus()
                ))
                ->add('paymentMethod', null, array(
                    'label' => 'form.paymentMethod'
                ))
                ->add('note', null, array(
                    'label' => 'form.note'
                ))
            ->end()
            ->with('form.group.order_sales', array(
                'class'       => 'col-md-12',
                'box_class'   => 'box box-primary'
            ))
                ->add('items', CollectionType::class, [
                    'label' => 'form.items',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table'
                ])
                ->add('totalAmount', MoneyType::class, array(
                    'label' => 'form.amount'
                ))
            ->end();
    }

    public function preUpdate($object)
    {
        /*$em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();

        $original = $this->getModelManager()->getEntityManager($this->getClass())
                         ->getUnitOfWork()->getOriginalEntityData($object);

        $manageStock = ManageStockFactory::create($object->getStatus(), $object, $em);
        $manageStock->updateQuantityAdmin($original);

        $em->flush();*/

        $uow = $this->getModelManager()->getEntityManager($this->getClass())->getUnitOfWork();
        foreach($object->getItems() as $item)
        {
            $changeset = $uow->getEntityChangeSet($item);
            print_r($changeset);
        }

         die;
    }

    private function getStatus()
    {
        $status = [];

        if($this->getSubject()->getStatus() == 'completed') {
            $status = [
                'Completato' => 'completed',
                'Rimborsato' => 'refunded',
            ];
        } else if($this->getSubject()->getStatus() == 'refunded') {
            $status = [
                'Rimborsato' => 'refunded',
            ];
        } else {
            $status = [
                'In Attesa di pagamento' => 'pending_payment',
                'In lavorazione' => 'processing',
                'In attesa' => 'on_hold',
                'Completato' => 'completed',
                'Rimborsato' => 'refunded',
                'Cancellato' => 'cancelled',
                'Fallito' => 'failed',
            ];
        }

        return $status;
    }

/* public function getFormTheme()
    {
        if($this->isCurrentRoute('edit') && $this->getSubject()->getItems()->count() > 0) {
            return array_merge(parent::getFormTheme(), ['@SonataAdmin/Custom/order_edit.html.twig']);
        }

        return parent::getFormTheme();
    }*/
}