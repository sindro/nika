<?php
namespace AdeShopBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class OrderItemAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('variant', null, array(
                'label' => 'form.variant'
            ))
            ->add('quantity', null, array(
                'label' => 'form.quantity'
            ));
    }
}