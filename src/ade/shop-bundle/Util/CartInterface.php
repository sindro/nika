<?php
namespace AdeShopBundle\Util;

use AdeShopBundle\Entity\Variant;

interface CartInterface
{
    /**
     * @return cart
     */
    public function get();

    /**
     * add item in cart
     */
    public function add(Variant $variant, $quantity);

    /**
     * add item in cart
     */
    public function addCombo(array $variants);

    /**
     * remove item from cart
     */
    public function remove(Variant $variant);

    /**
     * erase cart
     */
    public function erase();

    /**
     * count item cart
     */
    public function count();

    /**
     * check if cart is empty
     */
    public function isEmpty();


    public function getSubTotal(array $variants = null);
}