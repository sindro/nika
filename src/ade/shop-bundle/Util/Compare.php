<?php
namespace AdeShopBundle\Util;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AdeShopBundle\Entity\Variant;

class Compare implements CompareInterface
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function get()
    {
        return $this->session->get('compare', []);
    }

    public function add(Variant $variant)
    {
        $compare = $this->session->get('compare', []);

        if (empty($compare[$variant->getId()]))  {
            $compare[$variant->getId()] = 1;
        }

        $this->session->set('compare', $compare);
    }

    public function remove(Variant $variant)
    {
        $compare = $this->session->get('compare', []);

        unset($compare[$variant->getId()]);

        $this->session->set('compare', $compare);
    }

    public function erase()
    {
        $this->session->set('compare', []);
    }
}