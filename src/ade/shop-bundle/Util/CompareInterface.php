<?php
namespace AdeShopBundle\Util;

use AdeShopBundle\Entity\Variant;

interface CompareInterface
{
    /**
     * @return compare
     */
    public function get();

    /**
     * add item in compare
     */
    public function add(Variant $variant);

    /**
     * remove item from compare
     */
    public function remove(Variant $variant);

    /**
     * erase compare
     */
    public function erase();
}