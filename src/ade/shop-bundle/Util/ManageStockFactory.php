<?php
namespace AdeShopBundle\Util;

class ManageStockFactory
{
    public static function create($status, $order, $em)
    {
        switch($status)
        {
            case 'completed':
                return new ManageStockCompleted($order, $em);
                break;
            case 'refunded':
                return new ManageStockRefunded($order, $em);
                break;
        }
    }
}