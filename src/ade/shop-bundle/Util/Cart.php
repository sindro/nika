<?php
namespace AdeShopBundle\Util;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AdeShopBundle\Entity\Variant;
use Symfony\Component\Serializer\SerializerInterface;

class Cart implements CartInterface
{
    private $session;
    private $serializer;

    public function __construct(SessionInterface $session, SerializerInterface $serializer)
    {
        $this->session = $session;
        $this->serializer = $serializer;
    }

    public function get()
    {
        return $this->session->get('cart', []);
    }

    public function add(Variant $variant, $quantity)
    {
        $cart = $this->session->get('cart', []);

        if (empty($cart[$variant->getId()]))  {
            $cart[$variant->getId()] = $quantity;
        } else {
            $cart[$variant->getId()] = $cart[$variant->getId()] + $quantity;
        }

        $this->session->set('cart', $cart);
    }

    public function addCombo(array $variants)
    {
        $cart = $this->session->get('cart', []);

        foreach($variants as $variant) {
            if (empty($cart[$variant->getId()])) {
                $cart[$variant->getId()] = 1;
            } else {
                $cart[$variant->getId()] = $cart[$variant->getId()] + 1;
            }
        }

        $this->session->set('cart', $cart);
    }

    public function remove(Variant $variant)
    {
        $cart = $this->session->get('cart', []);

        unset($cart[$variant->getId()]);

        $this->session->set('cart', $cart);
    }

    public function getSubTotal(array $variants = null)
    {
        $cart = $this->session->get('cart', []);

        $total = 0.00;

        foreach ($cart as $pid => $q) {
            if (isset($variants[$pid])) {
                $total += $variants[$pid]->getCurrentPriceWithTax() * $q;
            }
        }

        return $total;
    }

    public function count()
    {
        $cart = $this->session->get('cart', []);

        return count($cart);
    }

    public function isEmpty()
    {
        if($this->count() == 0) {
            return true;
        }

        return false;
    }

    public function erase()
    {
        $this->session->set('cart', []);
    }
}