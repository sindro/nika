<?php
namespace AdeShopBundle\Util;

use AdeShopBundle\Entity\Order;
use Doctrine\ORM\EntityManager;

class ManageStockRefunded
{
    private $order;
    private $em;

    public function __construct(Order $order, EntityManager $em)
    {
        $this->order = $order;
        $this->em = $em;
    }

    public function updateQuantity()
    {
        if($this->getOrderStatus())
        {
            foreach ($this->order->getItems() as $item)
            {
                $variant = $item->getVariant();
                $variant->setInStock($variant->getInStock() + $item->getQuantity());
            }
        }
    }

    private function getOrderStatus()
    {
        $originalEntityData = $this->em->getUnitOfWork()->getOriginalEntityData($this->order);

        return $this->order->getStatus() == 'refunded' && $this->order->getStatus() != $originalEntityData['status'];
    }
}