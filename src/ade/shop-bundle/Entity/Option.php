<?php
namespace AdeShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="ade_shop_option")
 * @ORM\Entity()
 * @UniqueEntity({"title", "code"})
 */
class Option
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $title;

    /**
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    protected $code;

    /**
     * @ORM\OneToMany(targetEntity="OptionValue", mappedBy="option", cascade={"persist"}, orphanRemoval=true)
     * @Assert\Valid
     * @Assert\Count(min=1)
     */
    protected $values;

    use ORMBehaviors\Sortable\Sortable;
    
    public function __construct()
    {
        $this->values = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return collection
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param $values
     */
    public function setValues($values)
    {
        if (count($values) > 0) {
            foreach ($values as $value) {
                $this->addValue($value);
            }
        }
    }

    /**
     * @param OptionValue $value
     */
    public function addValue(OptionValue $value)
    {
        $this->values[] = $value;

        $value->setOption($this);
    }

    /**
     * Remove value
     */
    public function removeValue(OptionValue $value)
    {
        $this->values->removeElement($value);
    }

    public function __toString()
    {
        return (string) $this->getTitle();
    }
}