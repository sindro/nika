<?php
namespace AdeShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="ade_shop_combo_product_item")
 * @ORM\Entity
 */
class ComboProductItem
{
    /** 
     * @ORM\Id 
     * @ORM\GeneratedValue 
     * @ORM\Column(type="integer") 
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Variant", cascade={"persist"})
     * @ORM\JoinColumn(name="variant_id", referencedColumnName="id", nullable=true)
     * @Assert\NotBlank()
     */
    protected $variant;
    
    /**
     * @ORM\ManyToOne(targetEntity="ComboProduct", inversedBy="comboProductItems", cascade={"persist"})
     * @ORM\JoinColumn(name="combo_product_item_id", referencedColumnName="id")
     */
    protected $comboProduct;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * @param mixed $variant
     */
    public function setVariant(Variant $variant = null): void
    {
        $this->variant = $variant;
    }

    /**
     * @return mixed
     */
    public function getComboProduct()
    {
        return $this->comboProduct;
    }

    /**
     * @param mixed $comboProduct
     */
    public function setComboProduct($comboProduct): void
    {
        $this->comboProduct = $comboProduct;
    }

    public function __toString()
    {
       return (string) $this->getVariant();
    }
}
