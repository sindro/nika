<?php
namespace AdeShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Payum\Core\Model\Payment as BasePayment;
use App\Validator\Constraints as CustomAssert;

/**
 * @ORM\Table(name="ade_shop_order")
 * @ORM\Entity(repositoryClass="AdeShopBundle\Repository\OrderRepository")
 * @CustomAssert\SameItemInCollection(
 *      variable="variant",
        collection="items",
        errorPath="form,field",
        message="Valore già utilizzato"
 * )
 */
class Order extends BasePayment
{
    /** 
     * @ORM\Id 
     * @ORM\GeneratedValue 
     * @ORM\Column(type="integer") 
     */
    protected $id;

    /** 
     * @ORM\Column(name="numberBill", type="integer", nullable=true) 
     */
    protected $numberBill;
    
    /**
     * @ORM\Column(name="dateBill", type="date", nullable=true)
     */
    protected $dateBill;
    
    /**
     * @ORM\Column(name="tracking", type="string", length=500, nullable=true)
     */
    protected $tracking;
    
    /**
     * @ORM\Column(name="status", type="string", length=64, nullable=true)
     */
    protected $status;
    
    /**
     * @ORM\Column(name="firstname", type="string", length=100)
     * @Assert\NotBlank()
     */
    protected $firstname;

    /**
     * @ORM\Column(name="lastname", type="string", length=100)
     * @Assert\NotBlank()
     */
    protected $lastname;

    /**
     * @ORM\Column(name="company", type="string", length=100, nullable=true)
     */
    protected $company;

    /**
     * @ORM\Column(name="vat", type="string", length=32, nullable=true)
     */
    protected $vat;

    /**
     * @ORM\Column(name="email", type="string", length=180)
     * @Assert\NotBlank()
     */
    protected $email;

    /**
     * @ORM\Column(name="fiscal_code", type="string", length=16)
     * @Assert\NotBlank()
     */
    protected $fiscalCode;

    /**
     * @ORM\Column(name="phone", type="string", length=30)
     * @Assert\NotBlank()
     */
    protected $phone;

    /**
     * @ORM\Column(name="address", type="string", length=100)
     * @Assert\NotBlank()
     */
    protected $address;

    /**
     * @ORM\Column(name="postal_code", type="string")
     * @Assert\NotBlank()
     */
    protected $postalCode;

    /**
     * @ORM\Column(name="city", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $city;
    
    /**
     * @ORM\Column(name="country", type="string", length=100, nullable=true)
     * @Assert\NotBlank()
     */
    protected $country;

    /**
     * @ORM\ManyToOne(targetEntity="PaymentMethod", cascade={"persist"})
     * @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id", nullable=true)
     */
    protected $paymentMethod;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="orders", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(name="note", type="text", length=500, nullable=true)
     */
    protected $note;

    /**
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="order", cascade={"persist"}, orphanRemoval=true)
     * @Assert\Valid
     */
    protected $items;

    /**
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2)
     */
    protected $amount;

    use ORMBehaviors\Timestampable\Timestampable;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNumberBill()
    {
        return $this->numberBill;
    }

    /**
     * @param mixed $numberBill
     */
    public function setNumberBill($numberBill): void
    {
        $this->numberBill = $numberBill;
    }

    /**
     * @return mixed
     */
    public function getDateBill()
    {
        return $this->dateBill;
    }

    /**
     * @param mixed $dateBill
     */
    public function setDateBill($dateBill): void
    {
        $this->dateBill = $dateBill;
    }

    /**
     * @return mixed
     */
    public function getTracking()
    {
        return $this->tracking;
    }

    /**
     * @param mixed $tracking
     */
    public function setTracking($tracking): void
    {
        $this->tracking = $tracking;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company): void
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param mixed $vat
     */
    public function setVat($vat): void
    {
        $this->vat = $vat;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getFiscalCode()
    {
        return $this->fiscalCode;
    }

    /**
     * @param mixed $fiscalCode
     */
    public function setFiscalCode($fiscalCode): void
    {
        $this->fiscalCode = $fiscalCode;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     */
    public function setPostalCode($postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param mixed $paymentMethod
     */
    public function setPaymentMethod($paymentMethod): void
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note): void
    {
        $this->note = $note;
    }

    public function populateUserData($user)
    {
        $this->setUser($user);

        if($this->getUser()) {
            $dataProfile = ['firstname', 'lastname', 'email', 'phone', 'country', 'address', 'city', 'postalCode', 'company', 'vat', 'fiscalCode'];

            foreach ($dataProfile as $data) {
                $setter = 'set'.ucfirst($data);
                $getter = 'get'.ucfirst($data);
                $this->$setter($this->getUser()->$getter());
            }
        }
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    public function setItems($items)
    {
        if (count($items) > 0) {
            foreach ($items as $item) {
                $this->addItem($item);
            }
        }
    }

    public function setItemsByCart($cart, $variants)
    {
        foreach ($cart as $id => $q) {
            $item = new OrderItem();
            $item->setVariant($variants[$id]);
            $item->setQuantity($q);
            $this->addItem($item);
        }
    }

    /**
     * @param Item $item
     */
    public function addItem(OrderItem $item)
    {
        $this->items[] = $item;

        $item->setOrder($this);
    }

    /**
     * Remove item
     */
    public function removeItem(OrderItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }
}
