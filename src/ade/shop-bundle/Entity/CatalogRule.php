<?php
namespace AdeShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="ade_shop_catalog_rule")
 * @ORM\Entity(repositoryClass="AdeShopBundle\Repository\CatalogRuleRepository")
 */
class CatalogRule
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(name="context", type="string", length=64)
     */
    protected $context;

    /**
     * @ORM\Column(name="discount", type="decimal", nullable=true)
     * @Assert\NotBlank(groups={"product", "variant", "category", "manufacturer", "author", "supplier"})
     */
    protected $discount;

    /**
     * @ORM\Column(name="start_date", type="date", nullable=true)
     * @Assert\Expression(
     *     "not (this.getStartDate() > this.getEndDate())",
     *     message="form.validation.discount_start_date",
     *     groups={"product", "variant", "category", "manufacturer", "author", "supplier"}
     * )
     */
    protected $startDate;

    /**
     * @ORM\Column(name="end_date", type="date", nullable=true)
     * @Assert\Expression(
     *     "not (this.getEndDate() < this.getStartDate())",
     *     message="form.validation.discount_end_date",
     *     groups={"product", "variant", "category", "manufacturer", "author", "supplier"}
     * )
     */
    protected $endDate;

    /**
     * @ORM\ManyToOne(targetEntity="Product", cascade={"persist"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=true)
     * @Assert\NotBlank(groups={"product"})
     */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity="Category", cascade={"persist"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     * @Assert\NotBlank(groups={"category"})
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="Manufacturer", cascade={"persist"})
     * @ORM\JoinColumn(name="manufacturer_id", referencedColumnName="id", nullable=true)
     * @Assert\NotBlank(groups={"manufacturer"})
     */
    protected $manufacturer;

    /**
     * @ORM\ManyToOne(targetEntity="Author", cascade={"persist"})
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true)
     * @Assert\NotBlank(groups={"author"})
     */
    protected $author;

    /**
     * @ORM\ManyToOne(targetEntity="Supplier", cascade={"persist"})
     * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id", nullable=true)
     * @Assert\NotBlank(groups={"supplier"})
     */
    protected $supplier;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param mixed $context
     */
    public function setContext($context)
    {
        $this->context = $context;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor(Author $author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @param mixed $manufacturer
     */
    public function setManufacturer(Manufacturer $manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    /**
     * @return mixed
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param mixed $supplier
     */
    public function setSupplier(Supplier $supplier)
    {
        $this->supplier = $supplier;
    }

    public function __toString()
    {
        return (string) $this->getTitle();
    }
}