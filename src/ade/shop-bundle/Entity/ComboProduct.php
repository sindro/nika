<?php
namespace AdeShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Table(name="ade_shop_combo_product")
 * @ORM\Entity(repositoryClass="AdeShopBundle\Repository\ComboProductRepository")
 */
class ComboProduct
{
    /** 
     * @ORM\Id 
     * @ORM\GeneratedValue 
     * @ORM\Column(type="integer") 
     */
    protected $id;
    
    /**
     * @ORM\OneToMany(targetEntity="ComboProductItem", mappedBy="comboProduct", cascade={"persist"}, orphanRemoval=true)
     * @Assert\Valid
     * @Assert\Count(min="1")
     */
    protected $comboProductItems;


    use ORMBehaviors\Timestampable\Timestampable;

    public function __construct()
    {
        $this->comboProductItems = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getComboProductItems()
    {
        return $this->comboProductItems;
    }

    public function setComboProductItems($comboProductItems)
    {
        if (count($comboProductItems) > 0) {
            foreach ($comboProductItems as $comboProductItem) {
                $this->addComboProductItem($comboProductItem);
            }
        }
    }

    /**
     * @param comboProductItem $comboProductItem
     */
    public function addComboProductItem(ComboProductItem $comboProductItem)
    {
        $this->comboProductItems[] = $comboProductItem;

        $comboProductItem->setComboProduct($this);
    }

    /**
     * Remove ComboProductItem
     */
    public function removeComboProductItem(ComboProductItem $comboProductItem)
    {
        $this->comboProductItems->removeElement($comboProductItem);
    }

    public function getAmount()
    {
        $amount = 0.00;

        foreach ($this->comboProductItems as $comboProductItem) {
            $amount += $comboProductItem->getVariant()->getCurrentPriceWithTax();
        }

        return $amount;
    }

    public function getVariantsIds()
    {
        $ids = [];

        foreach ($this->comboProductItems as $comboProductItem) {
            $ids []= $comboProductItem->getVariant()->getId();
        }

        return implode(',', $ids);
    }
}
