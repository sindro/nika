<?php
namespace AdeShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="ade_shop_product", indexes={@ORM\Index(columns={"title","description"},flags={"fulltext"})})
 * @ORM\Entity(repositoryClass="AdeShopBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $title;

    /**
     * @ORM\Column(name="subtitle", type="string", length=500, nullable=true)
     */
    protected $subtitle;

    /**
     * @ORM\Column(name="context", type="string", length=64)
     */
    protected $context;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(name="is_public", type="boolean")
     */
    protected $isPublic = true;

    /**
     * @ORM\OneToMany(targetEntity="Variant", mappedBy="product", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"price" = "ASC"})
     */
    protected $variants;

    /**
     * @ORM\ManyToMany(targetEntity="Category", cascade={"persist"}, inversedBy="products", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="ade_shop_products_categories",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true)}
     *      )
     */
    protected $categories;

    /**
     * @ORM\ManyToOne(targetEntity="Manufacturer", cascade={"persist"}, inversedBy="products", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="manufacturer_id", referencedColumnName="id", nullable=true)
     */
    protected $manufacturer;

    /**
     * @ORM\ManyToMany(targetEntity="Author", cascade={"persist"})
     * @ORM\JoinTable(name="ade_shop_products_authors",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true)}
     *      )
     */
    protected $authors;

    /**
     * @ORM\ManyToOne(targetEntity="Supplier", cascade={"persist"})
     * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id", nullable=true)
     */
    protected $supplier;

    /**
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="products")
     */
    protected $productsWithProduct;

    /**
     * @ORM\ManyToMany(targetEntity="Product", inversedBy="productsWithProduct")
     * @ORM\JoinTable(name="ade_shop_product_products",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_product_id", referencedColumnName="id")}
     *      )
     */
    protected $products;

    /**
     * @ORM\ManyToMany(targetEntity="Option", cascade={"persist"})
     * @ORM\JoinTable(name="ade_shop_product_options",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", unique=false, nullable=false, onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="option_id", referencedColumnName="id", unique=false, nullable=false, onDelete="CASCADE")}
     *      )
     */
    protected $options;

    use ORMBehaviors\Timestampable\Timestampable;

    public function __construct()
    {
        $this->variants = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->authors = new ArrayCollection();
        $this->productsWithProduct = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->options = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @param mixed $subtitle
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param mixed $context
     */
    public function setContext($context)
    {
        $this->context = $context;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * @param mixed $isPublic
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;
    }

    /**
     * @return mixed
     */
    public function getVariants()
    {
        return $this->variants;
    }

    public function setVariants($variants)
    {
        if (count($variants) > 0) {
            foreach ($variants as $variant) {
                $this->addVariant($variant);
            }
        }
    }

    /**
     * @param Variant $variant
     */
    public function addVariant(Variant $variant)
    {
        $this->variants[] = $variant;

        $variant->setProduct($this);
    }

    /**
     * Remove variant
     */
    public function removeVariant(Variant $variant)
    {
        $this->variants->removeElement($variant);
    }

    /**
     * Get categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Get categories to array
     */
    public function getCategoriesToArray()
    {
        $categories = [];

        foreach($this->getCategories() as $category) {
            $categories []= $category->getTitle();
        }

        return $categories;
    }

    /**
     * Set categories
     */
    public function setCategories($categories)
    {
        if (count($categories) > 0) {
            foreach ($categories as $category) {
                $this->addCategory($category);
            }
        }
    }

    /**
     * Add category
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;
    }

    /**
     * Remove category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    public function hasCategory($title)
    {
        return in_array($title, $this->getCategoriesToArray());
    }

    /**
     * Set manufacturer
     */
    public function setManufacturer(Manufacturer $manufacturer = null)
    {
        $this->manufacturer = $manufacturer;
    }

    /**
     * Get manufacturer
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Get authors
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Set authors
     */
    public function setAuthors($authors)
    {
        if (count($authors) > 0) {
            foreach ($authors as $author) {
                $this->addAuthor($author);
            }
        }
    }

    /**
     * Add author
     */
    public function addAuthor(Author $author)
    {
        $this->authors[] = $author;
    }

    /**
     * Remove author
     */
    public function removeAuthor(Author $author)
    {
        $this->authors->removeElement($author);
    }

    /**
     * Set supplier
     */
    public function setSupplier(Supplier $supplier = null)
    {
        $this->supplier = $supplier;
    }

    /**
     * Get supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Get productsWithProduct
     */
    public function getProductsWithProduct()
    {
        return $this->productsWithProduct;
    }

    /**
     * Add productsWithProduct
     */
    public function addProductsWithProduct(Product $productsWithProduct)
    {
        $this->productsWithProduct[] = $productsWithProduct;
    }

    /**
     * Remove productsWithProduct
     */
    public function removeProductsWithProduct(Product $productsWithProduct)
    {
        $this->productsWithProduct->removeElement($productsWithProduct);
    }

    /**
     * Get products
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add product
     */
    public function addProduct(Product $product)
    {
        $this->products[] = $product;
    }

    /**
     * Remove product
     */
    public function removeProduct(Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get options
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set options
     */
    public function setOptions($options)
    {
        if (count($options) > 0) {
            foreach ($options as $option) {
                $this->addOption($option);
            }
        }
    }

    /**
     * Add option
     */
    public function addOption(Option $option)
    {
        $this->options[] = $option;
    }

    /**
     * Remove option
     */
    public function removeOption(Option $option)
    {
        $this->options->removeElement($option);
    }

    public function getOptionsToArray()
    {
        $options = [];

        foreach ($this->options as $option) {
            foreach($option->getValues() as $value)
                $options[$option->getTitle()][] = $value->getTitle();
        }

        return $options;
    }

    public function hasOptions(): bool
    {
        return !$this->options->isEmpty();
    }

    use ORMBehaviors\Sluggable\Sluggable;

    public function getSluggableFields()
    {
        return [ 'title' ];
    }

    public function generateSlugValue($values)
    {
        return implode('-', $values);
    }

    public function __toString()
    {
        return (string) $this->getTitle();
    }
}