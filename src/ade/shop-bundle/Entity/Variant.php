<?php
namespace AdeShopBundle\Entity;

use AdeGalleryBundle\Entity\Gallery;
use AdeShopBundle\Entity\Behaviors\Price;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table(name="ade_shop_variant")
 * @ORM\Entity(repositoryClass="AdeShopBundle\Repository\VariantRepository")
 * @Vich\Uploadable
 */
class Variant
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="sku", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    protected $sku;

    /**
     * @Vich\UploadableField(mapping="variant_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $imageName;

    protected $imageSize;

    /**
     * @ORM\Column(name="isbn", type="string", length=64, nullable=true)
     * @Assert\Isbn(type = "{isbn10, isbn13}")
     */
    protected $isbn;

    /**
     * @ORM\Column(name="price", type="decimal", precision=10, scale=4, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     */
    protected $price;

    /**
     * @ORM\Column(name="in_stock", type="integer")
     * @Assert\NotBlank()
     * @Assert\Type("int")
     */
    protected $inStock = 0;

    /**
     * @ORM\Column(name="height", type="decimal", precision=10, scale=2, nullable=true)
     */
    protected $height;

    /**
     * @ORM\Column(name="width", type="decimal", precision=10, scale=2, nullable=true)
     */
    protected $width;

    /**
     * @ORM\Column(name="depth", type="decimal", precision=10, scale=2, nullable=true)
     */
    protected $depth;

    /**
     * @ORM\Column(name="weight", type="decimal", precision=10, scale=2, nullable=true)
     */
    protected $weight;

    /**
     * @ORM\Column(name="short_description", type="text", nullable=true)
     */
    protected $shortDescription;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="variants", cascade={"persist"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity="AdeGalleryBundle\Entity\Gallery", cascade={"persist"})
     * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id")
     */
    protected $gallery;

    /**
     * @ORM\ManyToOne(targetEntity="AdeShopBundle\Entity\Tax", cascade={"persist"})
     * @Assert\NotBlank()
     */
    protected $tax;

    /**
     * @ORM\ManyToMany(targetEntity="OptionValue")
     * @ORM\JoinTable(name="ade_shop_variant_option_value",
     *      joinColumns={@ORM\JoinColumn(name="variant_id", referencedColumnName="id", unique=false, nullable=false, onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="option_value_id", referencedColumnName="id", unique=false, nullable=false, onDelete="CASCADE")}
     *      )
     */
    protected $optionValues;

    use Price;
    use ORMBehaviors\Sortable\Sortable;

    public function __construct()
    {
        $this->optionValues = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @param mixed $shortDescription
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(?File $image = null): void
    {
        $this->imageFile = $image;

        if (null !== $image) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }

    /**
     * @return mixed
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;
    }

    /**
     * @param mixed $isbn
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * @return mixed
     */
    public function getInStock()
    {
        return $this->inStock;
    }

    /**
     * @param mixed $inStock
     */
    public function setInStock($inStock)
    {
        $this->inStock = $inStock;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * @param mixed $depth
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @param mixed $gallery
     */
    public function setGallery(Gallery $gallery = null)
    {
        $this->gallery = $gallery;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param mixed $tax
     */
    public function setTax(Tax $tax)
    {
        $this->tax = $tax;
    }

    /**
     * Get optionValues
     */
    public function getOptionValues()
    {
        return $this->optionValues;
    }

    /**
     * Set optionValues
     */
    public function setOptionValues($optionValues)
    {
        if (count($optionValues) > 0) {
            foreach ($optionValues as $optionValue) {
                $this->addOptionValue($optionValue);
            }
        }
    }

    /**
     * Add optionValue
     */
    public function addOptionValue(OptionValue $optionValue)
    {
        $this->optionValues[] = $optionValue;
    }

    /**
     * Remove optionValue
     */
    public function removeOptionValue(OptionValue $optionValue)
    {
        $this->optionValues->removeElement($optionValue);
    }

    public function hasOptionValue(OptionValue $optionValue): bool
    {
        return $this->optionValues->contains($optionValue);
    }

    public function getSpecifications()
    {
        $specifications = $this->getProduct()->getOptionsToArray();

        if($this->getWidth())
        {
            $specifications['width'] = $this->getWidth();
        }

        if($this->getHeight())
        {
            $specifications['height'] = $this->getHeight();
        }

        if($this->getDepth())
        {
            $specifications['depth'] = $this->getDepth();
        }

        if($this->getWeight())
        {
            $specifications['weight'] = $this->getWeight();
        }

        return $specifications;
    }

    public function getProductName()
    {
        $name = $this->getProduct() . ' ';

        if($this->getOptionValues()->count() > 0) {
            $name .= join(',', $this->getOptionValues()->toArray());
        } else {
            $name .= ($this->getProduct()->getVariants()->count() > 1) ? $this->getSku() : '';
        }

        return $name;
    }

    public function __toString()
    {
        return (string) $this->getProductName();
    }
}