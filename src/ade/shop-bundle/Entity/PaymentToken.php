<?php
namespace AdeShopBundle\Entity;

use Payum\Core\Model\Token as BaseToken;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="ade_shop_payment_token")
 * @ORM\Entity()
 */
class PaymentToken extends BaseToken
{

}