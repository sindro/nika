<?php
namespace AdeShopBundle\Entity\Behaviors;

trait Price
{
    private $currentPrice;

    private $currentDiscount;

    private $saving;

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getCurrentPrice()
    {
        return $this->currentPrice;
    }

    /**
     * @param mixed $currentPrice
     */
    public function setCurrentPrice($currentPrice)
    {
        $this->currentPrice = $currentPrice;
    }

    /**
     * @return mixed
     */
    public function getCurrentDiscount()
    {
        return $this->currentDiscount;
    }

    /**
     * @param mixed $currentDiscount
     */
    public function setCurrentDiscount($currentDiscount)
    {
        $this->currentDiscount = $currentDiscount;
    }

    /**
     * Return price with iva
     */
    public function getPriceWithTax()
    {
        if(null === $this->getTax()) {
            return number_format($this->getPrice(), 2, '.','');
        }

        return number_format($this->getPrice() + ($this->price * $this->getTax()->getPercentage())/100, 2, '.','');;
    }

    public function getCurrentPriceWithTax()
    {
        if(null === $this->getTax()) {
            return number_format($this->getCurrentPrice(), 2, '.','');
        }

        return number_format($this->getCurrentPrice() + ($this->getCurrentPrice() * $this->getTax()->getPercentage())/100, 2, '.','');
    }

    public function getSaving()
    {
        return number_format($this->getPriceWithTax() - $this->getCurrentPriceWithTax(), 2, '.','');
    }
}