<?php
namespace AdeShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="ade_shop_order_item")
 * @ORM\Entity
 */
class OrderItem
{
    /** 
     * @ORM\Id 
     * @ORM\GeneratedValue 
     * @ORM\Column(type="integer") 
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Variant", cascade={"persist"})
     * @ORM\JoinColumn(name="variant_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    protected $variant;

    /**
     * @ORM\Column(name="quantity", type="integer")
     * @Assert\NotBlank()
     */
    protected $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="items", cascade={"persist"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    protected $order;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * @param mixed $variant
     */
    public function setVariant(Variant $variant = null)
    {
        $this->variant = $variant;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }

    public function __toString()
    {
        return (string) $this->getVariant();
    }
}
