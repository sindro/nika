<?php

namespace AdeShopBundle\Event;

use Symfony\Component\HttpFoundation\Response;

class GetResponseOrderEvent extends OrderEvent
{
    /**
     * @var Response
     */
    private $response;

    /**
     * @param Response $response
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
}
