<?php

namespace AdeShopBundle\Event;

use AdeShopBundle\Entity\Order;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

class OrderEvent extends Event
{
    /**
     * @var null|Request
     */
    protected $request;

    /**
     * @var Order
     */
    protected $order;

    /**
     * OrderEvent constructor.
     *
     * @param Order $order
     * @param Request|null  $request
     */
    public function __construct(Order $order, Request $request = null)
    {
        $this->order = $order;
        $this->request = $request;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }
}
