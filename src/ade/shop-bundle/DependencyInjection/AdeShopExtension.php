<?php

namespace AdeShopBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class AdeShopExtension extends Extension implements PrependExtensionInterface
{
    private $bundleConfigs;

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');

        $this->configureParameterSetting($container, $config);
        $this->configureParameterEntity($container, $config);

        $bundles = $container->getParameter('kernel.bundles');

        if(isset($bundles['SonataAdminBundle'])) {
            $this->configureParameterAdmin($container, $config);
            $loader->load('admin.yaml');
        }
    }

    public function prepend(ContainerBuilder $container)
    {
        $bundles = $container->getParameter('kernel.bundles');

        if (isset($bundles['SonataAdminBundle'])) {
            $this->bundleConfigs['Twig'] = current($container->getExtensionConfig('twig'));
            $this->bundleConfigs['SonataAdminBundle'] = current($container->getExtensionConfig('sonata_admin'));
            $twigConfig['form_themes'] = ['SonataCoreBundle:Form:datepicker.html.twig'];
            $twigConfig['globals'] = ['currency' => '€'];

            $sonataAdminConfig['dashboard']['groups']['admin.shop'] = [
                'label' => 'ade_shop',
                'label_catalogue' => 'AdeShopBundle',
                'icon' => '<i class="fa fa-shopping-cart"></i>',
                'items' => ['admin.product', 'admin.category', 'admin.author', 'admin.manufacturer', 'admin.supplier', 'admin.tax', 'admin.catalogRule', 'admin.option', 'admin.comboProduct', 'admin.paymentMethod', 'admin.order']
            ];

            $container->prependExtensionConfig('sonata_admin', array_merge($this->bundleConfigs['SonataAdminBundle'], $sonataAdminConfig));
            $container->prependExtensionConfig('twig', array_merge($this->bundleConfigs['Twig'], $twigConfig));
        }
    }

    public function configureParameterSetting(ContainerBuilder $container, array $config)
    {
        $container->setParameter('ade_shop.currency', $config['setting']['currency']);
        $container->setParameter('ade_shop.currency_code', $config['setting']['currency_code']);
        $container->setParameter('ade_shop.form.template.autocomplete_add_template', $config['setting']['form']['template']['autocomplete_add_template']);
    }

    public function configureParameterEntity(ContainerBuilder $container, array $config)
    {
        $container->setParameter('ade_shop.entity.product', $config['entity']['product']);
        $container->setParameter('ade_shop.entity.variant', $config['entity']['variant']);
        $container->setParameter('ade_shop.entity.author', $config['entity']['author']);
        $container->setParameter('ade_shop.entity.category', $config['entity']['category']);
        $container->setParameter('ade_shop.entity.manufacturer', $config['entity']['manufacturer']);
        $container->setParameter('ade_shop.entity.supplier', $config['entity']['supplier']);
        $container->setParameter('ade_shop.entity.tax', $config['entity']['tax']);
        $container->setParameter('ade_shop.entity.catalogRule', $config['entity']['catalogRule']);
        $container->setParameter('ade_shop.entity.option', $config['entity']['option']);
        $container->setParameter('ade_shop.entity.optionValue', $config['entity']['optionValue']);
        $container->setParameter('ade_shop.entity.comboProduct', $config['entity']['comboProduct']);
        $container->setParameter('ade_shop.entity.comboProductItem', $config['entity']['comboProductItem']);
        $container->setParameter('ade_shop.entity.paymentMethod', $config['entity']['paymentMethod']);
        $container->setParameter('ade_shop.entity.order', $config['entity']['order']);
        $container->setParameter('ade_shop.entity.orderItem', $config['entity']['orderItem']);
    }

    public function configureParameterAdmin(ContainerBuilder $container, array $config)
    {
        $container->setParameter('ade_shop.admin.product', $config['admin']['product']);
        $container->setParameter('ade_shop.admin.variant', $config['admin']['variant']);
        $container->setParameter('ade_shop.admin.author', $config['admin']['author']);
        $container->setParameter('ade_shop.admin.category', $config['admin']['category']);
        $container->setParameter('ade_shop.admin.manufacturer', $config['admin']['manufacturer']);
        $container->setParameter('ade_shop.admin.supplier', $config['admin']['supplier']);
        $container->setParameter('ade_shop.admin.tax', $config['admin']['tax']);
        $container->setParameter('ade_shop.admin.catalogRule', $config['admin']['catalogRule']);
        $container->setParameter('ade_shop.admin.option', $config['admin']['option']);
        $container->setParameter('ade_shop.admin.optionValue', $config['admin']['optionValue']);
        $container->setParameter('ade_shop.admin.comboProduct', $config['admin']['comboProduct']);
        $container->setParameter('ade_shop.admin.comboProductItem', $config['admin']['comboProductItem']);
        $container->setParameter('ade_shop.admin.paymentMethod', $config['admin']['paymentMethod']);
        $container->setParameter('ade_shop.admin.order', $config['admin']['order']);
        $container->setParameter('ade_shop.admin.orderItem', $config['admin']['orderItem']);
    }
}