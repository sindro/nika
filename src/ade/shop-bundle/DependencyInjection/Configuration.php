<?php
namespace AdeShopBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ade_shop');

        $this->addSettingSection($rootNode);
        $this->addEntitySection($rootNode);
        $this->addAdminSection($rootNode);
        
        return $treeBuilder;
    }
    
    private function addSettingSection(ArrayNodeDefinition $node)
    {
       $node
            ->children()
                ->arrayNode('setting')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('currency')->defaultValue('€')->end()
                        ->scalarNode('currency_code')->defaultValue('EUR')->end()
                        ->arrayNode('form')
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->arrayNode('template')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('autocomplete_add_template')->defaultValue('@AdeShop/Admin/Form/sonata_type_model_autocomplete_add.html.twig')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ; 
    }
    
    private function addEntitySection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->arrayNode('entity')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('product')->defaultValue('AdeShopBundle\\Entity\\Product')->end()
                        ->scalarNode('variant')->defaultValue('AdeShopBundle\\Entity\\Variant')->end()
                        ->scalarNode('author')->defaultValue('AdeShopBundle\\Entity\\Author')->end()
                        ->scalarNode('category')->defaultValue('AdeShopBundle\\Entity\\Category')->end()
                        ->scalarNode('manufacturer')->defaultValue('AdeShopBundle\\Entity\\Manufacturer')->end()
                        ->scalarNode('supplier')->defaultValue('AdeShopBundle\\Entity\\Supplier')->end()
                        ->scalarNode('tax')->defaultValue('AdeShopBundle\\Entity\\Tax')->end()
                        ->scalarNode('catalogRule')->defaultValue('AdeShopBundle\\Entity\\CatalogRule')->end()
                        ->scalarNode('option')->defaultValue('AdeShopBundle\\Entity\\Option')->end()
                        ->scalarNode('optionValue')->defaultValue('AdeShopBundle\\Entity\\OptionValue')->end()
                        ->scalarNode('comboProduct')->defaultValue('AdeShopBundle\\Entity\\ComboProduct')->end()
                        ->scalarNode('comboProductItem')->defaultValue('AdeShopBundle\\Entity\\ComboProductItem')->end()
                        ->scalarNode('paymentMethod')->defaultValue('AdeShopBundle\\Entity\\PaymentMethod')->end()
                        ->scalarNode('order')->defaultValue('AdeShopBundle\\Entity\\Order')->end()
                        ->scalarNode('orderItem')->defaultValue('AdeShopBundle\\Entity\\OrderItem')->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    private function addAdminSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->arrayNode('admin')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('product')->defaultValue('AdeShopBundle\\Admin\\ProductAdmin')->end()
                        ->scalarNode('variant')->defaultValue('AdeShopBundle\\Admin\\VariantAdmin')->end()
                        ->scalarNode('author')->defaultValue('AdeShopBundle\\Admin\\AuthorAdmin')->end()
                        ->scalarNode('category')->defaultValue('AdeShopBundle\\Admin\\CategoryAdmin')->end()
                        ->scalarNode('manufacturer')->defaultValue('AdeShopBundle\\Admin\\ManufacturerAdmin')->end()
                        ->scalarNode('supplier')->defaultValue('AdeShopBundle\\Admin\\SupplierAdmin')->end()
                        ->scalarNode('tax')->defaultValue('AdeShopBundle\\Admin\\TaxAdmin')->end()
                        ->scalarNode('catalogRule')->defaultValue('AdeShopBundle\\Admin\\CatalogRuleAdmin')->end()
                        ->scalarNode('option')->defaultValue('AdeShopBundle\\Admin\\OptionAdmin')->end()
                        ->scalarNode('optionValue')->defaultValue('AdeShopBundle\\Admin\\OptionValueAdmin')->end()
                        ->scalarNode('comboProduct')->defaultValue('AdeShopBundle\\Admin\\ComboProductAdmin')->end()
                        ->scalarNode('comboProductItem')->defaultValue('AdeShopBundle\\Admin\\ComboProductItemAdmin')->end()
                        ->scalarNode('paymentMethod')->defaultValue('AdeShopBundle\\Admin\\PaymentMethodAdmin')->end()
                        ->scalarNode('order')->defaultValue('AdeShopBundle\\Admin\\OrderAdmin')->end()
                        ->scalarNode('orderItem')->defaultValue('AdeShopBundle\\Admin\\OrderItemAdmin')->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
