const routes = require('../../../../../../public/js/fos_js_routes.json');
import Routing from '../../../../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);
require('owl.carousel');
import React from "react";
import ReactDOM from "react-dom";

class ProductTax extends React.Component
{
    constructor(props) {
        super(props);
    }

    render() {
        return <strong>{this.props.tax}</strong>
    }
}

class ProductSaving extends React.Component
{
    constructor(props) {
        super(props);
    }

    render() {
        if(this.props.currentPrice != this.props.price) {
            return <span>{this.props.saving}</span>
        } else {
            return(null)
        }
    }
}

class ProductSpecifications extends React.Component
{
    constructor(props) {
        super(props);
    }

    render() {
        const specifications = Object.entries(this.props.specifications).map(([key,value]) =>
            <tr key={key}>
                <td>{key}</td>
                <td>{value.toString()}</td>
            </tr>
        );
        return <tbody>{specifications}</tbody>
    }
}

class ProductShortDescription extends React.Component
{
    constructor(props) {
        super(props);
    }

    render(){
        return <p>{this.props.shortDescription}</p>
    }
}

class ProductInStock extends React.Component
{
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.inStock > 0) {
            return <div className="availability in-stock">
                <span>{productConfigurable.translations.available}</span>
            </div>
        } else {
            return <div className="availability out-of-stock">
                <span>{productConfigurable.translations.noavailable}</span>
            </div>
        }
    }
}

class ProductSku extends React.Component
{
    constructor(props) {
        super(props);
    }

    render(){
        return <em>{this.props.sku}</em>
    }
}

class ProductPrice extends React.Component
{
    constructor(props) {
        super(props);
    }

    render(){
        const text = this.props.isChanged ? '' : productConfigurable.translations.starting_from;
        if(this.props.currentPrice == this.props.price) {
            return <span className="electro-price">
                    {text}<ins><span className="amount">{this.props.currentPrice}</span></ins>
                   </span>
        } else {
            return <span className="electro-price">
                        {text}<ins><span className="amount">{this.props.currentPrice}</span></ins>
                        <del><span className="amount">{this.props.price}</span></del> (-{this.props.discount}%)
                    </span>
        }
    }
}

class ProductConfigurable extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            sku: props.sku,
            currentPrice: props.currentPrice,
            price: props.price,
            discount: props.discount,
            inStock: props.inStock,
            shortDescription: props.shortDescription,
            saving: props.saving,
            tax: props.tax,
            specifications: props.specifications,
            isChanged: props.isChanged
        };
        this.nodePrice = document.querySelector("#price");
        this.nodeSku = document.querySelector("#product_show_sku");
        this.nodeInStock = document.querySelector("#product-in-stock");
        this.nodeSpecifications = document.querySelector('#table-tab-specification');
        this.nodeShortDescription = document.querySelector('#product_short_description');
        this.nodeSaving = document.querySelector('#product_saving');
        this.nodeTax = document.querySelector('#product_tax');
    }
    componentDidMount() {
        var self = this;
        var is_rtl = false;
        var $sync1 = $('.single-product .electro-gallery').children('.thumbnails-single');
        var $sync2 = $('.single-product .electro-gallery').children('.thumbnails-all');
        var flag = false;
        var duration = 300;

        $sync1.owlCarousel({
            items: 1,
            margin: 0,
            dots: false,
            nav: false,
            rtl: is_rtl,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                768: {
                    items: 1
                },
            }
        });

        var thumbnail_column_class = $sync2.attr('class');
        var cols = parseInt(thumbnail_column_class.replace('thumbnails-all columns-', ''));

        $sync2.owlCarousel({
            items: cols,
            margin: 8,
            dots: true,
            nav: false,
            rtl: is_rtl,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 3
                },
                768: {
                    items: cols
                },
            }
        });

        $sync2.on('click', 'a', function (e) {
            e.preventDefault();
        });

        $sync2.on('click', '.owl-item', function () {
            $sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
        });

        $sync2.on('changed.owl.carousel', function (e) {
            if (!flag) {
                flag = true;
                $sync1.trigger('to.owl.carousel', [e.item.index, duration, true]);
                flag = false;
            }
        });

        jQuery('#add_to_cart_cartItem_variant').on('change', function () {
            var $variant_id = jQuery(this).val();

            jQuery.ajax({
                type: "GET",
                url: Routing.generate('ade_api_shop_variant', { product_id: productConfigurable.productId, id: $variant_id }),
                dataType: 'json',
                success: function(data){

                    self.setState({
                        sku: data.sku,
                        currentPrice: data.currentPrice,
                        price: data.price,
                        discount: data.discount,
                        inStock: data.inStock,
                        tax: data.tax,
                        saving: data.saving,
                        shortDescription: data.shortDescription,
                        specifications: data.specifications,
                        isChanged: true
                    });

                    if(data.images.length > 0) {
                        self.renderCarousel(data.images, $sync1, $sync2);
                    }

                    if (data.inStock == 0) {
                        jQuery('#add_to_cart_cartItem_add').prop('disabled', true);
                    } else {
                        jQuery('#add_to_cart_cartItem_add').prop('disabled', false);
                    }

                    $('.add_to_wishlist').attr('href', Routing.generate('ade_shop_wishlist_add', { variant : data.variantId }));
                    $('.add-to-compare-link').attr('href', Routing.generate('ade_shop_compare_add', { variant : data.variantId }));

                },
                error: function(){
                    self.setState({
                        sku: productConfigurable.sku,
                        currentPrice: productConfigurable.currentPrice,
                        price: productConfigurable.price,
                        discount: productConfigurable.discount,
                        inStock: productConfigurable.inStock,
                        tax: productConfigurable.tax,
                        saving: productConfigurable.saving,
                        shortDescription: productConfigurable.shortDescription,
                        specifications: productConfigurable.specifications,
                        isChanged: false
                    });
                }
            });
        });
    }
    renderCarousel(images, $sync1, $sync2) {
        var htmlThumbnailsSingle = '';
        var htmlThumbnailsAll = '';
        var cssClass = '';

        var thumbnailsSingle = $sync2.find('img').length;

        jQuery.map(images, function( val, i ) {
            if (i === thumbnailsSingle - 1) {
                cssClass = ' class="last"';
            } else if(i === 0) {
                cssClass = ' class="first"';
            }
            htmlThumbnailsSingle += '<div class="owl-item"><a class="zoom" data-lightbox="roadtrip" href="' + val.image + '"><img class="wp-post-image" src="' + val.image + '" alt="' + val.alt + '"/></a></div>';
            htmlThumbnailsAll += '<div class="owl-item"><a ' + cssClass + ' href="' + val.image + '"><img class="wp-post-image" src="' + val.image + '" alt="' + val.alt + '"/></a></div>';
        });

        $sync1.trigger('replace.owl.carousel', htmlThumbnailsSingle).trigger('refresh.owl.carousel');
        $sync2.trigger('replace.owl.carousel', htmlThumbnailsAll).trigger('refresh.owl.carousel');
    }
    componentDidUpdate() {
        this.renderNodes();
    }
    renderNodes() {
        const {nodePrice, nodeSku, nodeInStock, nodeShortDescription, nodeSpecifications, nodeSaving, nodeTax} = this;
        ReactDOM.hydrate(<ProductSku sku={this.state.sku}/>, nodeSku);
        ReactDOM.hydrate(<ProductPrice isChanged={this.state.isChanged} currentPrice={this.state.currentPrice} price={this.state.price} discount={this.state.discount} />, nodePrice);
        ReactDOM.hydrate(<ProductInStock inStock={this.state.inStock} />, nodeInStock);
        ReactDOM.hydrate(<ProductShortDescription shortDescription={this.state.shortDescription} />, nodeShortDescription);
        ReactDOM.hydrate(<ProductSpecifications specifications={this.state.specifications} />, nodeSpecifications);
        ReactDOM.hydrate(<ProductSaving currentPrice={this.state.currentPrice} price={this.state.price} saving={this.state.saving} />, nodeSaving);
        ReactDOM.hydrate(<ProductTax tax={this.state.tax} />, nodeTax);
    }
    componentWillUnmount() {
        ReactDOM.unmountComponentAtNode(this.nodePrice);
        ReactDOM.unmountComponentAtNode(this.nodeSku);
        ReactDOM.unmountComponentAtNode(this.nodeInStock);
        ReactDOM.unmountComponentAtNode(this.nodeSpecifications);
        ReactDOM.unmountComponentAtNode(this.nodeShortDescription);
        ReactDOM.unmountComponentAtNode(this.nodeSaving);
        ReactDOM.unmountComponentAtNode(this.nodeTax);
    }
    render(){
        return null
    }
}

document.getElementById('product-adding-to-cart').reset();

ReactDOM.hydrate(<ProductConfigurable
        sku={productConfigurable.sku}
        currentPrice={productConfigurable.currentPrice}
        price={productConfigurable.price}
        discount={productConfigurable.discount}
        inStock={productConfigurable.inStock}
        specifications={productConfigurable.specifications}
        shortDescription={productConfigurable.shortDescription}
        saving={productConfigurable.saving}
        tax={productConfigurable.tax}
        isChanged={false}/>,
    document.createElement("div"));
