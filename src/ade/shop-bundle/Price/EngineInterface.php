<?php
namespace AdeShopBundle\Price;

interface EngineInterface
{
    public function addDiscountRules($expression);

    public function calculatePrice($object, array $catalogRule);
}