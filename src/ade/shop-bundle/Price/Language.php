<?php
namespace AdeShopBundle\Price;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

final class Language extends ExpressionLanguage
{
    protected function registerFunctions()
    {
        $this->register('date', function($date){
            return sprintf('(new \DateTime(%s))', $date);
        }, function(array $values, $date){
            return new \DateTime($date);
        });

        $this->register('rangeDate', function(){}, function($args, $today, $catalogRule){
            return  $today >= $catalogRule['startDate'] and $today <= $catalogRule['endDate'];
        });

        $this->register('dateAreNull', function(){}, function($args, $catalogRule){
            return $catalogRule['startDate'] == null and $catalogRule['endDate'] == null;
        });
    }
}