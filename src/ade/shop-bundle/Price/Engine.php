<?php

namespace AdeShopBundle\Price;

class Engine implements EngineInterface
{
    private $language;
    private $discountRules = array();

    public function __construct(Language $language)
    {
        $this->language = $language;
    }

    public function addDiscountRules($expression)
    {
        $this->discountRules[] = $expression;
    }

    public function calculatePrice($object, array $catalogRule)
    {
        $price = '';

        foreach ($this->discountRules as $discountRule)
        {
            $price = $object->getPrice() - ($object->getPrice() * $this->language->evaluate($discountRule, ['catalogRule' => $catalogRule]));
        }

        return $price != '' ? (float) $price : $object->getPrice();
    }

}