<?php

namespace AdeShopBundle\Generator;

use AdeShopBundle\Entity\Product;

interface VariantGeneratorInterface
{
    /**
     * @param Product $product
     *
     * @throws \InvalidArgumentException
     */
    public function generate(Product $product): void;
}
