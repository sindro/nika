<?php

namespace AdeShopBundle\Generator;

use AdeShopBundle\Checker\VariantsParityCheckerInterface;
use AdeShopBundle\Factory\VariantFactoryInterface;
use AdeShopBundle\Entity\Product;
use AdeShopBundle\Entity\Variant;
use Doctrine\ORM\EntityManagerInterface;
use Webmozart\Assert\Assert;

final class VariantGenerator implements VariantGeneratorInterface
{
    /**
     * @var VariantFactoryInterface
     */
    private $productVariantFactory;

    /**
     * @var CartesianSetBuilder
     */
    private $setBuilder;

    /**
     * @var VariantsParityCheckerInterface
     */
    private $variantsParityChecker;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param VariantFactoryInterface $productVariantFactory
     * @param VariantsParityCheckerInterface $variantsParityChecker
     * @param EntityManagerInterface $em
     */
    public function __construct(VariantFactoryInterface $productVariantFactory, VariantsParityCheckerInterface $variantsParityChecker, EntityManagerInterface $em) {
        $this->productVariantFactory = $productVariantFactory;
        $this->setBuilder = new CartesianSetBuilder();
        $this->variantsParityChecker = $variantsParityChecker;
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function generate(Product $product): void
    {
        Assert::true($product->hasOptions(), 'Cannot generate variants for an object without options.');

        $optionSet = [];
        $optionMap = [];

        foreach ($product->getOptions() as $key => $option) {
            foreach ($option->getValues() as $value) {
                $optionSet[$key][] = $value->getCode();
                $optionMap[$value->getCode()] = $value;
            }
        }

        $permutations = $this->setBuilder->build($optionSet);

        foreach ($permutations as $permutation) {
            $variant = $this->createVariant($product, $optionMap, $permutation);

            if (!$this->variantsParityChecker->checkParity($variant, $product)) {
                $product->addVariant($variant);
            }
        }

        $this->em->flush();

    }

    /**
     * @param Product $product
     * @param array $optionMap
     * @param mixed $permutation
     *
     * @return Variant
     */
    private function createVariant(Product $product, array $optionMap, $permutation): Variant
    {
        /** @var Variant $variant */
        $variant = $this->productVariantFactory->createForProduct($product);
        $this->addOptionValue($variant, $optionMap, $permutation);

        return $variant;
    }

    /**
     * @param Variant $variant
     * @param array $optionMap
     * @param mixed $permutation
     */
    private function addOptionValue(Variant $variant, array $optionMap, $permutation): void
    {
        if (!is_array($permutation)) {
            $variant->addOptionValue($optionMap[$permutation]);

            return;
        }

        foreach ($permutation as $code) {
            $variant->addOptionValue($optionMap[$code]);
        }
    }
}
