<?php

namespace AdeShopBundle\Checker;

use AdeShopBundle\Entity\Product;
use AdeShopBundle\Entity\Variant;

interface VariantsParityCheckerInterface
{
    /**
     * @param Variant $variant
     * @param Product $product
     *
     * @return bool
     */
    public function checkParity(Variant $variant, Product $product): bool;
}
