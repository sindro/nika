<?php

namespace AdeShopBundle\Checker;

use AdeShopBundle\Entity\Product;
use AdeShopBundle\Entity\Variant;

final class VariantsParityChecker implements VariantsParityCheckerInterface
{
    /**
     * {@inheritdoc}
     */
    public function checkParity(Variant $variant, Product $product): bool
    {
        foreach ($product->getVariants() as $existingVariant) {
            // This check is require, because this function has to look for any other different variant with same option values set
            if ($variant === $existingVariant || count($variant->getOptionValues()) !== count($product->getOptions())) {
                continue;
            }

            if ($this->matchOptions($variant, $existingVariant)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Variant $variant
     * @param Variant $existingVariant
     *
     * @return bool
     */
    private function matchOptions(Variant $variant, Variant $existingVariant): bool
    {
        foreach ($variant->getOptionValues() as $option) {
            if (!$existingVariant->hasOptionValue($option)) {
                return false;
            }
        }

        return true;
    }
}
