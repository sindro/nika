<?php
namespace AdeShopBundle\Controller;

use AdeShopBundle\Entity\Variant;
use AdeShopBundle\Util\CompareInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Translation\TranslatorInterface;

class CompareController extends Controller
{
    /**
     * @Route("/compare", name="ade_shop_compare")
     */
    public function compare(CompareInterface $compare)
    {
        $sessionCompare = $compare->get();

        $variants = $this->getDoctrine()->getRepository(Variant::class)->findByIds(array_keys($sessionCompare));

        return $this->render('@AdeShop/Compare/compare.html.twig', ['variants' => $variants, 'compare' => $sessionCompare]);
    }

    /**
     * @Route("/compare/add/{variant}", name="ade_shop_compare_add", options={"expose" = true})
     * @ParamConverter("variant", options={"mapping"={"variant"="id"}})
     */
    public function add(Request $request, CompareInterface $compare, TranslatorInterface $translator, Variant $variant)
    {
        $compare->add($variant, 1);

        $this->addFlash('success', $translator->trans('flash.compare.success',[], 'AdeShopBundle'));

        return new RedirectResponse($request->headers->get('referer'));
    }

    /**
     * @Route("/compare/remove/{variant}", name="ade_shop_compare_remove")
     * @ParamConverter("variant", options={"mapping"={"variant"="id"}})
     */
    public function remove(Request $request, CompareInterface $compare, TranslatorInterface $translator, Variant $variant)
    {
        $compare->remove($variant);

        $this->addFlash('success', $translator->trans('flash.compare_remove.success', [], 'AdeShopBundle'));

        return new RedirectResponse($request->headers->get('referer'));
    }
}