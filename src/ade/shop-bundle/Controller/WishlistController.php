<?php
namespace AdeShopBundle\Controller;

use AdeShopBundle\Entity\Variant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Translation\TranslatorInterface;

class WishlistController extends Controller
{
    /**
     * @Route("/wishlist", name="ade_shop_wishlist")
     */
    public function wishlist()
    {
        $variants = $this->getUser()->getVariants();

        return $this->render('@AdeShop/Wishlist/wishlist.html.twig', [
            'variants' => $variants
        ]);
    }

    /**
     * @Route("/wishlist/add/{variant}", name="ade_shop_wishlist_add", options={"expose" = true})
     * @ParamConverter("variant", options={"mapping"={"variant"="id"}})
     */
    public function add(Request $request, Variant $variant, TranslatorInterface $translator)
    {
        $user = $this->getUser();

        if($user->hasVariant($variant))
        {
            $this->addFlash('error', $translator->trans('flash.wishlist.exist',[], 'AdeShopBundle'));

        } else {
            $user->addVariant($variant);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $translator->trans('flash.wishlist.success',[], 'AdeShopBundle'));
        }

        return new RedirectResponse($request->headers->get('referer'));
    }

    /**
     * @Route("/wishlist/remove/{variant}", name="ade_shop_wishlist_remove")
     * @ParamConverter("variant", options={"mapping"={"variant"="id"}})
     */
    public function remove(Request $request, Variant $variant, TranslatorInterface $translator)
    {
        $user = $this->getUser();

        if($user->hasVariant($variant))
        {
            $user->removeVariant($variant);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $translator->trans('flash.cart_remove.success', [], 'AdeShopBundle'));

        } else {
            $this->addFlash('error', $translator->trans('flash.wishlist.error',[], 'AdeShopBundle'));
        }


        return new RedirectResponse($request->headers->get('referer'));
    }
}