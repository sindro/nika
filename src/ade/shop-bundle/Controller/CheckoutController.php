<?php
namespace AdeShopBundle\Controller;

use AdeShopBundle\Entity\Order;
use AdeShopBundle\Event\GetResponseOrderEvent;
use AdeShopBundle\Form\Type\OrderType;
use AdeShopBundle\Provider\ProductProvider;
use AdeShopBundle\Util\CartInterface;
use AdeShopBundle\Util\ManageStockFactory;
use Payum\Core\Request\GetHumanStatus;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class CheckoutController extends Controller implements CheckCartController
{
    /**
     * @Route("/shop/checkout", name="ade_shop_checkout")
     */
    public function checkout(Request $request, CartInterface $cart, ProductProvider $productProvider, EventDispatcherInterface $eventDispatcher)
    {
        $sessionCart = $cart->get();

        $variants = $productProvider->findByCart();

        $subTotal = $cart->getSubTotal($variants);

        $order = $this->get('payum')->getStorage(Order::class)->create();

        $order->populateUserData($this->getUser());
        $order->setItemsByCart($sessionCart, $variants);
        $order->setAmount($subTotal);

        $form = $this->createForm(OrderType::class, $order);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $order = $form->getData();
            $event = new GetResponseOrderEvent($order, $request);

            $eventDispatcher->dispatch('ade_shop.order.token', $event);

            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }
        }

        return $this->render('@AdeShop/Checkout/checkout.html.twig', [
            'form' => $form->createView(),
            'variants' => $variants,
            'subtotal' => $subTotal,
            'cart' => $sessionCart
        ]);
    }

    /**
     * @Route("/pagamento/eseguito", name="ade_payment_done")
     */
    public function done(Request $request, CartInterface $cart)
    {
        $cart->erase();

        $token = $this->get('payum')->getHttpRequestVerifier()->verify($request);

        $gateway = $this->get('payum')->getGateway($token->getGatewayName());

        $gateway->execute($status = new GetHumanStatus($token));
        $payment = $status->getFirstModel();

        return new JsonResponse(array(
            'status' => $status->getValue(),
            'payment' => array(
                'total_amount' => $payment->getTotalAmount(),
                'currency_code' => $payment->getCurrencyCode(),
                'details' => $payment->getDetails(),
            ),
        ));
    }

    /**
     * @Route("/pagamento/paypal/eseguito", name="ade_payment_paypal_done")
     */
    public function paypalDone(Request $request, CartInterface $cart)
    {
        $cart->erase();

        $token = $this->get('payum')->getHttpRequestVerifier()->verify($request);

        $gateway = $this->get('payum')->getGateway($token->getGatewayName());

        $gateway->execute($status = new GetHumanStatus($token));
        $details = iterator_to_array($status->getFirstModel());

        $em = $this->getDoctrine()->getManager();
        $order = $this->getDoctrine()->getRepository(Order::class)
            ->findOneBy(['id' => $details['INVNUM']]);

        $order->setStatus($status->getValue() == 'canceled' ? 'cancelled' : 'completed');

        $manageStock = ManageStockFactory::create($order->getStatus(), $order, $em);
        $manageStock->updateQuantity();

        $em->flush();

        return new JsonResponse(array(
            'status' => $status->getValue(),
            'details' => $details,
        ));
    }
}