<?php
namespace AdeShopBundle\Controller;

use AdeShopBundle\Entity\Product;
use AdeShopBundle\Entity\Variant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Translation\TranslatorInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class ApiController extends Controller
{
    /**
     * @Route("/api/variant/{product_id}/{id}", name="ade_api_shop_variant", options={"expose" = true})
     * @ParamConverter("product", options={"mapping"={"product_id"="id"}})
     */
    public function apiVariant(UploaderHelper $helper, TranslatorInterface $translator, Product $product, $id)
    {
        $variant = $this->getDoctrine()->getRepository(Variant::class)
                        ->findOneBy(['product' => $product, 'id' => $id]);

        if(!$variant) {
            throw $this->createNotFoundException('Variant not found');
        }

        $image = $variant->getImageFile();

        $gallery = $variant->getGallery();

        $images = [];

        if(null !== $gallery) {
            foreach ($gallery->getImages() as $image) {
                $images[] = ['image' => $helper->asset($image, 'imageFile'), 'alt' => $image->getAlt()];
            }
        } else if($image) {
            $images[] = ['image' => $helper->asset($variant, 'imageFile'), 'alt' => $variant->getProduct()->getTitle()];
        }

        $currentPrice = twig_localized_currency_filter($variant->getCurrentPriceWithTax(), 'EUR');
        $price = twig_localized_currency_filter($variant->getPriceWithTax(), 'EUR');

        return new JsonResponse(
            array(
                'variantId' => $variant->getId(),
                'sku' => $variant->getSku(),
                'currentPrice' => $currentPrice,
                'price' => $price,
                'discount' => $variant->getCurrentDiscount(),
                'images' => $images,
                'inStock' => $variant->getInStock(),
                'saving' => $translator->trans('saving', ['%saving%' => twig_localized_currency_filter($variant->getSaving(), 'EUR')], 'AdeShopBundle'),
                'tax' => $variant->getTax() ? $translator->trans('price_tax',['%title%' => $variant->getTax()], 'AdeShopBundle') : '',
                'shortDescription' => $variant->getShortDescription(),
                'specifications' => $variant->getSpecifications()
            )
        );
    }
}