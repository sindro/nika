<?php
namespace AdeShopBundle\Controller\Admin;

use AdeShopBundle\Generator\VariantGeneratorInterface;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\TranslatorInterface;

class ProductAdminController extends Controller
{
    public function createAction()
    {
        $this->admin->checkAccess('create');

        if (!$this->getRequest()->get('product_type') && $this->getRequest()->isMethod('get')) {

            return $this->render('@AdeShop/Admin/Product/select_product_type.html.twig', [
                'action' => 'create',
            ]);
        }

        return parent::createAction();
    }

    public function generateVariantsAction(VariantGeneratorInterface $generator, TranslatorInterface $translator, $id)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        $generator->generate($object);

        $this->addFlash('sonata_flash_success', $translator->trans('generate_variants_success', [], 'AdeShopBundle'));

        return new RedirectResponse($this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters())));
    }
}