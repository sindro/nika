<?php
namespace AdeShopBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController as Controller;

class CatalogRuleAdminController extends Controller
{
    public function createAction()
    {
        $this->admin->checkAccess('create');

        if (!$this->getRequest()->get('rule_type') && $this->getRequest()->isMethod('get')) {

            return $this->render('@AdeShop/Admin/CatalogRule/select_rule_type.html.twig', [
                'action' => 'create',
            ]);
        }

        return parent::createAction();
    }
}