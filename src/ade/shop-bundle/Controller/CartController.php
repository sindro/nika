<?php
namespace AdeShopBundle\Controller;

use AdeShopBundle\Entity\Variant;
use AdeShopBundle\Util\CartInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

class CartController extends Controller
{
    /**
     * @Route("/shop/cart", name="ade_shop_cart")
     */
    public function cart(CartInterface $cart)
    {
        $sessionCart = $cart->get();

        $variants = $this->getDoctrine()->getRepository(Variant::class)->findByIds(array_keys($sessionCart));
        $subTotal = $cart->getSubTotal($variants);

        return $this->render('@AdeShop/Cart/cart.html.twig', [
            'variants' => $variants,
            'subtotal' => $subTotal,
            'cart' => $sessionCart
        ]);
    }

    /**
     * @Route("/shop/cart/add/{variant_id}", name="ade_shop_cart_add")
     * @ParamConverter("variant", options={"mapping"={"variant_id"="id"}})
     */
    public function add(Request $request, CartInterface $cart, TranslatorInterface $translator, Variant $variant)
    {
        $cart->add($variant, 1);

        $this->addFlash('success', $translator->trans('flash.cart.success',[], 'AdeShopBundle'));

        return new RedirectResponse($request->headers->get('referer'));
    }

    /**
     * @Route("/shop/cart/add/combo/{variant_ids}", name="ade_shop_cart_add_combo")
     */
    public function addCombo(Request $request, CartInterface $cart, TranslatorInterface $translator, $variant_ids)
    {
        $ids = explode(',', $variant_ids);

        $variants = $this->getDoctrine()->getRepository(Variant::class)->findByIds($ids);

        $cart->addCombo($variants);

        $this->addFlash('success', $translator->trans('flash.cart.success',[], 'AdeShopBundle'));

        return new RedirectResponse($request->headers->get('referer'));
    }

    /**
     * @Route("/shop/cart/remove/{variant}", name="ade_shop_cart_remove")
     * @ParamConverter("variant", class="AdeShopBundle:Variant", options={"id" = "variant"})
     */
    public function remove(Request $request, CartInterface $cart, TranslatorInterface $translator, Variant $variant)
    {
        $cart->remove($variant);

        $this->addFlash('success', $translator->trans('flash.cart_remove.success', [], 'AdeShopBundle'));

        return new RedirectResponse($request->headers->get('referer'));
    }
}