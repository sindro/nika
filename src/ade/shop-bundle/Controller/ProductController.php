<?php
namespace AdeShopBundle\Controller;

use AdeShopBundle\Entity\Product;
use AdeShopBundle\Entity\Category;
use AdeShopBundle\Form\Type\AddToCartType;
use AdeShopBundle\Form\Type\CartItemType;
use AdeShopBundle\Util\CartInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;

class ProductController extends Controller
{
    /**
     * @Route("/shop/{category}/{page}",
     *         name="ade_shop_index",
     *         requirements={"page":"\d+"},
     *         defaults={"page": 1},
     *         options={
     *             "breadcrumb" : {
     *                  "label" : "%%title%%"
     *             }
     *         }
     * )
     */
    public function index(Request $request, PaginatorInterface $paginator, $category, $page)
    {
        $query = $request->query->get('queryShop') ?: null;

        $category = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['slug' => $category]);

        if(!$category)
        {
            throw $this->createNotFoundException('Category not found');
        }

        $this->get('ade_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('ade_shop_index')
            ->setRouteParameters(array(
                'category' => $category->getSlug(),
            ))
            ->setLabelParameters(array(
                'title' => $category->getTitle(),
            ));

        $products = $this->getDoctrine()->getRepository(Product::class)->findProducts($category, null, null, $query);

        $pagination = $paginator->paginate($products, $page, 18);

        return $this->render('@AdeShop/Product/index.html.twig', [
            'category' => $category,
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/prodotto/{slug}", name="ade_shop_show", options={
     *             "breadcrumb" : {
     *                  "label" : "%%title%%",
     *                  "parent_route" : "ade_shop_index"
     *             }
     * })
     * @ParamConverter("product", options={"mapping"={"slug"="slug"}})
     */
    public function show(Request $request, CartInterface $cart, TranslatorInterface $translator, Product $product)
    {
        if($product->getVariants()->count() <= 0)
        {
            throw $this->createNotFoundException('You must set variants for this product');
        }

        $this->get('ade_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('ade_shop_index')
            ->setRouteParameters(array(
                'category' => $product->getCategories()->first()->getSlug(),
            ))
            ->setLabelParameters(array(
                'title' => $product->getCategories()->first()->getTitle(),
            ));

        $this->get('ade_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('ade_shop_show')
            ->setRouteParameters(array(
                'slug' => $product->getSlug(),
            ))
            ->setLabelParameters(array(
                'title' => $product->getTitle(),
            ));

        $form = $this->createForm(AddToCartType::class, null, array(
            'product' => $product
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            if($form->isValid()) {
                $data = $form->getData();
                $cart->add($data['cartItem']['variant'], $data['cartItem']['quantity']);

                $this->addFlash('success', $translator->trans('flash.cart.success',[], 'AdeShopBundle'));

                return new RedirectResponse($request->headers->get('referer'));
            } else {
                $this->addFlash('error', $translator->trans('flash.cart.error',[], 'AdeShopBundle'));
            }
        }

        return $this->render('@AdeShop/Product/show.html.twig', [
            'product' => $product,
            'form' => $form->createView()
        ]);
    }
}