<?php
namespace AdeShopBundle\Controller;

use AdeShopBundle\Entity\Category;
use AdeShopBundle\Entity\Manufacturer;
use AdeShopBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ComponentController extends Controller
{
    /**
     * @Route("/component/shop/manufacturer", name="ade_shop_component_manufacturer")
     */
    public function _manufacturer(Request $request)
    {
        $manufactures = $this->getDoctrine()->getRepository(Manufacturer::class)->findAll();

        return $this->render('@AdeShop/Component/_manufacturer.html.twig', ['manufacturers' => $manufactures]);
    }

    /**
     * @Route("/component/shop/price_filter", name="ade_shop_component_price_filter")
     */
    public function _priceFilter(Request $request)
    {
        $productWithMinPrice = $this->getDoctrine()->getRepository(Product::class)->findProductWithMinPrice();
        $productWithMaxPrice = $this->getDoctrine()->getRepository(Product::class)->findProductWithMaxPrice();

        return $this->render('@AdeShop/Component/_price_filter.html.twig', [
            'productWithMinPrice' => $productWithMinPrice,
            'productWithMaxPrice' => $productWithMaxPrice
        ]);
    }

    /**
     * @Route("/component/shop/categories", name="ade_shop_component_categories")
     */
    public function _categories(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findRootNodes();

        return $this->render('@AdeShop/Component/_categories.html.twig', [
            'categories' => $categories
        ]);
    }
}