<?php

namespace AdeBreadcrumbBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use AdeBreadcrumbBundle\DependencyInjection\AdeBreadcrumbExtension;

/**
 * Breadcrumb bundle class
 *
 * @codeCoverageIgnore
 */
class AdeBreadcrumbBundle extends Bundle
{

    public function getContainerExtension()
    {
        return new AdeBreadcrumbExtension();
    }
}
