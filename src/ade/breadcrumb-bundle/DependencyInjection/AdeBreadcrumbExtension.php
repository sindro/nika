<?php

namespace AdeBreadcrumbBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * Symfony2 DI extension
 *
 * @codeCoverageIgnore
 */
class AdeBreadcrumbExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('ade_breadcrumb.template', $config['template']);
        $container->setParameter('ade_breadcrumb.class.model', $config['model_class']);
        $container->setParameter('ade_breadcrumb.class.collection', $config['collection_class']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yaml');

        $container->setAlias('ade_breadcrumb.breadcrumb_provider', $config['provider_service_id'])->setPublic(true);
    }
}
