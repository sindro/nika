<?php

namespace AdeBreadcrumbBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * DI configuration
 *
 * @codeCoverageIgnore
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ade_breadcrumb');

        $rootNode
            ->children()
                ->scalarNode('template')->defaultValue('@AdeBreadcrumb/breadcrumbs.html.twig')->end()
                ->scalarNode('model_class')->defaultValue('AdeBreadcrumbBundle\Model\Breadcrumb')->end()
                ->scalarNode('collection_class')->defaultValue('AdeBreadcrumbBundle\Model\BreadcrumbCollection')->end()
                ->scalarNode('provider_service_id')->defaultValue('ade_breadcrumb.breadcrumb_provider.default')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
