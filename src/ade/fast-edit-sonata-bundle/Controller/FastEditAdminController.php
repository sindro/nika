<?php
namespace AdeFastEditSonataBundle\Controller;

use AdeShopBundle\Entity\Attribute;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
class FastEditAdminController extends Controller
{
    /**
     * @Route("/admin/fast-edit-field/{id}", name="ade_fast_edit_sonata_update_field")
     */
    public function updateFieldAction(Request $request)
    {
        $value = $request->get('value');
        $method = 'set' . ucwords($request->get('field'));
        $adminCode = $request->get('code');

        if(!$this->container->has($adminCode)) {
            throw $this->createNotFoundException(sprintf('unable to find service %s', $adminCode));
        }

        if(!method_exists($this->container->get($adminCode)->getClass(), $method)) {
            throw $this->createNotFoundException(sprintf('unable to find the method %s', $method));
        }

        $object = $this->get('doctrine')->getRepository($this->container->get($adminCode)->getClass())->findOneBy(['id' => $request->get('id')]);

        if (!$object) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id : %s', $object));
        }

        $em = $this->getDoctrine()->getManager();
        $object->$method($value);
        $em->flush();

        die();
    }

    /**
     * @Route("/admin/get-attributes", name="ade_fast_edit_sonata_get_attributes")
     */
    public function getAttributesAction(Request $request)
    {
        $data = $request->get('attributes');

        $attributes = $this->get('doctrine')->getRepository(Attribute::class)->findByIds(array_values($data));

        $options = [];

        foreach($attributes as $attribute)
        {
            $options []= $attribute->getOptionsToArray();
        }

        echo json_encode($this->array_flatten($options));

        die;
    }

    private function array_flatten($array)
    {
        if (!is_array($array)) {
            return false;
        }

        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, $this->array_flatten($value));
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }
}