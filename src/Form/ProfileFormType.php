<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseProfileFormType;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('username')
            ->add('firstname', null,[
                'label' => 'form.firstname',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('lastname', null,[
                'label' => 'form.lastname',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('phone', null,[
                'label' => 'form.phone',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('address', null,[
                'label' => 'form.address',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('postalCode', null,[
                'label' => 'form.postalCode',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('city', null,[
                'label' => 'form.city',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('country', null,[
                'label' => 'form.country',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('fiscalCode', null,[
                'label' => 'form.fiscalCode',
                'translation_domain' => 'FOSUserBundle'
            ]);
    }

    public function getParent()
    {
        return BaseProfileFormType::class;
    }

}