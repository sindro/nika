<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('username')
            ->add('firstname', null,[
                'label' => 'form.firstname',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('lastname', null,[
                'label' => 'form.lastname',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('address', null,[
                'label' => 'form.address',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('postalCode', null,[
                'label' => 'form.postalCode',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('city', null,[
                'label' => 'form.city',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('country', null,[
                'label' => 'form.country',
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('fiscalCode', null,[
                'label' => 'form.fiscalCode',
                'translation_domain' => 'FOSUserBundle'
            ]);
    }

    public function getParent()
    {
        return BaseRegistrationFormType::class;
    }

}