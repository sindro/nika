<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\MissingOptionsException;

/**
 * @Annotation
 */
class SameItemInCollection extends Constraint
{
    const ERROR_PATH_FORM = 'form';
    const ERROR_PATH_FIELD = 'field';

    public $message = 'Item already exists in collection "%collectionName%" .'; // %variable%
    public $variable;
    public $collection;
    public $errorPath = self::ERROR_PATH_FIELD;

    public function __construct($options = null)
    {
        parent::__construct($options);

        if (null === $this->collection) {
            throw new MissingOptionsException(sprintf('Option collection" must be given for constraint %s', __CLASS__));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}