<?php
namespace App\Validator\Constraints;

use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SameItemInCollectionValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (null === $value) {
            return;
        }

        $accessor = new PropertyAccessor();
        $value = $accessor->getValue($value, $constraint->collection);

        if (!is_array($value) && !$value instanceof \Countable) {
            throw new UnexpectedTypeException($value, 'array or \Countable');
        }

        if (count($value)) {
            $repeatedItems = [];
            $existedItems = [];

            $itemIndex = 0;
            foreach ($value as $item) {
                if ($constraint->variable) {
                    $variable = $accessor->getValue($item, $constraint->variable);
                } else {
                    $variable = $item;
                }
                if (in_array($variable, $existedItems)) {
                    if (!in_array($variable, $repeatedItems)) {
                        $repeatedItems[] = [
                            'itemIndex' => $itemIndex,
                            'item' => $item
                        ];
                    }
                } else {
                    $existedItems[] = $variable;
                }
                $itemIndex++;
            }

            foreach ($repeatedItems as $item) {
                $value = null;

                foreach (array_unique(array_map('trim', explode(',', $constraint->errorPath))) as $path) {
                    $this->addViolation(trim($path), $constraint, $item, $value);
                }
            }
        }
    }

    private function addViolation($path, $constraint, $row, $value = null)
    {
        $atPath = null;

        if ($path == SameItemInCollection::ERROR_PATH_FIELD && $constraint->errorPath) {
            $atPath = sprintf('%s[%d].%s', $constraint->collection, $row['itemIndex'], $constraint->variable);
        }

        $violation = $this->context->buildViolation($constraint->message)
            ->setParameter('%collectionName%', $constraint->collection)
            ->atPath($atPath);

        if($value) {
            $violation->setParameter('%variable%', $value);
        }

        $violation->addViolation();
    }
}