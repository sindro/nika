<?php
namespace App\Entity;

use AdeShopBundle\Entity\Order;
use AdeShopBundle\Entity\Variant;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @UniqueEntity("fiscalCode")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lastname;

    /**
     * @ORM\Column(name="company", type="string", length=100, nullable=true)
     */
    protected $company;

    /**
     * @ORM\Column(name="vat", type="string", length=32, nullable=true)
     */
    protected $vat;

    /**
     * @ORM\Column(name="fiscal_code", type="string", length=16, nullable=true)
     * @Assert\NotBlank()
     */
    protected $fiscalCode;

    /**
     * @ORM\Column(name="phone", type="string", length=30, nullable=true)
     * @Assert\NotBlank(groups={"Profile", "Registration"})
     */
    protected $phone;

    /**
     * @ORM\Column(name="address", type="string", length=100, nullable=true)
     * @Assert\NotBlank()
     */
    protected $address;

    /**
     * @ORM\Column(name="postal_code", type="string", nullable=true)
     * @Assert\NotBlank()
     */
    protected $postalCode;

    /**
     * @ORM\Column(name="city", type="string", length=100, nullable=true)
     * @Assert\NotBlank()
     */
    protected $city;

    /**
     * @ORM\Column(name="country", type="string", length=100, nullable=true)
     * @Assert\NotBlank()
     */
    protected $country;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Group")
     * @ORM\JoinTable(name="fos_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    /**
     * @ORM\Column(name="bonus", type="integer", nullable=true)
     */
    protected $bonus = 0;

    /**
     * @ORM\ManyToMany(targetEntity="AdeShopBundle\Entity\Variant")
     * @ORM\JoinTable(name="ade_shop_user_variant",
     *      joinColumns={@ORM\JoinColumn(name="variant_id", referencedColumnName="id", unique=false, nullable=false, onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", unique=false, nullable=false, onDelete="CASCADE")}
     *      )
     */
    protected $variants;

    /**
     * @ORM\OneToMany(targetEntity="AdeShopBundle\Entity\Order", mappedBy="user", cascade={"persist"})
     */
    protected $orders;

    use ORMBehaviors\Timestampable\Timestampable;

    public function __construct()
    {
        parent::__construct();
        $this->variants = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Overridden so that username is now optional
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->setUsername($email);
        return parent::setEmail($email);
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company): void
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param mixed $vat
     */
    public function setVat($vat): void
    {
        $this->vat = $vat;
    }

    public function getFullName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     */
    public function setPostalCode($postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return mixed
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getFiscalCode()
    {
        return $this->fiscalCode;
    }

    /**
     * @param mixed $fiscalCode
     */
    public function setFiscalCode($fiscalCode): void
    {
        $this->fiscalCode = $fiscalCode;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $groups
     */
    public function setGroups($groups): void
    {
        $this->groups = $groups;
    }

    /**
     * @param mixed $bonus
     */
    public function setBonus($bonus): void
    {
        $this->bonus = $bonus;
    }

    /**
     * Get variants
     */
    public function getVariants()
    {
        return $this->variants;
    }

    /**
     * Set variants
     */
    public function setVariants($variants)
    {
        if (count($variants) > 0) {
            foreach ($variants as $variant) {
                $this->addVariant($variant);
            }
        }
    }

    /**
     * Add variant
     */
    public function addVariant(Variant $variant)
    {
        $this->variants[] = $variant;
    }

    /**
     * Remove variant
     */
    public function removeVariant(Variant $variant)
    {
        $this->variants->removeElement($variant);
    }

    public function hasVariant(Variant $variant): bool
    {
        return $this->variants->contains($variant);
    }

    /**
     * Get orders
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set orders
     */
    public function setOrders($orders)
    {
        if (count($orders) > 0) {
            foreach ($orders as $order) {
                $this->addOrder($order);
            }
        }
    }

    /**
     * Add order
     */
    public function addOrder(Order $order)
    {
        $this->orders[] = $order;
    }

    /**
     * Remove order
     */
    public function removeOrder(Order $order)
    {
        $this->orders->removeElement($order);
    }

    public function hasOrder(Order $order): bool
    {
        return $this->orders->contains($order);
    }
}